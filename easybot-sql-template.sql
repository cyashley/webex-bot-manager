-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 27. Nov, 2019 23:37 PM
-- Server-versjon: 5.5.54-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easybot-template`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(100) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `command` text NOT NULL,
  `user` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `ad_groups`
--

CREATE TABLE `ad_groups` (
  `id` int(100) NOT NULL,
  `group_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `ad_group_mapping`
--

CREATE TABLE `ad_group_mapping` (
  `id` int(100) NOT NULL,
  `ad_group_id` int(100) NOT NULL,
  `local_group_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `blocked_contacts`
--

CREATE TABLE `blocked_contacts` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bots`
--

CREATE TABLE `bots` (
  `id` varchar(255) NOT NULL,
  `emails` varchar(100) NOT NULL,
  `displayName` varchar(255) NOT NULL,
  `avatar` varchar(400) NOT NULL,
  `type` varchar(50) NOT NULL,
  `access` varchar(255) NOT NULL,
  `main` int(10) NOT NULL,
  `defres` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bot_allowed_domain`
--

CREATE TABLE `bot_allowed_domain` (
  `botid` varchar(100) NOT NULL,
  `domainid` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bot_webhook`
--

CREATE TABLE `bot_webhook` (
  `id` int(11) NOT NULL,
  `webhookid` varchar(100) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `groupid` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `contacts`
--

CREATE TABLE `contacts` (
  `id` varchar(200) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `emails` varchar(100) NOT NULL,
  `avatar` varchar(500) NOT NULL,
  `orgId` varchar(200) NOT NULL,
  `type` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `contact_access_group_response`
--

CREATE TABLE `contact_access_group_response` (
  `id` varchar(200) NOT NULL,
  `contactid` varchar(200) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `domains`
--

CREATE TABLE `domains` (
  `id` int(100) NOT NULL,
  `domain` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `excluded_spaces`
--

CREATE TABLE `excluded_spaces` (
  `id` varchar(200) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `spacetitle` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `features`
--

CREATE TABLE `features` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `usage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `features`
--

INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES
(1, 'Admin controls', 'admin', 'The \"Admin Controls\" feature will allow a bot to manage the WebEx Teams Bot Managment site from the client using the assigned bot. The admin controls feature is quite powerful and should be protected with an access group!', '##ADMINISTRATOR COMMANDS\r\n---\r\n\r\n<blockquote class=primary>System</blockquote>\r\n\r\n- **admin maintenance** [**enable**/**disable**] \r\n    - Enables or disables maintenance mode, can be enabled to prevent new external tasks to be created in case of modifications or downtime. \r\n\r\n<blockquote class=primary>Message</blockquote>\r\n\r\n- **admin message list [messageid]** \r\n    - Get a list of stored messages with message id. You can supply a message id to the list command to preview a message.\r\n- **admin message announce [groupid] [messageid]**\r\n    - Announce a stored message specified groups. You can issue multiple groups in csv (no spaces)\r\n- **admin message echo [groupid],[email] [free text here..]**\r\n    - Sends an echo of your message to the specified groups or emails. You can issue a combo of multiple groups and emails in csv (no spaces)\r\n\r\n<blockquote class=primary>Feedback</blockquote>\r\n\r\n- **admin feedback create [Topic text..]** \r\n    - Creates a new feedback topic for this bot\r\n- **admin feedback allow_statement [topicId:0|1]**\r\n    - Turn a rule off or on.\r\n- **admin feedback topic [topicId]**\r\n    - View topic details and rules\r\n\r\n<blockquote class=primary>User management</blockquote>\r\n- **admin user list** \r\n    - Lists all the users added to the database\r\n- **admin check** [**email_address**] \r\n    - Checks if a user exists in the database and any memberships\r\n- **admin add** [**email_address**],[**email_address**]..\r\n    - Adds one or more new users to the database (if not exists)\r\n- **admin delete** [**email_address**],[**email_address**].. \r\n    - Deletes one or more users from the database\r\n- **admin block** [**email_address**] \r\n    - Blocks all my communication with this user regardless \r\n- **admin unblock** [**email_address**] \r\n    - Unblocks user \r\n\r\n<blockquote class=primary>Group and membership management</blockquote>\r\n- **admin group list** \r\n    - Lists available groups\r\n- **admin group members [groupid]**\r\n    - Lists all members of a group\r\n- **admin group create [groupname:groupid]**\r\n    - Creates a new group. The groupname and the groupid cannot contain spaces and must be separated with colon only if you create a groupid. Groupid is optional but it makes it easier to add users to the group, else the ID will be an incremental number.\r\n- **admin group delete [groupid]**\r\n- **admin group mention [groupid]** \r\n    - Creates a mention tag for all members of the group and will be mentioned by the bot in the current space.\r\n- **admin group addtospace [groupid]**\r\n    - Adds all the members of the group to the current space\r\n- **admin group add** [**groupid:email_addess**]  \r\n    - Adds a user or a group space to a group, you can separate the groupid and or emails with comma to add multiple users to multiple groups (groupid,groupid,groupid:email_address,email_address). To add a space you have to type **this** instead of an e-mail: [**groupid:this**] the space must be a group space and the group must be owned by the bot you are talking to. \r\n- **admin group remove** [**groupid:email_addess**] - Removes a user from a group, you can separate the groupid and or e-mails with comma to remove multiple groups (XX,XX,XX:email_address,email_address)\r\n\r\n<blockquote class=primary>Space controls (admin commands)</blockquote>\r\n- **admin spaceresponse status** \r\n    - Checks the response status in the current space (command must be issued in the space you wish to see the status)\r\n- **admin spaceresponse enable|disable** \r\n    - Enables or disables group response for any members in the space (command must be issued in the space you wish to enable)\r\n- **admin spaceresponse [email_address]** \r\n    - Enables spaceresponse for that particular user in the particular space (command must be issued in the space you wish to enable)\r\n- **admin joinable** \r\n    - Make a group space joinable with the **space join** command (must be issued inside the space you want to make joinable)\r\n\r\n<blockquote class=primary>Task handler</blockquote>\r\n- **admin queue report** \r\n    - Checks number of tasks in the queue\r\n- **admin queue purge** \r\n    - Purge task queue\r\n'),
(2, 'Space controls', 'space', 'Space controls lets you interact with a bot in a space and control spaces. Add csv of users, kick, create.', '##SPACE CONTROLS\r\n---\r\n\r\n<blockquote class=primary>SPACE CONTROLS</blockquote>\r\n\r\n- **space create** \r\n    - *Creates a new space with you and the bot with a autogenerated title*\r\n- **space create [title text ..]** \r\n    - *Creates a new space with you and the bot. The space will be named with UTC timestamp and the specified title*\r\n- **space create [email,email..]** \r\n    - *Creates a new space and adds the users specified with CSV at the same time*\r\n\r\n<blockquote>Note: If the **space create** command is issued in a team space, the space will be created within the team.</blockquote>\r\n\r\n- **space add [email,email..]** \r\n    - *Adds users to the current space*\r\n- **space kick [email]** \r\n    - Removes a user from a space, note that if you write only \"space kick\" it will remove you. \r\n- **space gencsv** \r\n    - I will provide you with a CSV of emails based on who is in the space you issue this command in. If there are too many people in the space I will not be able to provide the list. \r\n- **space list** \r\n    - Lists spaces marked as **joinable** for this bot (shows spaceid)\r\n- **space join [spaceid]**\r\n    - I will invite you to the space \r\n\r\n\r\n'),
(3, 'Random friendly jokes', 'joke', 'By issuing the <b>joke</b> command to a bot, it will reply with a joke. \r\n\r\nEntertainment feature ', '##JOKE\r\n---\r\n\r\n<blockquote class=primary>JOKE - ENTERTAINMENT</blockquote>\r\n\r\n- **joke** - *The bot will tell a random friendly joke*'),
(4, 'Random yomomma jokes', 'yomomma', 'A series of insulting jokes. \r\n\r\nCan be inappropriate in certain scenarios. \r\n\r\nEntertainment feature. ', '##YOMOMMA\r\n---\r\n\r\n<blockquote class=primary>YOMOMMA - ENTERTAINMENT</blockquote>\r\n\r\n- **yomomma** - *The bot will tell a random  yomomma joke*'),
(5, 'Random Chuck Norris jokes', 'chuck', 'A series of Chuck Norris jokes. \r\n\r\nSome of the jokes can be inappropriate. \r\n\r\nEntertainment feature.', '##CHUCK\r\n---\r\n\r\n<blockquote class=primary>CHUCK NORRIS - ENTERTAINMENT</blockquote>\r\n\r\n- **chuck** - *The bot will tell a random Chuck Norris joke*'),
(6, 'Remove last message', 'remove', 'This feature will when issued make the bot delete its latest response, can be issued multiple times to remove more content. ', '##REMOVE MESSAGE\r\n---\r\n\r\n<blockquote class=primary>REMOVE LAST MESSAGES</blockquote>\r\n\r\n- **remove last** - *The bot will remove its latest reply in the space*'),
(7, 'Service report', 'service', 'Gives feedback on the task listener (requires third party script to report correct status)', '##SERVICE\r\n---\r\n\r\n<blockquote class=primary>SERVICE REPORT</blockquote>\r\n\r\n- **service** - *The bot will let you know if the external task service is still alive*'),
(8, 'Usage reports', 'usage', 'Reports back the amount of commands the bot has responded to. ', '##USAGE\r\n---\r\n\r\n<blockquote class=primary>USAGE REPORTS</blockquote>\r\n\r\n- **usage** - *The bot will go through the activity logs and give an estimate of how many times a command has been issues and what command is used most frequently*'),
(9, 'Who am I', 'whoami', 'The user who issues the command will see what access groups he is currently member of in the Bot Manager. ', '##WHO AM I\r\n---\r\n\r\n<blockquote class=primary>WHOAMI - SELF CHECK</blockquote>\r\n\r\n- **whoami** - *The bot will check what groups a user is part of in the bot management system and provide the list of memberships to the user*'),
(10, 'Lookup spark user', 'whois', 'Issues a query in spark for a username or email. The bot will return details about this user. ', '##WHO IS\r\n---\r\n\r\n<blockquote class=primary>WHO IS PERSON</blockquote>\r\n\r\n- **whoid** [**email|name**] - *The bot will look up the user in spark and provide som trivial information about the user*'),
(11, 'Request being added', 'request', 'With the request feature you can let the user decide if he or she wants to receive notifications etc. The feature will automatically add the user to the database. You can also tag groups as \"default groups\". Whenever a user typed requests the user will be added to the default groups as well. ', '##REQUEST\r\n---\r\n\r\n<blockquote class=primary>Request participation</blockquote>\r\n\r\n- **request** - *The bot will add the user that issues this command to the database and to the pre-configured default groups. Makes administration of users easier*'),
(12, 'Subscribe to groups', 'subscribe', 'With subscription groups enabled you allow the users to decide what groups they should to be part of.\r\n\r\nIf you use a specific group for notifications frequently you might want to allow users to unsubscribe if they don\'t want more notification. \r\n\r\nSubscription groups can also be used for other purposes.  ', '##SUBSCRIPTION GROUPS\r\n---\r\n\r\n<blockquote class=primary>Usage:</blockquote>\r\n\r\n- **subscribe** '),
(13, 'Delete self', 'resign', 'A user that has been added in the bot management system will be able to delete him self from the database (only him self). ', '##RESIGN AS A USER\r\n---\r\n\r\n<blockquote class=primary>Usage:</blockquote>\r\n\r\n- **resign** - *Deletes the issuer as a user (if exists).*  '),
(14, 'Collect user feedback', 'feedback', 'Setup a bot to collect user input or save data from users. Create topics and let the users create entries of which others can vote on topic entries or comment on the entries.', '##FEEDBACK COMMANDS\r\n---\r\n\r\n<blockquote class=primary>Introduction</blockquote>\r\n\r\nThis feature opens up for user input. An admin can create one or more topics for the bot. \r\n\r\nEach topic has a set of rules that can allow a user to view and create entries of which other users can vote and comment on.\r\n\r\nEach topic may have a different set of rules, for example a topic can be private (only accessible to a specific group of people) or certain features may be turned off, like commenting or voting. I will notify you if such a rule applies. Only admins can create topics and setup rules. \r\n\r\n<blockquote class=primary>Topic commands</blockquote>\r\n\r\nNote: **[placeholders]** are typed without the **[]** \r\n\r\n- **feedback topics** \r\n    - View all available topics and the correlating topicId.\r\n- **feedback topic [topicId]** \r\n    - View all entries in the specified topicId. This also includes an overview of how many comments and votes per entry.\r\n- **feedback topic [topicId] add [entry text ..]**\r\n    - Create a new entry in the specified topicId\r\n\r\n<blockquote class=primary>Entry commands</blockquote>\r\n\r\n- **feedback entry [entryId]** \r\n    - View the full details of a specified entry including votes and comments. \r\n- **feedback entry [entryId] delete**\r\n    - Deletes the specified entryId if you have created it. Deleting an entry will also delete all votes and comments on the specified entry. This command will not provide a confirmation prompt and cannot be undone.  \r\n\r\n<blockquote class=primary>Votes and Comments</blockquote>\r\n\r\n- **feedback vote [entryId]** \r\n    - Place your vote on the specified entryId. You can revoke the vote by placing the same command a second time.   \r\n- **feedback comment [entryId] [comment text ...]**\r\n    - Place a comment on the specified entryId. \r\n- **feedback comment delete [commentId]**\r\n    - Deletes a comment that you have created using the commentId. The commentId can be found in the comment by viewing the entry details.   \r\n\r\n\r\n'),
(15, 'Devices ', 'device', 'Enable device control', '##DEVICE COMMANDS\r\n---\r\n\r\n<blockquote class=primary>View devices</blockquote>\r\n\r\n- **device list** - *List the available devices controlled by this bot (including deviceId)* \r\n\r\n<blockquote class=primary>Status and command</blockquote>\r\n- **device status [deviceId] [statusKey]** - *Get status from the device on a statuskey audio.volume*\r\n- **device command [deviceId] [commandKey] {attribute:value}** - *Send a command to a device with a command key*\r\n\r\nExample usage:\r\n\r\nThe **deviceId** is acquired using **device list**\r\n\r\n- **device status [deviceId] audio.volume** - Returns the current volume on the device\r\n- **device command [decviceId] audio.volume.set {\"Level\": 60}** - Set the volume on the device\r\n\r\n');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `feedback_entries`
--

CREATE TABLE `feedback_entries` (
  `id` int(100) NOT NULL,
  `topic_id` int(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `feedback_entry_comment`
--

CREATE TABLE `feedback_entry_comment` (
  `id` int(100) NOT NULL,
  `entry_id` int(100) NOT NULL,
  `comment` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `feedback_entry_vote`
--

CREATE TABLE `feedback_entry_vote` (
  `id` int(100) NOT NULL,
  `entry_id` int(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `voted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `feedback_topic`
--

CREATE TABLE `feedback_topic` (
  `id` int(100) NOT NULL,
  `botid` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `comments_allowed` int(1) NOT NULL DEFAULT '1',
  `votes_allowed` int(1) NOT NULL DEFAULT '1',
  `entry_create_allowed` int(1) NOT NULL DEFAULT '1',
  `entry_view_allowed` int(1) NOT NULL,
  `entry_delete_allowed` int(1) NOT NULL DEFAULT '1',
  `accessgroup` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `generic_feedback`
--

CREATE TABLE `generic_feedback` (
  `id` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `default_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `generic_feedback`
--

INSERT INTO `generic_feedback` (`id`, `message`, `default_message`) VALUES
('maintenance', '**Maintenance mode is currently enabled:** The requested task is blocked and purged when this mode is enabled due to ongoing maintenance or downtime. Please try again later.', '**Maintenance mode is currently enabled:** The requested task is blocked and purged when this mode is enabled due to ongoing maintenance or downtime. Please try again later.'),
('warning', '**Warning** We are experiencing temporary network issues!', '**Warning** We are experiencing temporary network issues!');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `sub_id` varchar(100) NOT NULL,
  `groupname` varchar(200) NOT NULL,
  `subscribable` int(10) NOT NULL,
  `default_group` int(1) NOT NULL,
  `description` text NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `groups`
--

INSERT INTO `groups` (`id`, `sub_id`, `groupname`, `subscribable`, `default_group`, `description`, `botid`) VALUES
(15, '', 'ADMINS', 0, 0, 'Respond to member in any group type', '0');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `group_contacts`
--

CREATE TABLE `group_contacts` (
  `groupid` int(100) NOT NULL,
  `contactid` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `group_groups`
--

CREATE TABLE `group_groups` (
  `groupid` varchar(100) NOT NULL,
  `nestedid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `group_spaces`
--

CREATE TABLE `group_spaces` (
  `groupid` int(100) NOT NULL,
  `spaceid` varchar(200) NOT NULL,
  `botid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `heartbeat`
--

CREATE TABLE `heartbeat` (
  `id` int(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(100) NOT NULL,
  `who` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `heartbeat`
--

INSERT INTO `heartbeat` (`id`, `time`, `ip`, `who`) VALUES
(1, '2017-12-10 23:26:14', '173.38.220.47', 'internal_pi');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `joinable_space`
--

CREATE TABLE `joinable_space` (
  `id` int(100) NOT NULL,
  `spaceid` varchar(200) NOT NULL,
  `spacetitle` varchar(400) NOT NULL,
  `access_group` int(100) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `log_bot`
--

CREATE TABLE `log_bot` (
  `id` int(1) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `log_bot`
--

INSERT INTO `log_bot` (`id`, `botid`) VALUES
(0, 'none');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `log_users`
--

CREATE TABLE `log_users` (
  `id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `queue_subtasks`
--

CREATE TABLE `queue_subtasks` (
  `subtask_id` varchar(100) NOT NULL,
  `task_id` varchar(100) NOT NULL,
  `bot_id` varchar(255) NOT NULL,
  `subtask_type` varchar(25) NOT NULL,
  `task_json` mediumtext NOT NULL,
  `group_id` varchar(25) NOT NULL,
  `task_code` varchar(3) NOT NULL,
  `payload` text NOT NULL,
  `response` text NOT NULL,
  `task_results` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `queue_task`
--

CREATE TABLE `queue_task` (
  `task_id` varchar(100) NOT NULL,
  `task_type` varchar(50) NOT NULL,
  `task_name` varchar(35) NOT NULL,
  `task_submitter` varchar(35) NOT NULL,
  `botresponse` int(1) NOT NULL,
  `botid` varchar(150) NOT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clear` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `response` text NOT NULL,
  `file_url` varchar(300) NOT NULL,
  `is_task` int(1) NOT NULL,
  `is_feature` int(1) NOT NULL,
  `accessgroup` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `roles`
--

CREATE TABLE `roles` (
  `id` int(100) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `saved_messages`
--

CREATE TABLE `saved_messages` (
  `id` int(100) NOT NULL,
  `title` text NOT NULL,
  `message` text NOT NULL,
  `files` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `service_status`
--

CREATE TABLE `service_status` (
  `id` int(1) NOT NULL,
  `maintenance_mode` int(1) NOT NULL,
  `warning_mode` int(1) NOT NULL,
  `task_monitor` int(11) NOT NULL,
  `maintenance_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `service_status`
--

INSERT INTO `service_status` (`id`, `maintenance_mode`, `warning_mode`, `task_monitor`, `maintenance_message`) VALUES
(1, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(25) NOT NULL,
  `settings` varchar(25) NOT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `site_settings`
--

INSERT INTO `site_settings` (`id`, `settings`, `value`) VALUES
(1, 'dbversion', '4');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `taskId` varchar(100) NOT NULL,
  `attr1` text NOT NULL,
  `attr2` text NOT NULL,
  `attr3` text NOT NULL,
  `attr4` text NOT NULL,
  `attr5` text NOT NULL,
  `attr6` text NOT NULL,
  `text` text NOT NULL,
  `user` varchar(200) NOT NULL,
  `bot` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `roomid` varchar(400) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `webex_response_codes`
--

CREATE TABLE `webex_response_codes` (
  `code` varchar(3) NOT NULL,
  `status` varchar(50) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `webex_response_codes`
--

INSERT INTO `webex_response_codes` (`code`, `status`, `label`, `description`) VALUES
('200', 'OK', '<span class=\"label label-success\">Success</span>', 'Successful request with body content.'),
('204', 'No Content', '<span class=\"label label-success\">No Content</span>', 'Successful request without body content.'),
('400', 'Bad Request', '<span class=\"label label-warning\">Bad Request</span>', 'The request was invalid or cannot be otherwise served. An accompanying error message will explain further.'),
('401', 'Unauthorized', '<span class=\"label label-warning\">Unauthorized</span>', 'Authentication credentials were missing or incorrect.'),
('403', 'Forbidden', '<span class=\"label label-warning\">Forbidden</span>', 'The request is understood, but it has been refused or access is not allowed.'),
('404', 'Not Found', '<span class=\"label label-warning\">Not Found</span>', 'The URI requested is invalid or the resource requested, such as a user, does not exist. Also returned when the requested format is not supported by the requested method.'),
('405', 'Method Not Allowed', '<span class=\"label label-warning\">Method Not Allowed</span>', 'The request was made to a resource using an HTTP request method that is not supported.'),
('409', 'Conflict', '<span class=\"label label-warning\">Conflict</span>', 'The request could not be processed because it conflicts with some established rule of the system. For example, a person may not be added to a room more than once.'),
('415', 'Unsupported Media Type', '<span class=\"label label-warning\">Unsupported Media Type</span>', 'The request was made to a resource without specifying a media type or used a media type that is not supported.'),
('429', 'Too Many Requests', '<span class=\"label label-warning\">Too Many Requests</span>', 'Too many requests have been sent in a given amount of time and the request has been rate limited. A Retry-After header should be present that specifies how many seconds you need to wait before a successful request can be made.'),
('500', 'Internal Server Error', '<span class=\"label label-danger\">Internal Server Error</span>', 'Something went wrong on the server'),
('502', 'Bad Gateway', '<span class=\"label label-danger\">Bad Gateway</span>', 'The server received an invalid response from an upstream server while processing the request. Try again later.'),
('503', 'Service Unavailable', '<span class=\"label label-danger\">Service Unavailable</span>', 'Server is overloaded with requests. Try again later.'),
('200', 'OK', '<span class=\"label label-success\">Success</span>', 'Successful request with body content.'),
('204', 'No Content', '<span class=\"label label-success\">No Content</span>', 'Successful request without body content.'),
('400', 'Bad Request', '<span class=\"label label-warning\">Bad Request</span>', 'The request was invalid or cannot be otherwise served. An accompanying error message will explain further.'),
('401', 'Unauthorized', '<span class=\"label label-warning\">Unauthorized</span>', 'Authentication credentials were missing or incorrect.'),
('403', 'Forbidden', '<span class=\"label label-warning\">Forbidden</span>', 'The request is understood, but it has been refused or access is not allowed.'),
('404', 'Not Found', '<span class=\"label label-warning\">Not Found</span>', 'The URI requested is invalid or the resource requested, such as a user, does not exist. Also returned when the requested format is not supported by the requested method.'),
('405', 'Method Not Allowed', '<span class=\"label label-warning\">Method Not Allowed</span>', 'The request was made to a resource using an HTTP request method that is not supported.'),
('409', 'Conflict', '<span class=\"label label-warning\">Conflict</span>', 'The request could not be processed because it conflicts with some established rule of the system. For example, a person may not be added to a room more than once.'),
('415', 'Unsupported Media Type', '<span class=\"label label-warning\">Unsupported Media Type</span>', 'The request was made to a resource without specifying a media type or used a media type that is not supported.'),
('429', 'Too Many Requests', '<span class=\"label label-warning\">Too Many Requests</span>', 'Too many requests have been sent in a given amount of time and the request has been rate limited. A Retry-After header should be present that specifies how many seconds you need to wait before a successful request can be made.'),
('500', 'Internal Server Error', '<span class=\"label label-danger\">Internal Server Error</span>', 'Something went wrong on the server'),
('502', 'Bad Gateway', '<span class=\"label label-danger\">Bad Gateway</span>', 'The server received an invalid response from an upstream server while processing the request. Try again later.'),
('503', 'Service Unavailable', '<span class=\"label label-danger\">Service Unavailable</span>', 'Server is overloaded with requests. Try again later.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_groups`
--
ALTER TABLE `ad_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_group_mapping`
--
ALTER TABLE `ad_group_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocked_contacts`
--
ALTER TABLE `blocked_contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `bots`
--
ALTER TABLE `bots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bot_allowed_domain`
--
ALTER TABLE `bot_allowed_domain`
  ADD UNIQUE KEY `botid` (`botid`,`domainid`);

--
-- Indexes for table `bot_webhook`
--
ALTER TABLE `bot_webhook`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `webhookid` (`webhookid`,`botid`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emails` (`emails`);

--
-- Indexes for table `contact_access_group_response`
--
ALTER TABLE `contact_access_group_response`
  ADD UNIQUE KEY `id` (`id`,`contactid`,`botid`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `domain` (`domain`);

--
-- Indexes for table `excluded_spaces`
--
ALTER TABLE `excluded_spaces`
  ADD UNIQUE KEY `id` (`id`,`botid`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `keyword` (`keyword`);

--
-- Indexes for table `feedback_entries`
--
ALTER TABLE `feedback_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_entry_comment`
--
ALTER TABLE `feedback_entry_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_entry_vote`
--
ALTER TABLE `feedback_entry_vote`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `entry_id` (`entry_id`,`email`);

--
-- Indexes for table `feedback_topic`
--
ALTER TABLE `feedback_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generic_feedback`
--
ALTER TABLE `generic_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_contacts`
--
ALTER TABLE `group_contacts`
  ADD UNIQUE KEY `unique_index` (`contactid`,`groupid`);

--
-- Indexes for table `group_spaces`
--
ALTER TABLE `group_spaces`
  ADD UNIQUE KEY `groupid` (`groupid`,`spaceid`,`botid`);

--
-- Indexes for table `heartbeat`
--
ALTER TABLE `heartbeat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `joinable_space`
--
ALTER TABLE `joinable_space`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `spaceid` (`spaceid`,`botid`),
  ADD KEY `id` (`spaceid`);

--
-- Indexes for table `log_bot`
--
ALTER TABLE `log_bot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_users`
--
ALTER TABLE `log_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue_subtasks`
--
ALTER TABLE `queue_subtasks`
  ADD PRIMARY KEY (`subtask_id`);

--
-- Indexes for table `queue_task`
--
ALTER TABLE `queue_task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`keyword`,`botid`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_messages`
--
ALTER TABLE `saved_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_status`
--
ALTER TABLE `service_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ad_groups`
--
ALTER TABLE `ad_groups`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ad_group_mapping`
--
ALTER TABLE `ad_group_mapping`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blocked_contacts`
--
ALTER TABLE `blocked_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bot_webhook`
--
ALTER TABLE `bot_webhook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedback_entries`
--
ALTER TABLE `feedback_entries`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedback_entry_comment`
--
ALTER TABLE `feedback_entry_comment`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedback_entry_vote`
--
ALTER TABLE `feedback_entry_vote`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedback_topic`
--
ALTER TABLE `feedback_topic`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `joinable_space`
--
ALTER TABLE `joinable_space`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_users`
--
ALTER TABLE `log_users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `saved_messages`
--
ALTER TABLE `saved_messages`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
