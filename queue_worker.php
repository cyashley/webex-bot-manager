<?php
include_once "public/includes/functions.inc.php";
$db_local = new Db();
$generate = new OutputEngine();
$spark = new SparkEngine();
$ldap = new LDAP();

$lockfile = 'temp/queue.lock';

$availabletasks = $db_local->select("SELECT DISTINCT task_id FROM queue_subtasks WHERE task_code = ''");
	
if (count($availabletasks)) {
    echo date('Y-m-d H:i:s') . " - STARTING - " . PHP_EOL;
    echo date('Y-m-d H:i:s') . " - ATTEMPTING TO LOCK QUEUE - " . PHP_EOL;
    if (queueLock("lock")) {
    	echo date('Y-m-d H:i:s') . " - INFO - QUEUE WAS LOCKED SUCCESSFULLY" . PHP_EOL;
		foreach($availabletasks as $key => $value) {
			$taskid = $value['task_id'];
			$task = $db_local->taskQueueFetchTask($taskid);
			$type = $task[0]['task_type'];
			$submitter = $task[0]['task_submitter'];
			$botid = $task[0]['botid'];
			$botresponse = $task[0]['botresponse'];
	
			echo date('Y-m-d H:i:s') . " - INFO - Sub Tasks with Parent Task ID " . $taskid . " Found" . PHP_EOL;
			$messagesinqueue = $db_local->taskQueueFetchSubtasks($taskid, "AND task_code = ''");
			
			if(!empty($messagesinqueue[0]['group_id'])) { $group = $messagesinqueue[0]['group_id']; }
	
			echo date('Y-m-d H:i:s') . " - INFO - Task Type for " . $taskid . " is " . $type . "" . PHP_EOL;
			if($type == 'messages')
			{
				$messagechunk = array_chunk($messagesinqueue, 50);
				
				foreach($messagechunk as $chunkcount => $chunkvalue)
				{
					var_dump($chunkvalue);
					$result = $spark->bulkSplitRequestPost_new($type, $chunkvalue, $botid, 50, 150, 10, 150);
					if($result == 1)
					{
						echo date('Y-m-d H:i:s') . " - INFO - Above SubTasks were successfully sent" . PHP_EOL;
					}
					else
					{
						echo date('Y-m-d H:i:s') . " - WARN - Issue processing above SubTasks" . PHP_EOL;
					}
					echo date('Y-m-d H:i:s') . " - INFO - Sleeping for 10 Seconds before processing the next chunk of messages" . PHP_EOL;
					sleep(10);
				}
			}
			elseif($type == 'addtolocal')
			{
				$messagechunk = array_chunk($messagesinqueue, 30);
				foreach($messagechunk as $chunkcount => $chunkvalue)
				{
					var_dump($chunkvalue);
					$result = $spark->bulkSplitRequestPost_new($type, $chunkvalue, $botid, 50, 150, 10, 150, $group);
					if($result == 1)
					{
						echo date('Y-m-d H:i:s') . " - INFO - Above SubTasks were successfully sent" . PHP_EOL;
					}
					else
					{
						echo date('Y-m-d H:i:s') . " - WARN - Issue processing above SubTasks" . PHP_EOL;
					}
					echo date('Y-m-d H:i:s') . " - INFO - Sleeping for 10 Seconds before processing the next chunk of contacts" . PHP_EOL;
					sleep(10);
				}
			}
			elseif($type == 'adgroup')
			{
				$connection = $ldap->connect($config['ADserver'],$config['ADport']);
				echo date('Y-m-d H:i:s') . " - INFO - Connecting to Configured AD/LDAP Server" . PHP_EOL;
	
				// Bind with LDAP instance
				$ldap->bind($connection,$config['ADuser'],$config['ADpass']);
				echo date('Y-m-d H:i:s') . " - INFO - Binding to AD/LDAP Server" . PHP_EOL;
				
				// Search with a wildcard
				echo date('Y-m-d H:i:s') . " - INFO - Searching for AD/LDAP Group " . $messagesinqueue[0]['task_json'] . "" . PHP_EOL;
				$GroupSearch = $ldap->search($connection,$config['BaseDN'],'CN=' . $messagesinqueue[0]['task_json']);
				$GroupSearchDN = $GroupSearch[0]['distinguishedname'][0];
				
				
				$subtaskid = $messagesinqueue[0]['subtask_id'];
				$group = $messagesinqueue[0]['group_id'];
	
				if($GroupSearch['count'] > 0)
				{
					echo date('Y-m-d H:i:s') . " - INFO - Found Group" . PHP_EOL;
					echo date('Y-m-d H:i:s') . " - INFO - Performing a recursive search" . PHP_EOL;
					$filter = "(&(objectCategory=Person)(sAMAccountName=*)(memberof:1.2.840.113556.1.4.1941:={$GroupSearchDN}))";
					$ADMembers = $ldap->search($connection,$config['BaseDN'],$filter,array('givenName','cn','mail'));
	
					$result[] = $db_local->query("UPDATE queue_subtasks SET task_code = '200', task_results = 'Group Found, Fetching Users' WHERE subtask_id = '$subtaskid'");
	
					foreach($ADMembers as $key=> $value)
					{
						echo date('Y-m-d H:i:s') . " - INFO - Gathering found users email addresses" . PHP_EOL;
	
						if($value['mail'][0] != NULL)
						{
	
							$email = $value['mail'][0];
	
							echo date('Y-m-d H:i:s') . " - INFO - Generating Sub Task IDs for each user" . PHP_EOL;
							$contactsubtaskid = $db_local->generatesubtaskid();
	
							echo date('Y-m-d H:i:s') . " - INFO - Adding user emails to subtask queue" . PHP_EOL;
							$result = $db_local->query("INSERT INTO queue_subtasks (subtask_id, task_id, bot_id, subtask_type, task_json, group_id) VALUES ('$contactsubtaskid', '$taskid', '$botid', 'addtolocal', '$email', '$group')");
						}
					}
				}
			}
			elseif($type == 'updatelocal')
			{
				$messagechunk = array_chunk($messagesinqueue, 85);
				foreach($messagechunk as $chunkcount => $chunkvalue)
				{
					var_dump($chunkvalue);
					$result = $spark->bulkSplitRequestPost_new($type, $chunkvalue, $botid, 1000, 150, 15, 100);
					if($result == 1)
					{
						echo date('Y-m-d H:i:s') . " - INFO - Above SubTasks were successfully sent" . PHP_EOL;
					}
					else
					{
						echo date('Y-m-d H:i:s') . " - WARN - Issue processing above SubTasks" . PHP_EOL;
					}
					echo date('Y-m-d H:i:s') . " - INFO - Sleeping for 10 Seconds before processing the next chunk of contacts" . PHP_EOL;
					sleep(10);
				}
			}
			//Check if the task was created via bot
			if ($botresponse) {
				$message = $db_local->taskQueueGenerateBotResponse($taskid);
				$spark->messageSend($spark->messageBlob($message, $submitter, $botid));
			}
		}
		if (queueLock("unlock")) {
			echo date('Y-m-d H:i:s') . " - INFO - Queue was unlocked" . PHP_EOL;
		} else echo date('Y-m-d H:i:s') . " - WARN - Unable to unlock the queue" . PHP_EOL;
	} else {
		echo date('Y-m-d H:i:s') . " - INFO - Task queue is busy with another task (locked)" . PHP_EOL;
		if (queueLock("isdeadlocked")) {
			echo date('Y-m-d H:i:s') . " - WARN - Task queue seems to be in a deadlock" . PHP_EOL;
			if (queueLock("unlock")) {
				echo date('Y-m-d H:i:s') . " - INFO - Lock removed, deadlock resolved!" . PHP_EOL;
			} else {
				echo date('Y-m-d H:i:s') . " - ERROR - Task queue is in an unrecoverable deadlock!" . PHP_EOL;
			}
		}
	}
} else {
	if (queueLock()) {
		if (queueLock("isdeadlocked")) {
			echo date('Y-m-d H:i:s') . " - WARN - Task queue seems to be in a deadlock" . PHP_EOL;
			if (queueLock("unlock")) {
				echo date('Y-m-d H:i:s') . " - INFO - No new tasks in queue, queue is still locked over one hour.. unlocking!" . PHP_EOL;
			} else {
				echo date('Y-m-d H:i:s') . " - ERROR - Not able to unlock the queue..!" . PHP_EOL;
			}
		}
		
	}
}
	


?>