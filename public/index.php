<?php
//setcookie('cross-site-cookie', 'name', ['samesite' => 'None', 'secure' => true]);
//Set required DB version for this WBM version
$NewDBVersion = '4';

$installfilecheck = '../config_ini.php';

if (!file_exists($installfilecheck)) {
    header("Location: install.php");
} 

   include('session.php');
   $pos_color = "#007203";
   $neg_color = "red";
   $inactivecolor = "lightgray";
   $site_path = "sites/";
   $gradrul = "<hr class='gradient'>";
   $infocolor = "#0C2C52";
   ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$link_confirm = "onclick='return confirm(\"Are you sure? This operation cannot be undone!\")'";
//include "includes/functions.inc.php";
if (!isset($_SESSION['status'])) {
  $_SESSION['status'] = "";
}

$windowid = (isset($_GET['id'])) ? $_GET['id'] : "bots";

$gitBasePath = '../.git'; 
$gitStr = file_get_contents($gitBasePath.'/HEAD');
$gitBranchName = rtrim(preg_replace("/(.*?\/){2}/", '', $gitStr));                                                                                            
$gitPathBranch = $gitBasePath.'/refs/heads/'.$gitBranchName;
$gitHash = file_get_contents($gitPathBranch);
$gitDate = date('Y-m-d', filemtime($gitPathBranch));

switch ($windowid) {
  case "login":
    $page = "{$site_path}login.php";
    break;
  case "setup":
    $page = "{$site_path}setup.php";
    break;
  case "administration":
    $page= "{$site_path}administration.php";
    break;
  case "logs":
    $page= "{$site_path}logs.php";
    break;
  case "contacts":
    $page = "{$site_path}contacts.php";
    break;
  case "spaces":
    $page = "{$site_path}spaces.php";
    break;
  case "groups":
    $page = "{$site_path}groups.php";
    break;
  case "feedback":
    $page = "{$site_path}feedback.php";
    break;
  case "messages":
    $page = "{$site_path}messages.php";
    break;
  case "bots":
    $page = "{$site_path}bots.php";
    break;
  case "docs":
    $page = "{$site_path}docs.php";
    break;
  case "devices":
    $page = "{$site_path}devices.php";
    break;
  case "upgrade":
    $page = "{$site_path}upgrade.php";
    break;
  case "tasks":
    $page = "{$site_path}tasks.php";
    break;
  default:
    $page= "{$site_path}bots.php";
    break;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="webexteams.ico">
  <title>Webex Bot Manager | <?php echo ucfirst($_GET['id']); ?></title>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- pace-progress -->
  <link rel="stylesheet" href="plugins/pace-progress/themes/black/pace-theme-flat-top.css">
  <!-- SweetAlert2 -->
  <script src="plugins/sweetalert2/sweetalert2.css"></script>
  <script src="plugins/sweetalert2/sweetalert2.all.js"></script>
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Editor -->
  <link rel="stylesheet" href="plugins/editormd/css/editormd.css" />

  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <link rel="stylesheet" href="dist/wbm.css">
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<script language="JavaScript">
function toggle(source, name) {
    checkboxes = document.getElementsByName(name);
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }
function loading(id, loadingtext)
{
  ele = document.getElementById(id);
  ele.style.display="block"
  ele.innerHTML='<i class="fa fa-spinner fa-spin"></i> ' + loadingtext;
}

</script>
</head>
<body class="hold-transition sidebar-mini pace-white">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-wbm navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <!-- <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right wbm-ciscoblue">
          <span class="dropdown-header" style="background-color: #FFFFFF">Logged in: <?php echo $_SESSION['name']; ?></span>
          <div class="dropdown-divider"></div>
          <div align="center" class="mt-3 pb-3 mb-1">
          <img src="<?php echo $_SESSION['avatar']; ?>" class="img-circle elevation-2" width="120px" height="120px" style="background-color: #FFFFFF" alt="User Image">
        </div>
         
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a> -->
          <!-- <div style="background-color: #FFFFFF">
          <?php echo "<a href='index.php?id=contacts&contactid={$_SESSION['userid']}' class='btn btn-sm btn-primary'>View Profile</a>"; ?>
          <a href="logout.php" class="btn btn-sm btn-primary float-right">Sign out</a>
        </div>
        </div>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-user"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4 text-sm">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="images/static/webexteams.png" alt="AdminLTE Logo" class="brand-image img-circle mt-1">
      <span class="brand-text font-weight-light text-lg">Webex Bot Manager</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image mt-2">
          <img src="images/static/gitlogo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <?php echo "<font color='white'>BRANCH: " . $gitBranchName . "<br>Version Date : " . $gitDate . "</font>"; ?>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="index.php?id=tasks" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>My Tasks</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?id=bots" class="nav-link">
              <i class="nav-icon fab fa-android"></i>
              <p>My Bots</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?id=messages" class="nav-link">
              <i class="nav-icon fas fa-paper-plane"></i>
              <p>Send Teams Message</p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-close">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-arrow-right"></i>
              <p>Activity
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="index.php?id=contacts" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index.php?id=groups" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Groups</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index.php?id=feedback" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Feedback</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index.php?id=spaces" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Spaces & Teams</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index.php?id=devices" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Devices</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview menu-close">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>Administration
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="index.php?id=administration&sub=default" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Settings</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index.php?id=logs" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Logs</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index.php?id=docs" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Guide</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper text-sm">
<?php echo issetor($warning) . issetor($warning1);
      $feedback = issetor($_GET['feedback']);
      if (file_exists('install.php')) {
          echo '
                    <div class="alert alert-danger alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
          <button type="button" class="close" data-dismiss="alert" >&times;</button>
          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          We\'ve detected that install.php file exists. For security reasons, please delete it ASAP!
        </div>
      ';
      }
      echo $db_local->verifydbversion($NewDBVersion);
      if (!empty($feedback)) {
        echo msgList($feedback);
      }
      if($db_local->adminCheckIfMaintenance() > 0)
      {
        echo '
                    <div class="alert alert-warning" style="margin-left: 10px; margin-right: 10px;">
          <h4><i class="fas fa-exclamation-triangle"></i> Warning!</h4>
          WBM is currently in Maintenance Mode.
        </div>
      ';
      }
      ?>  
      
      <?php include $page; ?>
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      
      <div align="center">
      <img src="<?php echo $_SESSION['avatar']; ?>" class="img-circle elevation-2" width="120px" height="120px" style="background-color: #FFFFFF" alt="User Image">
      <br />
      <h5><?php echo $_SESSION['name']; ?></h5>
    </div>
        <?php echo "<a href='index.php?id=contacts&contactid={$_SESSION['userid']}' class='btn btn-sm btn-block btn-primary' style='color: white;'>View Profile</a>"; ?>
        <a href="logout.php" class="btn btn-sm btn-block btn-danger float-right" style="color: white;">Sign out</a>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- Default to the left -->
    <strong><?php echo tooltip('Credits', 'Webex Bot Manager is created by Magnus Ohm (mohm@cisco.com). Credits to Yusof Yaghi (yyaghi@cisco.com) for the amazing front-end work!', 'fa-info-circle', 'top')?> Credits</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->


<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
  <!-- pace-progress -->
<script src="plugins/pace-progress/pace.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- Editor -->
<script src="plugins/editormd/editormd.min.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<script src="wbm.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#tasks").DataTable();
  });
</script>
</body>
</html>