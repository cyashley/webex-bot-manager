  $(function () {
	  const urlfields = window.location.search
	  
    // Contacts Table 
    $(document).ready(function (){   
      var table = $('#contacts').DataTable({
        'ajax': 'server_processing.php?type=table&table=contacts&primarykey=id', 
        'processing': true,
        'serverSide': true,
        'scrollX': true,
        'responsive': true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        'dom' : "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-3'><'col-sm-12 col-md-4'f><'col-sm-12 col-md-1'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", 
        'columns': [
          { "orderable": false },
          { "orderable": false },
          null,
          null,
          null,
          { "orderable": false }
        ],
        buttons: [
        { extend: 'csv', text: '<i class="fas fa-download"></i>', className: 'btn btn-primary btn-xs', exportOptions: { columns: [ 2, 3, 4 ] }  }
        ],
        'order': [1, 'asc']
      });

     // Handle click on "Select all" control
     $('#contacts-select-all').on('click', function(){
        // Check/uncheck all checkboxes in the table
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
     });

     // Handle click on checkbox to set state of "Select all" control
     $('#contacts tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
           var el = $('#contacts-select-all').get(0);
           // If "Select all" control is checked and has 'indeterminate' property
           if(el && el.checked && ('indeterminate' in el)){
              // Set visual state of "Select all" control 
              // as 'indeterminate'
              el.indeterminate = true;
           }
        }
     });
      
     $('#frm-contacts').on('submit', function(e){
        var form = this;

        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function(){
           // If checkbox doesn't exist in DOM
           if(!$.contains(document, this)){
              // If checkbox is checked
              if(this.checked){
                 // Create a hidden element 
                 $(form).append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', this.name)
                       .val(this.value)
                 );
              }
           } 
        });
     });
  });

    $(document).ready(function (){   
      var table = $('#queue_task').DataTable({
        'processing': true,
        'scrollX': true,
        'order': [0, 'desc']
      });
    });

    $(document).ready(function (){   
      var table = $('#subtasks').DataTable({
        'processing': true,
        'scrollX': true,
        'order': [0, 'asc']
      });
    });

    $(document).ready(function (){   
      var table = $('#bots').DataTable({
        'processing': true,
        'scrollX': true,
        'order': [0, 'asc']
      });
    });
    
    $(document).ready(function (){   
        var table = $('#spacelist').DataTable({
          'processing': true,
          'scrollX': true,
          'order': [0, 'desc']
        });
      });

    $(document).ready(function (){
      var table = $('#topics').DataTable({
        'ajax': 'server_processing.php?type=table&table=feedback_topic&primarykey=id&'+urlfields, 
        'processing': true,
        'serverSide': true,
        'scrollX': true,
        'order': [0, 'asc']
      });
    });

    $(document).ready(function() {
      // Setup - add a text input to each footer cell
      $('#logs tfoot th').each(function() {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
      });

      var table = $('#logs').dataTable({
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "desc" ]],
        "ajax": "server_processing.php?type=table&table=activity_log&primarykey=id",
        'responsive': true,
        'dom' : "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-3'><'col-sm-12 col-md-4'f><'col-sm-12 col-md-1'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", 
        buttons: [
        { extend: 'csv', text: '<i class="fas fa-download"></i>', className: 'btn btn-primary btn-xs' }
        ],
        initComplete: function() {
          var api = this.api();

          // Apply the search
          api.columns().every(function() {
            var that = this;

            $('input', this.footer()).on('keyup change', function() {
              if (that.search() !== this.value) {
                that
                  .search(this.value)
                  .draw();
              }
            });
          });
        }
      });
    });

        $(document).ready(function() {
      // Setup - add a text input to each footer cell
      $('#devices tfoot th').each(function() {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
      });

      var table = $('#devices').dataTable({
        "processing": true,
        "order": [[ 0, "desc" ]],
        'responsive': true,
        'dom' : "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-3'><'col-sm-12 col-md-4'f><'col-sm-12 col-md-1'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", 
        buttons: [
        { extend: 'csv', text: '<i class="fas fa-download"></i>', className: 'btn btn-primary btn-xs' }
        ],
        initComplete: function() {
          var api = this.api();

          // Apply the search
          api.columns().every(function() {
            var that = this;

            $('input', this.footer()).on('keyup change', function() {
              if (that.search() !== this.value) {
                that
                  .search(this.value)
                  .draw();
              }
            });
          });
        }
      });
    });


$(document).ready(function(){

 $('.botresponses').click(function(){
   
   var responseid = $(this).data('id');
   var botid = $(this).data('botid');

   // AJAX request
   $.ajax({
    async: true,
    url: 'server_processing.php?type=ajax',
    type: 'post',
    data: {responseid: responseid, botid: botid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#botresponses').modal('show'); 
    }
  });
 });
//  $('#botresponses').on('hidden.bs.modal', function () {
//  location.reload();
// })
});


    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //ToolTips
    $('[data-toggle="popover"]').popover({
    container: 'body'
    })
    
    //Bootstrap Duallistbox users (groups.php)
    $('.duallistboxuser').bootstrapDualListbox({infotext: false, nonSelectedListLabel: 'Available Users', selectedListLabel: 'Group members'})
     //Bootstrap Duallistbox spaces (groups.php)
    $('.duallistboxspace').bootstrapDualListbox({infotext: false, nonSelectedListLabel: 'Available Spaces', selectedListLabel: 'Group members'})
    //Bootstrap Duallistbox spaces (groups.php)
    $('.duallistbox').bootstrapDualListbox({infotext: false})
    
    

  })
  // $('#botreponses').on('show.bs.modal', function (event) {
  // var button = $(event.relatedTarget) // Button that triggered the modal
  // var id = button.data('id') // Extract info from data-* attributes
  // var json = button.data('json')
  // var obj = JSON.parse(json)
  // // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  // var modal = $(this)
  // modal.find('.modal-title').text('New message to ' + json)
  // modal.find('.modal-body input').html(id)
  // })

  $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });


