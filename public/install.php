<?php

error_reporting(0);
@ini_set('display_errors', 0);

$phpver = phpversion();

if ($phpver >= '7.0')
{
    $phpver = 'Passed';
}
else
{
    $phpver =  'Your php version is not supported - Currently PHP is ' . $phpver;
}
$name = 'curl';

$extensions = get_loaded_extensions();

$curl = array_search("curl", $extensions);
if(!empty($curl))
{
    $curlcheck = 'Passed';
}
else
{
    $curlcheck =  'curl is not installed';
}

$installfilecheck = '../config_ini.php';

switch ($_GET['sub']) {
    case 'db':
        if (file_exists($installfilecheck)) {
            header("Location: install.php?sub=primarybot");
        }
        break;
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Webex Bot Manager - Install</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <link rel="stylesheet" href="dist/wbm.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="install-box">
  <div class="login-logo">
    <a href="index.php"><img  src="images/static/webexteams.png" height="100px" width="90px">
    	<br><b>Webex </b>Bot Manager</a>
  </div>
  <!-- /.login-logo -->
	<div class="card card-primary card-outline">
		<div class="card-header">
			<h3 class="card-title">Webex Bot Manager - Installation</h3>
			<div class="install-card-tools">
				<?php 
							if(empty($_GET['sub']))
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
										Step 1/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'db')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
										Step 2/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'proxy')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
										Step 3/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'primarybot')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 67%;">
										Step 4/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'createadmin')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 83%;">
										Step 5/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'finished')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
										Step 6/6
									</div>
								</div>';
							}
						?>
						</div>
						<!-- /.card-tools -->
					</div>
					<!-- /.card-header -->
					<div class="card-body">
					<?php 
						if(empty($_GET['sub']))
						{
							echo '
								<div class="row">
									<div class="col-md-5">
									   PHP version Check (>= 7.0)
									</div>
									<div class="col-md-5">
									   ' . $phpver . '
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
									   PHP CURL module Installed
									</div>
									<div class="col-md-5">
									   ' . $curlcheck  . '
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
									Web Server has write access to directory
									</div>
									<div class="col-md-5">';
									$rootdoc = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
                           $subrootdoc = str_replace("/public", "", $rootdoc);
                           if (is_writable($rootdoc)) {
                              echo "$rootdoc <br>";
                              echo 'Houston, we have write permission!<br>';
                              echo (is_writable($rootdoc."/images/bots")) ? "": "Cannot write to bot avatar folder<br>";
                              echo (is_writable($subrootdoc."/temp")) ? "": "Cannot write to temp lock folder<br>";
                           } else {
                              echo "Root directory is: $rootdoc";
                              echo "<br>";
                              echo 'Houston, we have a problem! The root directory is not writable!';
                           }
                           echo '</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
								';
								if ($phpver == 'Passed' && $curlcheck == 'Passed' && is_writable($rootdoc))
								{
									echo '<a class="btn btn-primary pull-right" href="install.php?sub=db">Next</a>';
								}
							echo '</div>';
					}
					elseif ($_GET['sub'] == 'db')
					{					
						echo '<b>This process will create all the tables needed for Webex Bot Manager</b><br>';
						echo '<br><b>Please enter your database credentials</b><br>';
						echo '
						<form name="dbcreds" method="post" action="" enctype="multipart/form-data">
							<div class="row">
                        <div class="col-md-3">
                        Database Name:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="text" name="dbname" class="form-control form-control-sm" placeholder="dabase name" required/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                        Database Username:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="username" name="dbuser" class="form-control form-control-sm" placeholder="username" required/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                        Database Password:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="password" name="dbpass" class="form-control form-control-sm" placeholder="password" required/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                        Database Host:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="text" name="dbhost" class="form-control form-control-sm" placeholder="localhost" value="localhost" required/>
                        </div>
                     </div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer">';
							if(isset($_POST['dbload']))
                     {
                        $button = '<br><button type="submit" name="dbload" class="btn btn-primary">Re-try operation</button>';
                         $conn = @new mysqli($_POST['dbhost'], $_POST['dbuser'], $_POST['dbpass']);
                         if ($conn->connect_errno) {
                             echo '<font color="red">Failed to connect to MySQL: ';
                             echo '<br>Error: ' . $conn->connect_error . '</font>';
                             echo $button;
                             die();
                         }
                         else {
                             echo "<font color='green'>Connection successful!</font>";
                             $dbname = $_POST['dbname'];
                             $db = $conn->select_db($dbname);
                             if (!$db) {
                                 echo "<br><font color='orange'>Database: $dbname did not exist, trying to create the database..</font>";
                                 if ($conn->query('CREATE DATABASE IF NOT EXISTS `' . $dbname . '` CHARACTER SET utf8 COLLATE utf8_general_ci') === TRUE) {
                                     echo "<br><font color='green'>Successfully created database: $dbname</font>"; 
                                     $db = $conn->select_db($dbname);
                                     if (!$db) {
                                         echo "<br><font color='green'>Something went wrong selecting the database: $dbname</font>";
                                         echo $button;
                                         die();
                                     }
                                 } else {
                                     echo "<br><font color='red'>Failed to create database $dbname : ".$conn->error."</font>";
                                     echo $button;
                                     die();
                                 }
                             } else {
                                 echo "<br><font color='green'>$dbname exists</font>";
                                 $tables = $conn->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '$dbname'");
                                 $list = array();
                                 while($row = $tables->fetch_assoc()) {
                                     $list[] = $row['TABLE_NAME'];
                                 }        
                                 if (count($list)) {
                                     echo $conn->error;
                                     echo "<br><font color='red'>Database was not empty, unable to proceed... $dbname has (".count($list).") tables </font>";  
                                     echo $button;
                                     die();
                                 } else {
                                     echo "<br><font color='green'>$dbname is empty, creating tables..</font>";
                                 }
                             }
                         }
                        //Credit to https://dev.to/erhankilic/how-to-import-sql-file-with-php--1jbc
                       
                         $configFile = '../config_ini.php'; 
                         $config_ini = fopen($configFile, "w") or die("Unable to create config.ini file. Please check apache has write permissions to the directory.");
                        
                         $config_data = '<?php 
$config = array("dbhost"=>"'.$_POST['dbhost'].'", 
"dbname"=>"'.$_POST['dbname'].'",
"username"=>"'.$_POST['dbuser'].'",
"password"=>"'.$_POST['dbpass'].'",
"proxyurl"=>"",
"proxyuserpass"=>"",
"base_dir"=>"'.$subrootdoc.'");
?>';
                        fwrite($config_ini, $config_data);
                     
                        //Name of the SQL file
                        $sqlTemplate = '../easybot-sql-template.sql';
                        // Temporary variable, used to store current query
                        $templine = '';
                        // Read in entire file
                        $lines = file($sqlTemplate);
                        // Loop through each line
                        foreach ($lines as $line) {
                        // Skip it if it's a comment
                           if (substr($line, 0, 2) == '--' || $line == '')
                              continue;

                        // Add this line to the current segment
                           $templine .= $line;
                        // If it has a semicolon at the end, it's the end of the query
                           if (substr(trim($line), -1, 1) == ';') {
                              // Perform the query
                              $conn->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $conn->error() . '<br /><br />');
                              // Reset temp variable to empty
                              $templine = '';
                           }
                        }
                        echo "<br><font color='green'>Tables imported successfully to $dbname</font>";
                        $conn->close($conn);
                        echo '<a class="btn btn-primary float-right" href="install.php?sub=proxy">Next</a>';
                     }
                     else
                     {
                        echo '<button type="submit" name="dbload" class="btn btn-primary">Connect to DB and load tables</button>';
                     }
                  echo '</form>
                  </div>
                  ';
               }
				elseif ($_GET['sub'] == 'proxy')
					{
						echo '<b>If WBM is located in an area in your network that requires a proxy server to reach the internet, fill in the below. If not, skip.</b><br>';
						echo '<br><b>Please enter your proxy information - Credentials are not needed for a lot of proxy servers</b><br>';
						echo '
						<form name="proxyserver" method="post" action="" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-2">
									Proxy Host:
									</div>
									<div class="col-md-5 mb-1">
									<input type="text" name="proxyhost" class="form-control form-control-sm"/>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
									Proxy Port:
									</div>
									<div class="col-md-5 mb-1">
									<input type="text" name="proxyport" class="form-control form-control-sm"/>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
									Proxy Username:
									</div>
									<div class="col-md-5 mb-1">
									<input type="username" name="proxyuser" class="form-control form-control-sm" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
									Proxy Password:
									</div>
									<div class="col-md-5 mb-1">
									<input type="password" name="proxypass" class="form-control form-control-sm" />
									</div>
								</div>
							</div>
							<div class="card-footer">';
							function checkinternet( $proxyurl, $proxycreds)
							{
								$url = 'www.webex.com';
								$ch = curl_init($url);
								curl_setopt($ch, CURLOPT_NOBODY, true);
								curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
								curl_setopt($ch, CURLOPT_PROXY, $proxyurl);
								curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxycreds);
								curl_exec($ch);
								$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								curl_close($ch);
								if (200==$retcode) {
									echo '<font color="green">Houston, we have internet connectivity!</font>';
									echo '<a class="btn btn-primary float-right" href="install.php?sub=primarybot">Next</a>';
									$rootdoc = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
									$subrootdoc = str_replace("/public", "", $rootdoc);
									require $subrootdoc . '/config_ini.php';
									$rootdoc = $config['base_dir'];
									$dbhost = $config['dbhost'];
									$dbuser = $config['username'];
									$dbpass = $config['password'];
									$dbname = $config['dbname'];
									$configFile = $subrootdoc.'/config_ini.php'; 
									$config_ini = fopen($configFile, "w") or die("Unable to create config.ini file. Please check apache has write permissions to the directory.");
									$config_data = '<?php 
$config = array("dbhost"=>"'.$dbhost.'", 
"dbname"=>"'.$dbname.'",
"username"=>"'.$dbuser.'",
"password"=>"'.$dbpass.'",
"proxyurl"=>"'.$proxyurl.'",
"proxyuserpass"=>"'.$proxycreds.'",
"base_dir"=>"'.$subrootdoc.'");
?>';
				
								fwrite($config_ini, $config_data) or die("Unable");
									
								} 
								else 
								{
									echo '<font color="red">Ruh Roh! I\'m unable to get to webex.com </font> <br><br>';
									echo '<button type="submit" name="proxy" class="btn btn-danger">Try Again</button>';
								}
							}
							if(isset($_POST['proxy']))
							{
								if(!empty($_POST['proxyhost']) && !empty($_POST['proxyport']))
								{
									$proxyurl = $_POST['proxyhost'] . ":" . $_POST['proxyport'];
								}
								if(!empty($_POST['proxyuser']) && !empty($_POST['proxyuser']))
								{
									$proxycreds = $_POST['proxyuser'] . ":" . $_POST['proxypass'];
								}
								checkinternet($proxyurl, $proxycreds);
							}
							else
							{
								echo '<button type="submit" name="proxy" class="btn btn-primary">Check Internet connection</button>';
							}
							echo '
							</div>
						</form>';
					}
					elseif ($_GET['sub'] == 'primarybot')
					{
						include "includes/functions.inc.php";
						$db_local = new Db();
						$generate = new OutputEngine();
						$spark = new SparkEngine();
						echo '
						<b>Primary Bot Setup!</b>
						<br>
						<br>
						<form name="primarybot" method="post" action="" enctype="multipart/form-data">
						
							<div class="row">
								<div class="col-md-3">
								Enter valid Bot Access Token:
								</div>
								<div class="col-md-5">
								<input type="text" class="form-control form-control-sm" value="" name="accesstoken"> 
								</div>
								<div class="col-md-4">
								<a tabindex="0" role="button" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="Bot Access Token" data-content="The Bot Access Token can be generated from under your profile in developer.webex.com">How do I get the bot Access Token?</a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
								Setup direct conversation webhook
								</div>
								<div class="col-md-5">
								<input type="checkbox" checked name="webhook_direct" value="1">
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
								Setup group conversation webhook
								</div>
								<div class="col-md-5">
								<input type="checkbox" checked name="webhook_group" value="1"> 
								</div>
							</div>
							<br>
						</div>
						
						<div class="card-footer">
						
						';
						if (isset($_POST['accesstoken'])) {
							$token = trim($_POST['accesstoken']);
							$botAdd = $spark->wizardAddBot($token, $primary=True);
							$bot = $db_local->botGetMainInfo();
							if ($botid = $bot['id']) {
								if (isset($_POST['webhook_direct'])) {
									$spark->webhookCreateQuick($botid, 'direct');
								}
								if (isset($_POST['webhook_group'])) {
									$spark->webhookCreateQuick($botid, 'group');
								}
								echo msgList("success-BotAdd");
								echo feedbackMsg("Primary bot added!", $botAdd, "info");
								echo '<a class="btn btn-primary float-right" href="install.php?sub=createadmin">Next</a>';
							}
							else {
								echo msgList("alert-BotAddAccess");
								var_dump($botAdd);
							}

						}
						else
						{
							echo '<input type="submit" class="btn btn-md btn-primary" value="Add primary bot">';
						}
						echo '</div>
						</form>';
					}
					elseif ($_GET['sub'] == 'createadmin')
					{
						echo '
						<b>WBM needs a VALID webex teams email to administer this site!</b><br>
						<br>
						<form method="post" action="" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-3">
								Teams Email Address
								</div>
								<div class="col-md-5">
									<input type="email" size="100" value="" class="form-control" name="useremail" required>
								</div>
							</div>
							</div>
							<div class="card-footer">
							 
						';
						include "includes/functions.inc.php";
						$db_local = new Db();
						$generate = new OutputEngine();
						$spark = new SparkEngine();
						$primaryBotInfo = $db_local->botGetMainInfo();
						if (empty($primaryBotInfo)) {
							$primaryFound = false;
						}
						else {
							$primaryFound = true;
							$botid = $primaryBotInfo['id'];
							if (empty($primaryBotInfo['access'])) {
								$tokenExists = false;
							}
							else {
								$tokenExists = true;
								$me = $spark->peopleGetMe($botid);
								$tokenValid = ($me['id'] == $botid) ? true:false;
							}
						}

						if(isset($_POST['useremail'])) {
							if (validateEmail($useremail = $db_local->quote($_POST['useremail']))) {
								$payload = array("sender"=>$botid, "recepientType"=>"email", "recepientValue"=>$useremail);
								$userDetails = $spark->peopleGet($payload);
								if (count($userDetails['items']) == 1) {
									$db_local->contactAdd($userDetails, $botid);
									if (!empty($userdata = $db_local->contactFetchContacts($userDetails['items'][0]['id']))) {
										$userid = $userdata[0]['id'];
										$password = generatePassword();
										$token = hash('md5', generatePassword());
										$hashpw = password_hash($password, PASSWORD_DEFAULT);
										$host = getHostUrl();
										$text = "Here is your new login passord to the [Webex Bot Manager]($host), please change the password in the user settings when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
										list($foo, $domain) = explode('@', $userdata[0]['emails']);
										$domainid = $db_local->acceptedDomainAdd(array('domain'=>$domain));
										if ($domainid){
											$db_local->botAddAllowedDomain($domainid, $botid);
										}
										$features = $db_local->adminGetFeatures();
										$feature_list = blockquote("Here are my currently enabled features, type the keyword to get a help list", "success");
										if (count($features)) {
											foreach ($features as $key => $value) {
												$db_local->adminSetFeature($value['id'], $botid);
												$feature_list .= "\n- {$value['keyword']}";
											}
										}
										$db_local->userUpdateSettings('update',$hashpw,$userid,'1');
										$db_local->userUpdateSettings('token',$token,$userid,'1');
										$db_local->botSetDefaultResponse("Hi, nice to meet you! This is my default response. You can change it and create more responses in the bot settings. You can also enable features in the bot responses tab. Have fun :)" ,$botid);
										if ($spark->messageSend($spark->messageBlob($text, $userid, $botid))) {
											echo msgList("success-Query");
											$spark->messageSend($spark->messageBlob($feature_list, $userid, $botid));
											sleep(2);
											$spark->messageSend($spark->messageBlob("You can login and start to administer the responses. Remember, you might want to disable some of the commands or put them behind an accessgroup to avoid misuse.", $userid, $botid));
										}
										else {
											echo msgList("success-Generic");
										}
										echo '<a class="btn btn-primary pull-right" href="install.php?sub=finished">Finish</a>';
									}
									else {
										echo msgList("warning-AddFailed");
									}
								}
								else {
									echo msgList("warning-UserNotFound");
								}
							}
							else {
								echo msgList("warning-Email");
							}
						}
						else 
						{
							echo '<input type="submit" class="btn btn-md btn-primary" value="Add site admin">
						</form>';
						}
						echo '</div>';
					}
					elseif ($_GET['sub'] == 'finished')
					{
						echo '<div align="center">';
						echo '<b>You\'ve completed the installation of the Webex Bot Manager!</b><br><br>';
						echo '<a href="index.php" class="btn btn-lg btn-primary">Click Me to Log in!</a>';
						echo '</div>';
					}
			?>
		</div>
	</div>
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

</body>
</html>
