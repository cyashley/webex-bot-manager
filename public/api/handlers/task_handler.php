<?php
include '../../includes/functions.inc.php';
$db = new Db();
$auth_header = issetor($_SERVER['HTTP_AUTHORIZATION']);
if (!empty($auth_header)) {
	list($type, $token) = explode(" ", $auth_header);
	if (!$db->adminCheckIfValidAPIToken($token)) {
		header("Status: 401 Unauthorized");
		$output = array('Error'=>'401 Unauthorized - incorrect token');
	} 
	else {
		if ($_GET['fetch'] == "next_task") {
			$qs = $db -> constructUpdateValues(	"heartbeat",
					array(	'id' => '1',
							'ip' => $_SERVER['REMOTE_ADDR'],
							'time' => get_timestamp()
					),
					"WHERE id = '1'");
			$db -> query($qs);
			$output = $db -> select_specific("SELECT * FROM tasks LIMIT 1");
		}
	}
	if ($_GET['fetch'] == "service") {
		$service = $db -> adminCheckServiceStatus();
		$maintenance = $db -> adminCheckIfMaintenance();
		$output = array("service"=>$service, "maintenance"=>$maintenance);
	}
}
else {
	header("Status: 401 Unauthorized");
	$output = array('Error'=>'401 Unauthorized - incorrect token');
}
header('Content-type: application/json');
echo json_encode($output, JSON_PRETTY_PRINT);
if (!empty($output[id])) {
	$output = $db -> query("DELETE FROM tasks WHERE id = {$output['id']}");
}
?>