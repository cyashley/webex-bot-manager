<?php 
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-type: application/json');

$db = new Db();
$spark = new SparkEngine();
$auth_header = $_SERVER['HTTP_AUTHORIZATION'];
$output = array('Error'=>'Empty');

list($type, $token) = explode(" ", $auth_header);

if (!$db->adminCheckIfValidAPIToken($token)) {
	header("Status: 401 Unauthorized");
	$output = array('Error'=>'401 Unauthorized - incorrect token');	
} 
else {
	//Handle incoming data
	$incoming = file_get_contents("php://input");
	$dec_incoming = json_decode($incoming,true);
	
	//Assign required variables (required incoming fields, groups, message and botid)
	$rooms_raw = $contacts_raw = array();
	$a = 0;
	$groups = $dec_incoming['groups'];
	$message = $dec_incoming['message'];
	$botid = $dec_incoming['botid'];
	$botinfo = $db->botFetchBots($botid);
	$output['botid'] = $botid;
	
	//The bot must exist, the message cannot be empty and there must be a group specified in the request
	if (count($botinfo) > 0 and !empty($message) and count($groups) > 0) {
		foreach ($groups as $key => $value) { //Handle multiple groups in a loop
			$group_details = $db->groupFetchGroups($value['id']); 
			if ($group_details[0]['botid']) {
				$space_members = $db->groupGetSpaceMembers($value['id'], $group_details[0]['botid']);
				foreach ($space_members as $spacekey => $spacevalue) {
					$rooms_raw[] = $spacevalue['spaceid'];
				}
			}
			$members = $db->groupGetMembers($value['id']);
			foreach ($members as $key => $value) {
				$contacts_raw[] = $value['contactid'];
			}
		}	
		if (count($contacts_raw) > 0) {
			$contacts_raw = array_unique($contacts_raw);
			
			foreach ($contacts_raw as $key => $value) {
				$m_query[$a]['url'] = $spark->getApiUrl('messages');
				$m_query[$a]['auth'] = $spark->authGet($botid);
				$m_query[$a]['method'] = "POST";
				$m_query[$a]['type'] = "json";
				$m_query[$a]['post'] = array();
				$m_query[$a]['post']['toPersonId'] = $value;
				$m_query[$a]['post']['markdown'] = $message;
				$a++;
			}
		}
		if (count($rooms_raw) > 0) {
			$rooms_raw = array_unique($rooms_raw);
			
			foreach ($rooms_raw as $key => $value) {
				$m_query[$a]['url'] = $spark->getApiUrl('messages');
				$m_query[$a]['auth'] = $spark->authGet($botid);
				$m_query[$a]['method'] = "POST";
				$m_query[$a]['type'] = "json";
				$m_query[$a]['post'] = array();
				$m_query[$a]['post']['roomId'] = $value;
				$m_query[$a]['post']['markdown'] = $message;
				$a++;
			}
		}
		if (count($m_query) > 0) {
			$result = $spark->data_multi_post($m_query);
			$report = $spark->check_multi_error($result);
			$output = array("success"=>"Successfully executed query","report"=>$report);
		}
	}
	else {
		$output = array('Error'=>'Did not receive the expected dataset (groups, message and botid)', 'received'=>$dec_incoming);
	}
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>