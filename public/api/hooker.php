<?php

include '../includes/functions.inc.php';
//Start classes
$db_local = new Db();
$spark = new SparkEngine();

$maintenance = ($db_local->adminCheckIfMaintenance()) ? "Enabled" : "Disabled";
$warning_mode = ($db_local->adminCheckIfWarning()) ? "Enabled" : "Disabled";

//Incoming data from webhook (Teams)
$raw_webhook_data = file_get_contents("php://input");
$webhook_data_array = json_decode($raw_webhook_data,true);

//Find owner of the webhook
$botid = $db_local->botGetWebhookOwner($webhook_data_array['id']);
if (!$botid) die();
$botinfo = $db_local->botFetchBots($botid);
if (!$botinfo) die();
$auth = $spark->authGet($botid);
if (!$auth) die();
	
$access = "granted";

//Get message ID from incoming spark data
$incoming_message_id = $webhook_data_array["data"]["id"];

//If message exist
if ($incoming_message_id) {
	//Get the actual message details based on message ID from spark
	$response = $spark->messageGetDetails($botid, $incoming_message_id);
	if (!$response) die();
	//Assigning variables based on the incoming message
	$who_sent = $response['personEmail'];
	$personId = $response['personId'];
	$roomId = $response['roomId'];
	$recepientType = 'roomId';
	$payload = trim($db_local->adminFilterMentions($botinfo[0]['displayName'], $response['text']));
	$files = issetor($response['files'][0]);
	$type = "direct";
	$botmail = $botinfo[0]['emails'];
	
	//Find the initial response array based on the text input from Spark
	$payload_attributes = explode(" ", $payload);
	$keyword = $db_local->quote(strtolower(array_shift($payload_attributes)));
	$reply = $db_local->responseFetchResponse($botid, $keyword);
	
	//E-mail verification details, if the email does not have the correct domain, then stop here.
	if ($who_sent == $botmail) die();
	//Check if there is a restriction on the bot
	if ($db_local->adminCheckBotRestriction($botid)) {
		//If there is a restriction, check if the e-mail domain is valid
		if (!$db_local->adminCheckIfValidDomain($who_sent, $botid)) {
			$spark->messageSendEvent("**{$who_sent}** sent **{$keyword}**:\n- Full command: **" . $payload . "**\n- Response Access: **denied - domain violation (domain restriction policy)!**\n- Bot: **{$botmail}**");
			die();
		}
	}
	//Check if the user is blocked
	if ($db_local->adminGetBlockedContacts($who_sent)) {
		$spark->messageSendEvent("**{$who_sent}** sent **{$keyword}**:\n- Full command: **" . $payload . "**\n- Response Access: **denied - user blocked (blocked user policy)!**\n- Bot: **{$botmail}**");
		die();
	}
	
	//Webhook accessgroup checker (check if person has access to use webhook)
	$webhookAccessgroup = $db_local->webhookGetAccessGroup($webhook_data_array['id']);
	//0 is no restriction on the webhook
	if($webhookAccessgroup != 0) {
		if (!$db_local->groupCheckIfMember($personId, $webhookAccessgroup)) {
			//If the user does not have access, check for group space exceptions
			if ($db_local->adminCheckGroupResponseAcl($botid,$roomId) or (count($db_local->adminCheckUserGroupResponseAcl($botid,$roomId,$personId)))) {
				$type = "group";
			} else {
				$access = "denied - webhook restriction (Webhook restriction policy) - No response sent to user";
			}
		}
	}
	
	if ($webhook_data_array['name'] == "group") {
		$type = "group";
	}
	
	//Response to the incoming message
	if ($reply and $access == "granted") {
		//Command access verification start ---------------------
		if ($reply['accessgroup'] != 0) {
			$contactinfo = $db_local->contactFetchContacts($who_sent);
			if ($db_local->groupCheckIfMember($contactinfo[0]['id'], $reply['accessgroup'])) {
				//Do nothing (has access to the command)
			}
			else {
				//Deny access, reply with access denied
				$access = "denied - command restriction (Command restriction policy)";
				$reply['response'] = "Sorry, I cannot find **{$who_sent}** in the accesslist for this command. Permission was denied.";
			}
		}
		//Command access verification end-----------------------
		
		//Task flag is checked (create a API entry for a listener to pick up if access is granted)
		if ($reply['is_task'] == 1 and $access == "granted") {
			if(!$db_local->adminCheckIfMaintenance()) {
				if (!$db_local->adminCheckServiceStatus()){
					if (!$db_local->taskAddNewTask($keyword, $payload_attributes, array($who_sent, $type, $roomId, $botmail, $payload))) {
						$reply['response'] = "The request failed a sanity check and was purged, please check your input and try again.";
					}
				}
				else {
					$reply['response'] = feedbackMsg("Task service status",$db_local->adminCheckServiceStatus('report'), "warning", 1);
				}
			}
			else {
				$reply['response'] = feedbackMsg("Maintenance mode is enabled!",$db_local->adminGetMaintenanceMessage(), "warning", 1);
			}
		}
		
		//Feature flag is checked (execute feature and send back reply)
		if ($reply['is_feature'] == 1 and $access == "granted") {
			$info = array($botid, $who_sent, $personId, $roomId, $type, $botmail);
			if (!empty($files) and validateUrl($files)){
				$info[] = $files;
			}
			if (!$result = $spark->featureExecute($keyword, $payload_attributes, $info)) {
				$reply['response'] = "Failed to execute feature!";
			}
			else {
				$reply['response'] = $result;
			}
		}
		
		//Build the spark response array
		$query = array('recepientType' => 'roomId',
				'recepientValue' => $roomId,
				'sender' => $botid,
				'text' => $reply['response'],
				'type' => $type
		);
		if(!empty($reply['file_url']) and $access == "granted") {
			$query['files'] = array($reply['file_url']);
		}
		
	}
	else {
		//If default response is not blank and access is granted, send the default response
		if ($botinfo[0]['defres'] != "" and $access == "granted") {
			$query = array('recepientType' => 'roomId',
					'recepientValue' => $roomId,
					'sender' => $botid,
					'text' => $botinfo[0]['defres'],
					'type' => $type
			);
		} else $reply['response'] = "removed";
		$reply['keyword'] = ($access == "granted") ? "$keyword (Unknown keyword)":"$keyword ($access)";
		$access = ($access == "granted") ? "granted - but the keyword was unknown":$access;
	}
	
	//Add warning message if warning mode is enabled
	if ($db_local->adminCheckIfWarning()) {
		$warning = feedbackMsg("SYSTEM MESSAGE!",$db_local->adminGetWarningMessage(), "warning", 1);
		$query['text'] = $warning . $query['text']; 
	}
	
	//Send message back to user (if response is removed, send nothing)
	$test = ($reply['response'] != "removed") ? json_encode($spark->messageSend($query)):false;
	
	//Try to resolve name of the sender and send event notification
	$name = $db_local->contactGetName($who_sent);
	$name = (validateEmail($name)) ? "" : " ($name) ";
	$task_mon = ($db_local->adminCheckIfTaskMonitor()) ? "\n- " . $db_local->adminCheckServiceStatus('report') : "\n- Task monitor is **Disabled**";
	$spark->messageSendEvent("<strong>" . $who_sent . $name . "</strong> sent keyword: <strong>" . $reply['keyword'] . "</strong>:\n- Room Type: <strong>" . $type . "</strong>\n- Full text: <strong>" . $payload . "</strong>\n- Access: <strong>" . $access . "</strong>\n- Conversation with: <strong>" . $botmail . "</strong> $task_mon\n- Maintenance mode: <strong>" . $maintenance . "</strong>\n- Warning mode: <strong>" . $warning_mode . "</strong>");
	
	//Log usage
	$qs = $db_local -> constructInsertValues("activity_log", array('botid'=>$botid,'command'=>$payload,'user'=>$who_sent));
	$db_local -> query($qs);
	
}
?>