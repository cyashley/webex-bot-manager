<?php

include "includes/functions.inc.php";
$db_local = new Db();
$generate = new OutputEngine();
$spark = new SparkEngine();
$ldap = new LDAP();

if(!empty($_SERVER['DOCUMENT_ROOT']))
{
	$rootdoc = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
	$rootdoc = str_replace("/public", "", $rootdoc);
	$configfile = "config_ini.php";
	$fullfilepath = $rootdoc.'/'.$configfile;
	if (!file_exists($fullfilepath)) {
		echo "I have issues finding $configfile which is required, the document root must be set to the webex-bot-manager folder. I am looking for the file in the following root path I got from the server: $rootdoc . Can you please verify that your web server has the correct root directory configured?";
	} 
	require $fullfilepath;
}
else
{
	require '../config_ini.php';	
}

   session_start();
   if (isset($_POST['username'])){
	$username = $db_local->quote($_POST['username']);
} else $username = issetor($_GET['username']);
if (isset($_POST['forgot'])) {
	if (!validateEmail($username)) {
		header("Location: login.php?feedback=warning-Email&username=$username");
	}
	else {	
		$userinfo = $db_local->select("SELECT * FROM contacts WHERE emails = '$username'")[0];
		if (count($userinfo)>0) {
			if ($db_local->adminCheckIsLoginUser($userinfo['id'])) {
				$userid = $userinfo['id'];
				$botid = $db_local->botGetMainInfo()['id'];
				$password = generatePassword();
				$host = getHostUrl();
				$hashpw = password_hash($password, PASSWORD_DEFAULT);
				$text = "Here is your new login passord to [Webex Bot Manager]($host), please change the password when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
				$db_local->userUpdateSettings('update',$hashpw,$userid,'1');
				$spark->messageSend($spark->messageBlob($text, $userid, $botid));
				redirect("login.php?feedback=success-PwReset&username=$username");
			} else redirect("login.php?feedback=success-PwReset&username=$username");
		} else redirect("login.php?feedback=success-PwReset&username=$username");
	}
} 
else {
	if (isset($_POST['login'])) {
		if (!validateEmail($username)) {
			redirect("Location: login.php?feedback=warning-Email&username=$username");
		}
     else {
      $username = $_POST['username'];
      $password = $_POST['password']; 
	  if(!empty($_POST['loginmethod'])){ $loginmethod = $_POST['loginmethod']; } else { $loginmethod = ''; }
	  
	  if($loginmethod == 'AD')
	  {
		  $user = substr($username, 0, strpos($username, "@"));
		$connection = $ldap->connect($config['ADserver'],$config['ADport']);

		// Bind with LDAP instance
		$bind = @$ldap->bind($connection,$username,$password);
		if($bind)
		{
			// Search with a wildcard
			$GroupSearch = @$ldap->search($connection,$config['BaseDN'],'CN=' . $config['AdminGroup'])[0]['member'];
			if(!empty($GroupSearch))
			{
				$GroupMembers = array();
				foreach(array_slice($GroupSearch,1) as $key => $value)
				{
					$DropDN = substr($value, 0, strpos($value, ","));
					$DropCN = str_replace('CN=','',$DropDN);
					$GroupMembers[] = $DropCN;
				}
				
				if(in_array($user, $GroupMembers, true))
				{
					$userexist = count($db_local->select("SELECT * FROM contacts WHERE emails = '$username'"));
					if($userexist == 0)
					{
						$mainbot = $db_local->botGetMainInfo();
						$botid = $mainbot['id'];
						$payload = array("sender"=>$botid, "recepientType"=>"email", "recepientValue"=>$username);
						$userDetails = $spark->peopleGet($payload);
						if (count($userDetails['items']) == 1) {
							$db_local->contactAdd($userDetails, $botid);
							$personid = $userDetails['items'][0]['id'];
							$db_local->userUpdateSettings('update','', $personid, '1');
						}
					}
					else
					{
						$checkifadmin = $db_local->select("SELECT * FROM contacts WHERE emails = '{$username}'")[0]['type'];
						if($checkifadmin != '1')
						{
							$db_local->query("UPDATE contacts SET type='1' WHERE emails = '{$username}'");
						}
					}
					$_SESSION['login_user'] = $username;
					$userinfo = $db_local->select("SELECT * FROM contacts WHERE emails = '$username'")[0];
					$_SESSION['status'] = "logged";
					$_SESSION['userid'] = $userinfo['id'];
					$_SESSION['email'] = $username;
					$_SESSION['name'] = $db_local->contactGetName($userinfo['id']);
					$_SESSION['avatar'] = $userinfo['avatar'];
					redirect("index.php?id=bots");
				}
				else
				{
					$error = 'You are not authorized';
				}
			}
			else
			{
				$error = "Can't find AD admin group";
			}
		}
		else
		{
			$error = "Your Login Name or Password is invalid";
		}
	  }
	  else 
	  {
		  if($db_local->userLogin($username, $password)) {
			$_SESSION['login_user'] = $username;
			$userinfo = $db_local->select("SELECT * FROM contacts WHERE emails = '$username'")[0];
			$_SESSION['status'] = "logged";
			$_SESSION['userid'] = $userinfo['id'];
			$_SESSION['email'] = $username;
			$_SESSION['name'] = $db_local->contactGetName($userinfo['id']);
			$_SESSION['avatar'] = $userinfo['avatar'];
			redirect("index.php?id=bots");	
		  }else {
			 $error = "Your Login Name or Password is invalid";
		  }
	  }
	 }
   }
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Webex Bot Manager | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><img  src="images/static/webexteams.png" height="100px" width="90px">
    	<br><b>Webex </b>Bot Manager</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start</p>
      <form class="form-signin" action = "" method = "post">
        <div class="input-group mb-3">
          <input type="email" id="username" name="username" class="form-control" placeholder="Email" required <?php echo (!issetor($username)) ? "autofocus" : ""; ?> value="<?php echo issetor($username); ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="password" name="password" class="form-control" placeholder="Password" <?php echo (issetor($username)) ? "autofocus" : ""; ?> placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
			<?php

				$ldapenabled = $db_local->select("SELECT * FROM site_settings WHERE settings = 'ldap_enabled'");
				if (!empty($ldapenabled[0]['value']) == '1')
				{
					echo "
					<select name='loginmethod' class='form-control' required>
						<option value='AD' selected>Active Directory</option>
						<option value='Webex'>Local Account</option>	
					</select>
					";
				}
			
			?>
        </div>
        <button class="btn btn-sm btn-primary btn-block" type="submit" value = "login" name="login" >Sign in</button>
        <button class="btn btn-sm btn-danger btn-block" type="submit" value = "forgot" name="forgot" >Forgot Password</button>
      </form>
      <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php if(empty($error)){}else{ echo $error;} ?></div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

</body>
</html>
