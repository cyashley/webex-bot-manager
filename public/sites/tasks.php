<?php
if ($windowid != "login" and !verify()) header("Location: index.php?id=login");
$subpage = issetor($_GET['subpage']);
$user = $_SESSION['email'];

if (isset($_POST['resubmitsubtask'])) {
	$subtaskid = $_POST['resubmitsubtask'];
	$taskid = $_POST['resubmittaskid'];
	$db_local->taskQueueResubmit($taskid, $subtaskid);
}
if (isset($_POST['clearalltasks'])) {
	$result = $db_local->taskQueueClear($user);
	if($result == 1)
	{
		echo alerts('success', 'Success!', 'All Tasks have been cleared', 'index.php?id=tasks');
	}
}
if (isset($_POST['deletealltasks'])) {
	$db_local->taskQueuePurge($user);
}
if (isset($_POST['allfailedtasks'])) {
	$taskid = issetor($_POST['allfailedtasks']);
	$db_local->taskQueueResubmit($taskid);
}
if(!empty($_GET['clear']) == 'yes')
{
	$taskid = $_GET['taskid'];
	$result = $db_local->taskQueueClear($user, $taskid);
	if($result == 1)
	{
		echo alerts('success', 'Success!', 'Task has been cleared', 'index.php?id=tasks');
	}
}
?>
<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Tasks</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
 <div class="content">
      <div class="container-fluid">

<?php

switch ($subpage) {

	case "mysubtask":
		$response = $payload = "";
		$mysubtaskid = issetor($_GET['subtaskid']);
		$mysubtask = $db_local->taskQueueFetchSubtask($mysubtaskid);
		$task_json = $mysubtask[0]['task_json'];
		$type = $mysubtask[0]['subtask_type'];
		$status = $mysubtask[0]['task_code'];
		$task_result = $mysubtask[0]['task_results'];
		if (!empty(json_decode($mysubtask[0]['response']))) {
			$response = json_encode(json_decode($mysubtask[0]['response']), JSON_PRETTY_PRINT);
		}
		if (!empty(json_decode($mysubtask[0]['payload']))) {
			$payload = json_encode(json_decode($mysubtask[0]['payload']), JSON_PRETTY_PRINT);
		}
			
		
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary">
					<div class="card-header with-border">
						<h3 class="card-title"><?php echo "Subtask {$mysubtask[0]['subtask_id']}"?></h3>
					</div>
		<div class="card-body">
		<?php 
		if ($status != '') {
		?>
		<table width='100%' id='tasks' class='table table-bordered table-striped'>
					<tbody>
						<tr>
							<th>Type</th>
							 <td><?php echo "{$mysubtask[0]['subtask_type']}";?>
						<tr>
							<th>Task</th>
							 <td><?php echo $task_json;?>
						<tr>
							<th>Payload</th>
							 <td><pre><?php echo $payload; ?></pre>
						<tr>
							<th>Response</th>
							 <td><pre><?php echo $response; ?></pre>
						<tr>
							<th>Task result</th>
							 <td><pre><?php echo $task_result; ?></pre>
						<tr>
							<th>Subtask Status</th>
							 <td><?php echo $db_local->httpResponseLabel($status, true); ?>
						</tr>
					</tbody>
				</table>
		</div>
		</div>
		</div>
		</div>
		<?php }
		else {
			echo 'No response yet</div></div>
		</div>
		</div>';
		} ?>
		
<?php 
	break;
	case "mytasks":

?>

<div class="row">
	<div class="col-12">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">SubTasks</h3>
			</div>
			<div class="card-body" style="overflow: auto">
				<table width='100%' id='subtasks' class='table table-bordered table-striped'>
					<thead>
						<tr>
							<th>SubTask ID</th>
							<th>User Name</th>
							<th>User Email</th>
							<th>Task Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$taskid = $_GET['taskid'];
							$subtasks = $db_local->select("SELECT * FROM queue_subtasks WHERE task_id = '{$taskid}'");
							
							//$allsubtasks = "";
							foreach($subtasks as $key=>$value)
							{
								$subtaskid = $value['subtask_id'];
								echo '<tr>';
								echo '<td><a href="index.php?id=tasks&subpage=mysubtask&subtaskid='.$subtaskid.'">'.$subtaskid.'</a></td>';

								if($value['subtask_type'] == 'messages')
								{
									$taskjson = json_decode($value['task_json'], true);
									$messagetoid = $taskjson['rec'];
									$messagetoname = $db_local->contactGetName($messagetoid);
									echo '<td>' . $messagetoname . '</td>';
									$messagetoemail = $db_local->contactGetEmail($messagetoid);
									echo '<td>' . $messagetoemail . '</td>';
								}elseif($value['subtask_type'] == 'addtolocal')
								{
									$useremail = $value['task_json'];
									$messagetoname = $db_local->contactGetName($useremail);
									echo '<td>' . $messagetoname . '</td>';
									echo '<td>' . $useremail . '</td>';
								}
								elseif($value['subtask_type'] == 'updatelocal')
								{
									$messagetoname = $db_local->contactGetName($value['task_json']);
									echo '<td>' . $messagetoname . '</td>';
									$useremail = $db_local->contactGetEmail($value['task_json']);
									echo '<td>' . $useremail . '</td>';
								}
								else
								{
									echo '<td>N/A</td>';
									$useremail = $value['task_json'];
									echo '<td>' . $useremail . '</td>';
								}

								$errorcode = $value['task_code'];
								$errordetails = $db_local->select("SELECT * FROM webex_response_codes WHERE code = '{$errorcode}'");

								if(!empty($errordetails[0]['label']))
								{
									echo '<td align="center">' . $errordetails[0]['label'] . '</td>';
								}
								else
								{
									echo '<td align="center"><span class="label label-default">In Message Queue</span></td>';
								}
								$bodid = $value['bot_id'];
								if(!empty($errordetails[0]['code']))
								{
									if($errordetails[0]['code'] != 200)
									{										
										echo '<td align="center">
										<form method="post" action="#taskservice" id="taskservice" enctype="multipart/form-data">
												<input id="button" type="submit" class="btn btn-xs btn-primary btn-block" value="Retry">
												<input type="hidden" value="' . $subtaskid . '" name="resubmitsubtask">
												<input type="hidden" value="' . $taskid . '" name="resubmittaskid">
											</form>
											</td>';
									}
									else
									{
										echo '<td>No Action Available</td>';
									}
								}
								else
								{
									echo '<td>Please wait, in queue for processing..</td>';
								}
								
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
				<div class="card-footer">
				<form method="post" action="#taskservice" id="taskservice" enctype="multipart/form-data">
					<input id="button" type="submit" class="btn btn-sm btn-danger" value="Resubmit All Failed Tasks">
					<input type="hidden" value="<?php echo $taskid; ?>" name="allfailedtasks">
				</form>
				</div>
			</div>
		</div>
	</div>


<?php
		break;
		default:
?>


<div class="row">
	<div class="col-12">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">My Tasks</h3>
			</div>
			<div class="card-body" style="overflow: auto">
				<table id='queue_task' width="100%" class='table table-bordered table-striped'>
					<thead>
						<tr>
							<th>Submitted</th>
							<th>Task Type</th>
							<th>Task Name</th>
							<th>Task Progress</th>
							<th>Unprocessed SubTasks</th>
							<th>Successful SubTasks</th>
							<th>Failed SubTasks</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$user = $_SESSION['login_user'];
							$tasks = $db_local->select("SELECT * FROM queue_task WHERE task_submitter = '{$user}' AND clear = ''");

							foreach($tasks as $key=>$value)
							{
								echo '<tr>';
								echo '<td>' . $value['submit_timestamp'] . '</td>';
								echo '<td>' . $value['task_type'] . '</td>';
								echo '<td>' . $value['task_name'] . '</td>';
								$taskid = $value['task_id'];
								$subtasks = $db_local->select("SELECT * FROM queue_subtasks WHERE task_id = '{$taskid}'");

								$unprocessed = array();
								$success = array();
								$failures = array();
								foreach ($subtasks as $subkey => $subvalue)
								{
									if($subvalue['task_code'] == '200')
									{
										$success[] = '1';
									}elseif($subvalue['task_code'] == '')
									{
										$unprocessed[] =  '0';
									}
									else
									{
										$failures[] = '2';
									}
								}
								if(!empty($subtasks)) { $subtaskcount = count($subtasks);} else { $subtaskcount = 0;};
								if(!empty($success)) { $successfulsubtask = count($success);} else { $successfulsubtask = 0;};
								if(!empty($failures)) { $failedsubtasks = count($failures);} else { $failedsubtasks = 0;};
								if(!empty($unprocessed)) { $unprocessedsubtasks = count($unprocessed);} else { $unprocessedsubtasks = 0;};

								$progressbar = (($successfulsubtask + $failedsubtasks)/$subtaskcount)*100;
								
								echo '<td><div class="progress">
	                 						<div class="progress-bar progress-bar-success" style="width: ' . (int)$progressbar . '%"><font color="black">' . (int)$progressbar . '%</font></div>
	            						</div></td>';
								echo '<td>' . $unprocessedsubtasks . '</td>';
								echo '<td>' . $successfulsubtask . '</td>';
								echo '<td>' . $failedsubtasks . '</td>';
								echo '<td><a href="index.php?id=tasks&subpage=mytasks&taskid=' . $taskid . '" title="View SubTasks"><i class="fas fa-external-link-alt"></i></a>
									  &ensp;<a href="index.php?id=tasks&taskid=' . $taskid . '&clear=yes" title="Clear Task From Table"><i class="fa fa-eraser" style="color: red !important"></i></a>
								</td>';
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
				<div class="card-footer">
					<table><tr><td><button value="Refresh Page" class="btn btn-sm btn-primary" onClick="window.location.reload();">Refresh Table</button>
					<td><form method="post" action="#clearalltasks" id="clearalltasks" enctype="multipart/form-data">
						<button value="Clear all tasks" class="btn btn-sm btn-danger" style="margin-left: 2px;">Clear all tasks</button> 
					<input type="hidden" name="clearalltasks" value='true'>
					</form><!-- <td><form method="post" action="#clearalltasks" id="clearalltasks" enctype="multipart/form-data">
						<button value="Clear all tasks" class="btn btn-sm btn-danger" <?php echo $link_confirm; ?> style="margin-left: 2px;">Delete all tasks</button>
					<input type="hidden" name="deletealltasks" value='true'>
					</form>
					</td> --></td></tr></table>
				</div>
    	</div>
	</div>
</div>
<?php
}
?>
</div>
</div>
