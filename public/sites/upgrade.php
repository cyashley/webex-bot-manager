<section class="content-header">
	<h1><?php echo issetor($feedback); ?>
		Upgrade
	</h1>
</section>
<section class="content">
<?php

if ($windowid != "login" and !verify()) header("Location: login.php"); 

function upgradeQuery($version)
{
	//SQL STATEMENTS FOR EACH VERSION

	/* EXAMPLE ARRAY FOR UPGRADE QUERIES
	$upgrade_queries["<VERSION NUMBER>"][] = "-- Create example table"; //This is a comment to display on screen
	$upgrade_queries["<VERSION NUMBER>"][] = "CREATE TABLE example ( //SQL query
		`id` INT AUTO_INCREMENT,
		`example1` VARCHAR(255) NOT NULL,
		`example2` VARCHAR(255) NOT NULL,
		PRIMARY KEY (id)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["<VERSION NUMBER>"][] = "UPDATE `site_settings` SET `value`='<VERSION NUMBER>' WHERE `settings` = 'dbversion'"; //MANDATORY query to update dbversion in site_settings table
	*/

	/*
	DB Version 2 update. 
	Adding tables for AD Groups and Mapping AD groups to local groups for nightly tasks
	Adding queue tasks and queue subtasks for new queuing system.
	*/
	$upgrade_queries["2"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 2";
	$upgrade_queries["2"][] = "-- Create ad_groups table";
	$upgrade_queries["2"][] = "CREATE TABLE `ad_groups` (
	  `id` int(100) NOT NULL AUTO_INCREMENT,
	  `group_name` varchar(255) NOT NULL,
	  PRIMARY KEY (id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create ad_group_mapping table";
	$upgrade_queries["2"][] = "CREATE TABLE `ad_group_mapping` (
	  `id` int(100) NOT NULL AUTO_INCREMENT,
	  `ad_group_id` int(100) NOT NULL,
	  `local_group_id` int(100) NOT NULL,
	  PRIMARY KEY (id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create queue_task table";
	$upgrade_queries["2"][] = "CREATE TABLE `queue_task` (
	  `task_id` varchar(100) NOT NULL,
	  `task_type` varchar(50) NOT NULL,
	  `task_name` varchar(35) NOT NULL,
	  `task_submitter` varchar(35) NOT NULL,
	  `submit_timestamp` int(10) NOT NULL,
	  `clear` varchar(3) NOT NULL,
	  PRIMARY KEY (task_id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create queue_subtasks table";
	$upgrade_queries["2"][] = "CREATE TABLE `queue_subtasks` (
	  `subtask_id` varchar(100) NOT NULL,
	  `task_id` varchar(100) NOT NULL,
	  `bot_id` varchar(255) NOT NULL,
	  `subtask_type` varchar(25) NOT NULL,
	  `task_json` mediumtext NOT NULL,
	  `group_id` varchar(25) NOT NULL,
	  `task_code` varchar(3) NOT NULL,
	  `task_results` varchar(255) NOT NULL,
	  PRIMARY KEY (subtask_id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create webex_response_codes table";
	$upgrade_queries["2"][] = "CREATE TABLE `webex_response_codes` (
	  `code` varchar(3) NOT NULL,
	  `status` varchar(50) NOT NULL,
	  `label` varchar(255) NOT NULL,
	  `description` varchar(500) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Inserting Codes into webex_response_codes table";
	$upgrade_queries["2"][] = "INSERT INTO `webex_response_codes` (`code`, `status`, `label`, `description`) VALUES
	('200', 'OK', '<span class=\"label label-success\">Success</span>', 'Successful request with body content.'),
	('204', 'No Content', '<span class=\"label label-success\">No Content</span>', 'Successful request without body content.'),
	('400', 'Bad Request', '<span class=\"label label-warning\">Bad Request</span>', 'The request was invalid or cannot be otherwise served. An accompanying error message will explain further.'),
	('401', 'Unauthorized', '<span class=\"label label-warning\">Unauthorized</span>', 'Authentication credentials were missing or incorrect.'),
	('403', 'Forbidden', '<span class=\"label label-warning\">Forbidden</span>', 'The request is understood, but it has been refused or access is not allowed.'),
	('404', 'Not Found', '<span class=\"label label-warning\">Not Found</span>', 'The URI requested is invalid or the resource requested, such as a user, does not exist. Also returned when the requested format is not supported by the requested method.'),
	('405', 'Method Not Allowed', '<span class=\"label label-warning\">Method Not Allowed</span>', 'The request was made to a resource using an HTTP request method that is not supported.'),
	('409', 'Conflict', '<span class=\"label label-warning\">Conflict</span>', 'The request could not be processed because it conflicts with some established rule of the system. For example, a person may not be added to a room more than once.'),
	('415', 'Unsupported Media Type', '<span class=\"label label-warning\">Unsupported Media Type</span>', 'The request was made to a resource without specifying a media type or used a media type that is not supported.'),
	('429', 'Too Many Requests', '<span class=\"label label-warning\">Too Many Requests</span>', 'Too many requests have been sent in a given amount of time and the request has been rate limited. A Retry-After header should be present that specifies how many seconds you need to wait before a successful request can be made.'),
	('500', 'Internal Server Error', '<span class=\"label label-danger\">Internal Server Error</span>', 'Something went wrong on the server'),
	('502', 'Bad Gateway', '<span class=\"label label-danger\">Bad Gateway</span>', 'The server received an invalid response from an upstream server while processing the request. Try again later.'),
	('503', 'Service Unavailable', '<span class=\"label label-danger\">Service Unavailable</span>', 'Server is overloaded with requests. Try again later.');";
	$upgrade_queries["2"][] = "UPDATE `site_settings` SET `value`='2' WHERE `settings` = 'dbversion'";
	/*
	 DB Version 3 update
	 Added system message feature
	 
	 */
	$upgrade_queries["3"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 3";
	$upgrade_queries["3"][] = "ALTER TABLE `generic_feedback` ADD PRIMARY KEY(`id`);";
	$upgrade_queries["3"][] = "-- Alter table, insert warning mode";
	$upgrade_queries["3"][] = "ALTER TABLE `service_status` ADD `warning_mode` INT(1) NOT NULL AFTER `maintenance_mode`;";
	$upgrade_queries["3"][] = "-- Create warning message and default";
	$upgrade_queries["3"][] = "INSERT INTO `generic_feedback` (`id`, `message`, `default_message`) VALUES ('warning', '**Warning:** We are currently experiencing network issues so you may experience problems with communication.', '**Warning:** We are currently experiencing network issues so you may experience problems with communication.');";
	$upgrade_queries["3"][] = "ALTER TABLE `queue_task` CHANGE `submit_timestamp` `submit_timestamp` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';";
	$upgrade_queries["3"][] = "UPDATE `site_settings` SET `value`='3' WHERE `settings` = 'dbversion'";
	/*
	 DB Version 4 update
	 Added botid and botresponse to task so the task knows which bot was used to send the request and also to notify a user when a task was completed.
	 The user can add a task to add contacts via a bot. This will show as "Add contacts via bot" as task name. 
	 */
	$upgrade_queries["4"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 4";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_task` CHANGE `submit_timestamp` `submit_timestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_task` ADD `botresponse` INT(1) NOT NULL AFTER `task_submitter`;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_task` ADD `botid` VARCHAR(150) NOT NULL AFTER `botresponse`;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_subtasks` CHANGE `task_results` `task_results` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_subtasks` ADD `payload` TEXT NOT NULL AFTER `task_code`;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_subtasks` ADD `response` TEXT NOT NULL AFTER `payload`;";
	$upgrade_queries["4"][] = "UPDATE `site_settings` SET `value`='4' WHERE `settings` = 'dbversion'";
	return $upgrade_queries[$version];
}

if(!empty($_GET['function'])){ $upgradeFunction = $_GET['function']; } else { $upgradeFunction = ''; }
if(!empty($_GET['currentdbver'])){ $currentDBver = $_GET['currentdbver']; } else { $currentDBver = ''; }
if(!empty($_GET['newdbver'])){ $newDBver = $_GET['newdbver']; } else { $newDBver = ''; }


if($upgradeFunction == 'upgradedb')
{
	$upgradedb = upgradedb($db_local,$currentDBver,$newDBver);
	
	echo '
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">DB Upgrade Status</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body"  style="height: 250px; overflow: auto;">
					<table class="table">
						<tr>
							<th width="75%">ID</th>
							<th>Status</th>
						</tr>';
						
						foreach ($upgradedb as $version => $queryobj) {
							foreach ($queryobj as $resultkey => $resultobj ) {
									echo'<tr>
										 <td style="word-wrap: break-word">'. $resultobj[0] .'</td>';
									if($resultobj[1] == '1')
									{
										echo '<td>SQL successful</td>';
									}
									else
									{
										echo '<td>' . $resultobj[1] . '</td>';
									}
									echo'
								</tr>';
								
							}
									
							
						}
				echo '</table>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-primary btn-block" onclick="location.href = \'index.php?id=upgrade\';" >CLOSE</button>
						</div>
					</div>
				</div>
			</div>';
}
elseif($upgradeFunction == 'loadsitesettings')
{
	$loadsitesettings = $db_local->insertsitesettings();

	if(!empty($loadsitesettings))
	{
		if($loadsitesettings[0] === true)
		{
			redirect("index.php?id=upgrade&function=successful-insertsitesettings");			
		}
	}
}

function upgradedb($db_local,$currentDBversion,$NewDBVersion) 
{
	$upgradedb = array();
	$version = $currentDBversion + 1;
	while ($version <= $NewDBVersion)
	{
		$upgrade_queries = upgradeQuery($version);
		foreach($upgrade_queries as $key => $value) {
			$upgradedb[$version][$key][] = $value;
			$upgradedb[$version][$key][] = $db_local->query($value);
		}
		$version++;
	}
	return $upgradedb;
}
?>

<div class="row">
	<div class="col-md-3">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">WBM Version</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class='row'>
					<div class='col-md-10'>
						WBM Database Version
					</div>
					<div class='col-md-2'>
						<?php echo $db_local->checkdbversion()[0]['value']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>