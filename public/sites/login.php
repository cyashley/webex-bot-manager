<?php 
if (isset($_POST['username'])){
	$username = $db_local->quote($_POST['username']);
} else $username = issetor($_GET['username']);
if (isset($_POST['forgot'])) {
	if (!validateEmail($username)) {
		header("Location: index.php?id=login&feedback=warning-Email&username=$username");
	}
	else {	
		$userinfo = $db_local->select("SELECT * FROM contacts WHERE emails = '$username'")[0];
		if (count($userinfo)>0) {
			if ($db_local->adminCheckIsLoginUser($userinfo['id'])) {
				$userid = $userinfo['id'];
				$botid = $db_local->botGetMainInfo()['id'];
				$password = generatePassword();
				$hashpw = password_hash($password, PASSWORD_DEFAULT);
				$text = "Here is your new login passord to the Spark Bot Management site, please change the password when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
				$db_local->userUpdateSettings('update',$hashpw,$userid,'1');
				$spark->messageSend($spark->messageBlob($text, $userid, $botid));
				header("Location: index.php?id=login&feedback=success-PwReset&username=$username");
			} else header("Location: index.php?id=login&feedback=success-PwReset&username=$username");
		} else header("Location: index.php?id=login&feedback=success-PwReset&username=$username");
	}
} 
else {
	if (isset($_POST['login'])) {
		if (!validateEmail($username)) {
			header("Location: index.php?id=login&feedback=warning-Email&username=$username");
		}
		else {
			$password = $db_local->quote($_POST['password']);				
			if ($db_local->userLogin($username, $password)) {
				$userinfo = $db_local->select("SELECT * FROM contacts WHERE emails = '$username'")[0];
				$_SESSION['status'] = "logged";
				$_SESSION['userid'] = $userinfo['id'];
				$_SESSION['email'] = $username;
				$_SESSION['name'] = $db_local->contactGetName($userinfo['id']);
				$_SESSION['avatar'] = $userinfo['avatar'];
				header("Location: index.php?id=bots");	
			}
			else {
				$_SESSION['status'] = "";
				header("Location: index.php?id=login&feedback=alert-Login&username=$username");
			}
		}	
	}
}
if (isset($_GET['signoff'])) {
	session_destroy();
	header("Location: index.php?id=login");
}

?>

	<div id="login-section">
		<form action="index.php?id=login" method='post'>
			<div id="input">
				<table>
				<tr>
					<td colspan=2>
						<h1>LOGIN</h1>
				<tr>
					<td colspan=2>
						<input type="text" placeholder="Email address" value='<?php echo issetor($username); ?>' required="Required" name="username" />		
				<tr>
					<td colspan=2>
						<input type="password" placeholder="Password" name="password" />
				<tr>
					<td>
						<input type='submit' name='login' value='Login'>
					<td>
						<input type='submit' name='forgot' class='generic' value='Forgot password'>
				</table>
			</div>
		</form>
	</div>


