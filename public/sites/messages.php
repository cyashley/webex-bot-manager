<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$botid = $executor = $disable = $editchecked = $botname = "";
$rooms_selected = $contacts_selected = $rooms_raw = $contacts_raw = array();
$savemsg = "Save";
$savevalue = "Save";

if (empty($_GET['botid'])) {
	$disable = "disabled";
}
else {
	$botid = $db_local->quote($_GET['botid']);
	$botid_link = "&botid=" . $botid;	
	$botinfo = $db_local->botFetchBots($botid);
	$botname = " will be sent from <b>{$botinfo[0]['displayName']}</b>";
	if (isset($_GET['delete_saved'])) {
		$db_local->messagesDelete($db_local->quote($_GET['delete_saved']));
	}
	if (isset($_POST['Save']) or isset($_POST['Edit'])){
		$title = issetor($_POST['title']);
		if (!$title) {
			$title = "Unnamed message";
		}
		$save_data=array('message' => $db_local->quote($_POST['message']), 'files' => $_POST['files'], 'title' => $db_local->quote($title));
		(isset($_POST['Save'])) ? $result = $db_local->messagesSave($save_data) : $result = $db_local->messagesUpdate($save_data, $_POST['update']);
		$die = 1;
	}
	if (isset($_GET['edit_saved'])) {
		$msgedit = $db_local->messagesLoad($db_local->quote($_GET['edit_saved']));
		$savemsg = "Edit";
		$savevalue = "Save changes";
		$editchecked = (issetor($_GET['c']) == '1') ? "checked":"";
	}
	
	$direct_rooms = $spark->roomGet(array("sender"=>$botid,"max"=>"5000","type"=>"direct"));
	
}
?>
<?php
if (isset($_POST['retry_messages'])) {
	$a = 0;
	foreach ($_POST['personids'] as $key => $value) {
		$messageParsel[$a]['rec'] = $value;
		$messageParsel[$a]['type'] = "toPersonId";
		$messageParsel[$a]['message'] = $_POST['messagepayload'];
		if (!empty($_POST['files'])) $messageParsel[$a]['files'] = $_POST['files'];
		$a++;
	}
	//Start request intervals
	echo $spark->bulkSplitRequestPost("messages", $messageParsel, $botid, 0, 50, 150, 15, 100, $_SESSION['login_user']);
	//End request intervals
}
if (isset($_POST['sendmessage'])) {
	if (isset($die)) die();
	$a = 0;
	
	$taskid = $db_local->generatetaskid();

	if (isset($_POST['contacts']) and (count($_POST['contacts']))) {
		foreach ($_POST['contacts'] as $key => $value) {
			$contacts_raw[] = $value;
		}
	}			
	if (isset($_POST['groups']) and (count($_POST['groups']))) {
		foreach ($_POST['groups'] as $key => $value) {
			$group_details = $db_local->groupFetchGroups($value);
			if ($group_details[0]['botid']) {
				$space_members = $db_local->groupGetSpaceMembers($value, $group_details[0]['botid']);
				foreach ($space_members as $spacekey => $spacevalue) {
					$rooms_raw[] = $spacevalue['spaceid'];
				}
			}
			$members = $db_local->groupGetMembers($value);
			foreach ($members as $key => $value) {
				$contacts_raw[] = $value['contactid'];
			}
		}
	}
	if (isset($_POST['rooms']) and (count($_POST['rooms']))) {
		foreach ($_POST['rooms'] as $key => $value) {
			$rooms_raw[] = $value;
		}
	}	
	if(isset($_GET['botid'])) {
		$direct_rooms = $spark->roomGet(array("sender"=>$botid,"max"=>"5000","type"=>"direct"));
	}
	//Clean duplicate entries if any
	$contacts_selected = (count($contacts_raw) > 0) ? array_unique($contacts_raw) : array();
	$rooms_selected = (count($rooms_raw) > 0) ? array_unique($rooms_raw) : array();
	$messageParsel = array();
	
	if (isset($_POST['alldirect']) and $_POST['alldirect'] == 1){
		foreach ($direct_rooms['items'] as $key => $value){
			//$messageParsel[$a]['subtaskid'] = $db_local->generatesubtaskid();
			$subtaskid = $db_local->generatesubtaskid();
			$messageParsel[$subtaskid]['rec'] = $value;
			$messageParsel[$subtaskid]['type'] = "roomId";
			$messageParsel[$subtaskid]['message'] = $_POST['message'];
			if (!empty($_POST['files'])) $messageParsel[$subtaskid]['files'] = $_POST['files'];
			$a++;
		}
	}
	else {	
		if (count($contacts_selected)) {
			foreach ($contacts_selected as $key => $value) {
				//$messageParsel[$a]['subtaskid'] = $db_local->generatesubtaskid();
				$subtaskid = $db_local->generatesubtaskid();
				$messageParsel[$subtaskid]['rec'] = $value;
				$messageParsel[$subtaskid]['type'] = "toPersonId";
				$messageParsel[$subtaskid]['message'] = $_POST['message'];
				if (!empty($_POST['files'])) $messageParsel[$subtaskid]['files'] = $_POST['files'];
				$a++;
			}
		}
	}
	if (count($rooms_selected) > 0) {
		foreach ($rooms_selected as $key => $value) {
			//$messageParsel[$a]['subtaskid'] = $db_local->generatesubtaskid();
			$subtaskid = $db_local->generatesubtaskid();
			$messageParsel[$subtaskid]['rec'] = $value;
			$messageParsel[$subtaskid]['type'] = "roomId";
			$messageParsel[$subtaskid]['message'] = $_POST['message'];
			if (!empty($_POST['files'])) $messageParsel[$subtaskid]['files'] = $_POST['files'];
			$a++;
		}
	}
	if (count($messageParsel)) {
	//----------------------- Message request execute -----------
	
	//Insert task into task queue
	$result = $db_local->taskQueueInsert($taskid, $_POST['title'], "messages", $messageParsel, $botid, $_SESSION['login_user']);
	if($result == 1)
	{
		echo "<script>	Swal.fire({
		title: 'Success!',
		html: 'Message(s) sent to queue for processing',
		type: 'success',
		confirmButtonText: 'Close',
		})
		</script>";
	}
	//End request intervals
	} else $feedback = msgList("warning-NoReceivers");
}
//Page tooltips
$tooltips = array(
	'messages'=>tooltip('Messages','A powerful tool to distribute messages / announcements to people and spaces.'),
	'selectBot'=>tooltip('Select a bot','Select which bot you want to send the message from, by clicking the bot avatar below'),
	'messageName'=>tooltip('Message name','If you save the message, the name of the message will be displayed to under the saved messages. The message name is NOT included in the sent message.'),
	'message'=>tooltip('Message','Anything you type in this field will be sent to the user, in the right field you will see approximately how the message would look in Webex Teams when received by the contact. Feel free to send a test message to yourself before distributing to many people or spaces to make sure it looks OK.'),
	'fileUrl'=>tooltip('File URL','Type in a publically reachable URL that is pointing to a file (image or other files). Linking to large files is not recommended when sending a lot of messages.'),
	'recipients'=>tooltip('Recipients','Select who should receive the message. You can send a message to groups of people, groups of spaces, single users, single spaces or all 1:1 (take caution when sending messages to all 1:1 - max 1000). The bot will not send duplicate messages to one person if the user is part of more than one group for example.')
);
?>

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Messages</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
 <div class="content">
      <div class="container-fluid">

<div class="row">
	<div class="col-lg-8">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title"><?php echo $tooltips['messages']; ?> Messages <?php echo $botname; ?></h3>
			</div>
			<div class="card-body">
				<form id='message' method='post' onsubmit='loading("process", "Please wait...")' action='#' enctype='multipart/form-data'>	
					<div class="box-body">
						<div class="form-group">
							<label for="BotSelection"><?php echo $tooltips['selectBot']; ?> Select a bot</label>
							<br>
						<?php 
							$selected = $generate->botGenSelector('messages', issetor($botid));
							
							if ($selected) {
								$executor = "<b>{$selected['displayName']}</b>";
								try{
									$executor_avatar = "<img class='rounded' title='{$selected['displayName']} (SELECTED)' height='35' width='35' src='images/bots/{$selected['displayName']}.jfif'>";
								}catch (exception $e) {
								    echo 'WBM does not have access to the images directory.';
								}
								
							}
							
							$disable = (empty($executor)) ? "disabled" : "";
							if($disable) {
							    echo "
							</div>
							</div>";
						}
						else{


							
						?>
						</div>
						<div class="form-group">
							<label for="Headline"><?php echo $tooltips['messageName']; ?> Message name:</label>
							<input type="text" <?php echo $disable; ?> class="form-control" id="Headline" placeholder="Message name" name='title' value="<?php echo issetor($msgedit[0]['title'])?>">
						</div>
						<div class="form-group">
							<label for="Message"><?php echo $tooltips['message']; ?> Message:</label>
							<link rel="stylesheet" href="plugins/editormd/css/editormd.css" />

							<div id="test-editor" id="Message" class="form-control">
								<textarea required='' name='message' <?php echo $disable; ?>><?php echo issetor($msgedit[0]['message']); ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="FileURL"><?php echo $tooltips['fileUrl']; ?> File URL</label>
							<input type='text' <?php echo $disable; ?> class="form-control" placeholder='File URL' name='files' value='<?php echo issetor($msgedit[0]['files']);?>'>
							<input type='hidden' value='<?php echo issetor($_GET['edit_saved']); ?>' name='update'>
						</div>
						
						<div class="box-header with-border">
							<h6><?php echo $tooltips['recipients']; ?> Select Recipients</h6>
						</div>
						<?php  
							$groups = $db_local->groupFetchGroups();
							$number_of_groups = count($groups); 
						?>
						<div class="row" style="padding-top: 1%">
							<div class="col-md-3">
								Public groups:
								<select class="form-control select2" name="groups[]" multiple="multiple" style="width: 100%;">
								<?php 
									echo $generate->groupLinks('optionlinks',"","", $botid);
								?>
								</select>
							</div>
							<div class="col-md-3">
									Spaces:
									<?php 
										if(isset($_GET['botid'])) {
											$query = array("sender"=>$botid,"max"=>"200","type"=>"group");
											echo '<select class="form-control select2" name="rooms[]" multiple="multiple" style="width: 100%;">';
											echo $generate->roomGenCheckbox($spark->roomGet($query), "rooms[]", $botid);
											echo '</select>';
										}
										else {
											echo "Select a bot to view spaces!<br>";
										}
										$contacts = $db_local->contactFetchContacts();
										$number_of_contacts = count($contacts);
									?>
								
							</div>
							<div class="col-md-3">
								<div class="form-group">
									Users:
									<select class="form-control select2" name='contacts[]' multiple="multiple" style="width: 100%;">
									<?php 
									foreach ($contacts as $key => $value) {
										$name = "{$value['firstName']} {$value['lastName']}";
										if (trim($name) == "") {
											$name = $value['emails'];
										}
										echo "<option value='{$value['id']}'>$name</option>";
									}
									?>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								All 1:1: (<?php echo (isset($direct_rooms['items'])) ? colorize_value($infocolor, count($direct_rooms['items'])) : colorize_value($infocolor, 0);?>)
								<select class="form-control select2" name='alldirect' multiple="multiple" style="width: 100%;">
									<option value="1">All 1:1 conversations</option>
								<!-- <?php 
									echo "<input type='checkbox' {$disable} name='alldirect' value='1'> <a href='index.php?id=spaces&botid=$botid&spacetype=direct'>All 1:1 conversations</a> <br><br>"; ?> -->
								</select>
							</div>
						</div>
						<br>
						<br>
						<div class="box-footer">
							<div class="row" style="padding-top: 1%">
								<div class="col-md-3 mb-2">
								<button type="submit" class="btn btn-primary btn-block" name='sendmessage' <?php echo $disable; ?> <?php echo $link_confirm; ?>>Send message</button>
								</div> 
								<div class="col-md-3 mb-2">
									<input <?php echo $disable;  ?> type='submit' class="btn btn-primary btn-block" value='<?php echo $savevalue; ?>' name='<?php echo $savemsg; ?>'>
								</div>
								<div class="col-md-3 mb-2">
									<?php echo (issetor($_GET['edit_saved'])) ? "<input type='submit' class='btn btn-primary btn-block' value='Save as new' name='Save'>":"";?>
								</div>
								<div class="col-md-3 mb-1">
									<?php echo (issetor($_GET['edit_saved'])) ? "<a href='index.php?id=messages$botid_link' class='btn btn-danger btn-block'>Cancel</a>":"";?>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Saved Messages</h3>
			</div>
			<div class="card-body">
				<?php 
				//Print out all stored messages so they can be re-populated in the text fields
				if(isset($_GET['botid'])){
					$stored_messages = $db_local->messagesLoad();
					$nummsg = count($stored_messages);
					//echo "<table class='rounded compact'><tr><td colspan='2'><b><font color='$infocolor'>$nummsg</font> STORED MESSAGES</b><td>";
					echo "<table width='100%' id='tasks' class='table table-bordered table-striped'><thead><tr><th>Message Name</th><th>Edit</th><th>Delete</th></tr></thead><tbody>";
					foreach ($stored_messages as $key => $messagedetails) {
						echo "<tr>
								<td><a href='index.php?id=messages$botid_link&edit_saved={$messagedetails['id']}' title='Click to load message'>{$messagedetails['title']}</a> 
								<td align='center'><a href='index.php?id=messages$botid_link&edit_saved={$messagedetails['id']}' title='Click to edit message' style='color: $pos_color;'><i class='fas fa-pencil-alt'></i></a></b><br></li>
									<td align='center'><a href='index.php?id=messages$botid_link&delete_saved={$messagedetails['id']}' $link_confirm title='Click to delete message' style='color: red'><i class='fas fa-trash'></i></a></li>";
					}
					echo "</table>";
					}
				}?>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php
unset($_POST);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="plugins/editormd/editormd.min.js"></script>
<script type="text/javascript">
	$(function() {
		var editor = editormd("test-editor", {
			 width  : "100%",
			 height : "400px",
			path   : "plugins/editormd/lib/",
			toolbarIcons : function() {
            // Or return editormd.toolbarModes[name]; // full, simple, mini
            // Using "||" set icons align right.
            return ["undo", "redo", "|", "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|", "h1", "h2", "h3", "h3", "h4", "h5", "h6", "|", "list-ul", "list-ol", "hr", "|", "link", "reference-link", "image", "code", "preformatted-text", "code-block", "table", "datetime", "emoji", "html-entities", "pagebreak", "|", "goto-line", "unwatch", "preview", "clear", "search"]
        },
		});
	});
</script>
<script src="plugins/editormd/languages/en.js"></script>