<?php if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); ?>

<!-- Content Header (Page header) -->
<div class='content-header'>
	<div class='container-fluid'>
		<div class='row mb-2'>
			<div class='col-sm-6'>
				<h1 class='m-0 text-dark'>User Guide</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class='content'>
	<div class='container-fluid'>
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					
					<div class="card-body">
						<div class='doc-main'><h3>Introduction</h2>
						<p>Welcome to Webex Teams Bot Manager, a feature rich tool for managing your bots and your day to day workflows in Webex Teams. This tool 
						is targeting novice / intermediate scripters that wants to get started with bots in Webex Teams. </p>

						<p>Bot Manager currently wraps around most of the API's found in Webex Teams (Messages, People, Spaces, Teams, Devices). The API communication is built in 
						and you don't have to program anything to get started. Adding a bot and creating making it interactive takes less than a minute with this tool.</p>

						<h2>Getting started with bot mangement</h2><h3>Primary bot</h3>

						<p>The primary bot is the first bot that is added through the setup wizard. It does not matter which bot is primary, but it is required to have a primary bot.
						The primary bot makes background requests and lookups to Webex Teams when needed to update certain elements. This is fully transparent. You can anytime promote a bot
						to be the primary bot in the bot settings.</p> 

						<h3>My bots</h3>

						<p>My bots is a dashboard that provides you a quick overview of the bots you have added. You can simply add more bots by typing in
						their e-mail address in the "Add bots" field. From this page you can see how many webhooks, responses, excluded spaces the bot is 
						configured with. If the bot has a accesstoken, is restricted or configured as the primary bot.</p>

						<p>Once a new bot is added, you must provide a valid access token (required) to be able to send requests on behalf of the bot. Click the bot to access the bot profile
						 to add an access token.</p>

						<h3>Bot profile</h3>
						The bot profile page will display the bots details, avatar and a set of options.

						<table class='rounded'>
							<tr>
								<td><b>Options</b><td><b>Summary</b>
							<tr>
								<td>ME<td>This page will display the bot details from the Webex Teams API in a list. 
							<tr>
								<td>ACTIVTITY<td>Select which activity you want to do with the bot, send messages, manage devices or teams and spaces on behalf of the current bot. 
							<tr>
								<td>RESPONSES<td>Define which commands the bot should respond to and what the response should be. You can also configure a response to trigger an external task. See the API guide for more about external tasks.
							<tr>
								<td>WEBHOOKS<td>Create webhooks for the bot, either custom or use the pre-defined (recommended). You can create a 1:1 filter webhook or a group filter webhook. See the webhooks section for more information. 
							<tr>
								<td>SETTINGS<td>The settings page is where you configure the bot basics. Here you can update the bot details (if you change its avatar or update the displayname etc.), update the access token, enable domain restriction, set the default response
												or promote the bot to primary. If the bot is already primary, you will see a checkmark. From this page you can also delete the bot.  
						</table>

						<h3>Webhooks</h3>

						<p>Two webhooks are automatically generated for the first bot that is added through the setup wizard (you could also choose to not enable this by default). These two webhooks are pre-built as a 1:1 filter webhook and a group filter webhook. </p>

						<table class='rounded'>
							<tr>
								<td><b>Type</b><td><b>Description</b>
							<tr>
								<td>1:1 filter<td>We receive an event when someone is talking to the bot privately. 
							<tr>
								<td>Group filter<td>We recieve an event when someone is talking to the bot in a space with many people. 
						</table>

						<p>The Webex Bot Manager do not currently support other webhooks than messages created but it does allow you to create and keep track of custom webhooks that may go to other web services as well. You also get the oppertunity to easily delete a bot webhook.</p>

						<p>The pre-created webhooks are called "direct" and "group" for a reason. The webhook that has the name "group" has a a built in access restriction, which means you can control who can talk to the bot in a group space. If you create your own group webhook to the internal API and give it a different name than group,
						it will bypass the built in restriction and the bot will reply in groups to everyone (domain restriction still apply, please see the domain restriction section). </p>

						<p><b>Three levels of access for the group webhook</b></p>

						<table class='rounded'>
							<tr>
								<td><b>Type</b><td><b>Description</b>
							<tr>
								<td>Individual access<td>You can grant a single (or several) person(s) access to talk to the bot in a group space. You can grant individual access through the bot (admin controls) if enabled, or through "spaces & teams". <br><br>
														 Bot command: <b>admin spaceresponse email@domain.com</b>
														     
							<tr>
								<td>Space access<td>We recieve an event when someone is talking to the bot in a space with many people. 
							<tr>
								<td>Global access<td>We recieve an event when someone is talking to the bot in a space with many people. 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

