<?php if ($windowid != "login" and !verify()) redirect("login.php"); 
$subpage = issetor($_GET['sub']);
$botid = issetor($_GET['botid']);
$maininfo = $db_local->botGetMainInfo();
$base = "index.php?id=base";
$link = "$base&sub=profile&botid={$botid}&apiq=";
$exec = "sites/execution/bots_exec.php";
$botid_form_hidden = "<input type='hidden' value='$botid' name='botid'>";
$tooltips = array(
		'responseKeyword'=>tooltip('The keyword','The keyword is the first word that the user types to the bot. If the user types the keyword, the bot will send the "Response" back to the user. For example if the keyword is "hi" and the response is "hello", if the user types "hi there" or "hi", the bot will respond with "hello".'),
		'createResponse'=>tooltip('Create a response','In order to use the two way communication feature, the server must be publically hosted and the bot must have at least one syncrynized webhook'),
		'addBot'=>tooltip('Bot access token','You can add as many bots you want as long as you provide the accesstoken! Log into https://developer.webex.com to create more bots!'),
		'taskTrigger'=>tooltip('Task trigger','The task trigger will forward the keyword and whatever the user typed in to the task API. While the details are forwarded, the tool will send the "Response" to the user, which could be "Please wait..". An external script or application can pick up the information from the task API and use that to execute custom tasks which means, just about anything. Please read the API guide for information on how to get started!'),	
		'fileUrl'=>tooltip('File URL','Type in a reachable URL that is pointing to a file, the bot will send the file as an attachment back to the user when this response is triggered. Please note that large files are not recommended due to the added wait time.'),
		'accessGroup'=>tooltip('Access group','If an access group is set on a keyword, the user who tries to trigger the response must be part of the access group. If the user is not part of the access group the response will be blocked. The bot will let the user know that the user does not have access.'),
		'response'=>tooltip('Response','The text typed in this field is sent back to the user if triggered by the keyword. A plain keyword response could be "help" as the keyword and the response would be the a help document that tells the user what the bot can do (list the other keywords for example).'),
);
//quickhooks
if (isset($_POST['createhook_group'])) {
	$whid = $spark->webhookCreateQuick($botid, 'group');
	redirect("");
	
} 
else if (isset($_POST['createhook_direct'])) {
	$whid = $spark->webhookCreateQuick($botid, 'direct');
	redirect("");
}

if (isset($_POST['addbottoken'])) {
	if ($spark->wizardAddBot($_POST['addbottoken'])) {
		echo alerts('success', 'Success!', 'Webex Bot has been added');
	} else echo alerts('error', 'Adding Bot was unsuccessful', 'Verify the Bot Access Token is correct');;
} 

if (isset($_POST['deletebot'])) {
	$deleteid = $db_local->quote($_POST['deletebot']);
	if ($botid) {
		$db_local->botDelete($botid);
		echo alerts('success', 'Success!', 'Bot Deleted', 'index.php?id=bots');
	}
}
if (isset($_POST['addtoken'])) {
	$result = $db_local->botUpdateSettings($botid,$db_local->quote($_POST['addtoken']));
	echo $result;
	if($result == 1)
	{
		echo alerts('success', 'Success!', 'Access Token has been updated', 'index.php?id=tasks');
	}
}
if (isset($_POST['domaintrigger'])) {
	$db_local->botDeleteAllowedDomain($botid);
	if (isset($_POST['botdomains'])) {
		foreach ($_POST['botdomains'] as $key => $domainid){
			$db_local->botAddAllowedDomain($domainid, $botid);
		}
	}
}
if (isset($_POST['defres'])) $db_local -> botSetDefaultResponse($db_local->quote($_POST['defres']), $botid);
if (isset($_GET['setprimary'])) $db_local->botSetPrimary($botid);

function submenu() {
	global $link;

	echo '
		<div class="card-header p-2">
			<ul class="nav nav-pills">';
				echo '
				<li class="nav-item"><a class="nav-link" href="' . $link . 'settings">Settings</a></li>
				<li class="nav-item"><a class="nav-link" href="' . $link . 'response">Responses</a></li>
				<li class="nav-item"><a class="nav-link" href="' . $link . 'webhooks">Webhooks</a></li>
			</ul>
		</div>
	';
	// echo '
	// <div class="btn-group">
 //        <a href="' . $link . 'settings"><button type="button" class="btn btn-info btn-md">Settings1</button></a>
 //        <a href="' . $link . 'response"><button type="button" class="btn btn-info btn-md">Responses</button></a>
 //        <a href="' . $link . 'webhooks"><button type="button" class="btn btn-info btn-md">Webhooks</button></a>
	// </div>';
}

$bots = $db_local->botFetchBots();
$botinfo = (issetor($botid)) ? $db_local->botFetchBots($botid)[0] : "";
echo "<!-- Content Header (Page header) -->
<div class='content-header'>
	<div class='container-fluid'>
		<div class='row mb-2'>
			<div class='col-sm-6'>
				<h1 class='m-0 text-dark'>Webex Bots</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
 <div class='content'>
      <div class='container-fluid'>";

//PROFILE
if ($subpage == "profile") {
	if (isset($_GET['updatebot'])) {
		$data = array('sender' => $maininfo['id'], 'recepientType' => 'id', 'recepientValue' => $botinfo['id']);
		$db_local->botUpdate($spark->peopleGet($data));
		$botinfo = $db_local->botFetchBots($botid)[0];
		var_dump($botinfo);
	}
	//$profile = $generate->botGenProfile($botinfo);
	$action = issetor($_GET['apiq']);
	//echo "<div>{$profile}</div>";
	if (empty($botinfo['access'])) {
		echo "<div id='input'><table class='smallform'><tr><td><form method='post' action='index.php?id=bots&sub=profile&botid=$botid&apiq=settings&addaccess' enctype='multipart/form-data'>
		<font color='red'><b>BOT ACCESS TOKEN IS MISSING - PLEASE ADD AN ACCESS TOKEN FOR THIS BOT:</b></font><input type='text' name='addtoken' placeholder='ACCESSTOKEN'><input type='submit' name='tokenadd' value='Add token'> <input type='submit' $link_confirm name='deletebot' class='cancel' value='Delete bot'>
		</form></table></div>";
		die();
	}
	else {
		echo "
		<nav class='navbar navbar-static-top'>
		<div class='collapse navbar-collapse' id='navbar-collapse'  style='background-color:#03bbea'>
          <ul class='nav navbar-nav'>
			<li><a href='{$link}settings'><font color='black'>Settings</font></a></li>
            <li><a href='{$link}response'><font color='black'>Responses</font></a></li>
            <li><a href='{$link}webhooks'><font color='black'>Webhooks</font></a></li>
          </ul>
        </div>
		</nav>
		";
	}
	
	$profile = $spark->peopleGetMe($botinfo['id']);
	//$profile = $db_local->botFetchBots($botinfo['id'])[0];
	
	?>
	<div class="row">
		<div class='col-md-4'>
			<div class='card card-widget widget-user-2'>
				<div class='widget-user-header' style="background-color:#03bbea">
					<div class='widget-user-image'>
						<img class='img-circle elevation-2' src='images/bots/<?php echo $profile['id']; ?>.jfif' alt='Bot Icon'>
					</div>
					<h3 class='widget-user-username'><?php echo $profile['displayName']; ?></h3>
					<h5 class='widget-user-desc'>Webex Teams Bot</h5>
				</div>
				<div class='card-footer p-0' style="background-color:white">
					<ul class='nav flex-column'>
						<li class='nav-item'><div class='nav-link'>NickName <span class='float-right'><?php echo $profile['nickName']; ?></span></div></li>
						<li class='nav-item'><div class='nav-link'>Email <span class='float-right'><?php echo $profile['emails'][0]; ?></span></div></li>
						<li class='nav-item'><div class='nav-link'>Created <span class='float-right'><?php echo $profile['created']; ?></span></div></li>
						<li class='nav-item'><div class='nav-link'>Status <span class='float-right'><?php echo $profile['status']; ?></span></div></li>
						<li class='nav-item'><div class='nav-link'>Type <span class='float-right'><?php echo $profile['type']; ?></span></div></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div align="center">
					<?php submenu(); ?>
				</div>
            </div>
        </div>
    </div>

	<?php
	switch ($action) {
		case "webhooks":
			if (isset($_POST['webhook_setaccess'])) {
				$webhookid = issetor($_POST['webhookid']);
				$groupid = issetor($_POST['whgroup']);
				if ($webhookid and is_numeric($groupid)) {
					$db_local->webhookSetAccessGroup($webhookid, $groupid);
				}
			}
			?>
			<div class='row'>
				<div class='col-lg-3'>
					<div class='card card-primary card-outline'>
						<div class='card-header'>
							<h3 class='card-title'>Create Webhooks</h3>
						</div>
						<div class='card-body'>
							<form id='quickhook' method='post' enctype='multipart/form-data'>
								<input type='submit' name='createhook_group' class="btn btn-md btn-primary btn-block" value='Create group conversation webhook (Automatic)'> 
								<input type='submit' name='createhook_direct' class="btn btn-md btn-primary btn-block" value='Create direct conversation webhook (Automatic)'> 
								<button type="button" class="btn btn-md btn-primary btn-block" data-toggle="modal" data-target="#modal-md">
                  				Create new webhook (Manual)
                			</button>
							</form>
						</div>
					</div>
				</div>
				<div class='col-lg-9'>
					<div class='card card-primary card-outline'>
						<div class='card-header'>
							<h3 class='card-title'>My Webhooks</h3>
						</div>
						<div class='card-body'>
							<?php

								foreach ($spark->webhookGet($botid)['items'] as $key => $value)
								{ 
									if(!empty($value['id'])){ $webhookid = $value['id']; } else { $webhookid = '';}
									if(!empty($value['name'])){ $name = $value['name']; } else { $name = '';}
									if(!empty($value['targetUrl'])){ $targeturl = $value['targetUrl']; } else { $targeturl = '';}
									if(!empty($value['resource'])){ $resource = $value['resource']; } else { $resource = '';}
									if(!empty($value['event'])){ $event = $value['event']; } else { $event = '';}
									if(!empty($value['filter'])){ $filter = $value['filter']; } else { $filter = '';}
									if(!empty($value['orgId'])){ $orgid = $value['orgId']; } else { $orgid = '';}
									if(!empty($value['createdBy'])){ $createdby = $value['createdBy']; } else { $createdby = '';}
									if(!empty($value['appId'])){ $appid = $value['appId']; } else { $appid = '';}
									if(!empty($value['ownedBy'])){ $ownedby = $value['ownedBy']; } else { $ownedby = '';}
									if(!empty($value['status'])){ $status = $value['status']; } else { $status = '';}
									if(!empty($value['created'])){ $created = $value['created']; } else { $created = '';}

									$sync = $db_local->adminCheckWebhookExists($webhookid, $botid);
						
									$resyncmsg = $resync = "";
									
									if (!$sync) {
										$resync = "<input type='submit' value='Re-sync webhook' class='btn btn-md btn-warning btn-block' name='resync'>";
										
									}
									$exists = onoff($sync);
									$disable = ($sync) ? "":"disabled";
									$accessgroup = $db_local->webhookGetAccessGroup($webhookid);
									$groupoptions = $generate->groupOptions($accessgroup);
									$resyncmsg = ($sync) ? "This webhook is in sync!":"This webhook is not in sync with the database and triggers will not work. Please re-sync";

									echo "<div class='row mb-2 border'> 
											<div class='col-lg-8 table-responsive' style='overflow: auto'>
												<table width='100%'>
													<tr><td width='10%'><b>ID</b></td><td>{$webhookid}</td></tr>
													<tr><td><b>Name</b></td><td>{$name}</td></tr>
													<tr><td><b>Target URL</b></td><td>{$targeturl}</td></tr>
													<tr><td><b>Resource</b></td><td>{$resource}</td></tr>
													<tr><td><b>Event</b></td><td>{$event}</td></tr>
													<tr><td><b>Filter</b></td><td>{$filter}</td></tr>
													<tr><td><b>Org Id</b></td><td>{$orgid}</td></tr>
													<tr><td><b>Created By</b></td><td>{$createdby}</td></tr>
													<tr><td><b>App Id</b></td><td style='word-break: break-all;'>{$appid}</td></tr>
													<tr><td><b>Owned By</b></td><td>{$ownedby}</td></tr>
													<tr><td><b>Status</b></td><td>{$status}</td></tr>
													<tr><td><b>Created</b></td><td>{$created}</td></tr>
												</table>
												<br>
											</div>
											<div class='col-lg-1'>

											</div>
											<div class='col-lg-3'>
											<form action='#$webhookid' id='$webhookid' method='post' enctype='multipart/form-data'>
												<div class='row mb-2'>
													<b>Database sync:</b>&nbsp;&nbsp;&nbsp;$exists&nbsp;&nbsp;&nbsp;$resyncmsg
													$resync
												</div>
												<div class='row'>
													<b>Options:</b> 
												</div>
												<div class='row mb-2'>
													<select $disable name='whgroup' class='form-control'>
														<option value='0'>No access group selected</option>
														$groupoptions
													</select>
												</div>
												<div class='row mb-2'>
													<input type='submit' $disable value='Set accessgroup' class='btn btn-md btn-primary btn-block' name='webhook_setaccess'>
													<input type='hidden' value='$webhookid' name='webhookid'> 
												</div>
												<div class='row mb-2'>
													<input type='submit' value='Delete webhook' class='btn btn-md btn-danger btn-block' name='delete_webhook' {$link_confirm} class='cancel'>
												</div>
											</form>
										</div>	
								  	</div>";
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal-md">
			<div class="modal-dialog modal-md">
			  <div class="modal-content">
			    <div class="modal-header">
			      <h4 class="modal-title">Create Manual Webhook</h4>
			      <button type="button" class="close" aria-label="Close">
			        <span aria-hidden="true">&times;</span>
			      </button>
			    </div>
			    <div class="modal-body">
			      <form name='webhooks' method='post' action='index.php?id=bots&sub=profile&botid=<?php echo $botinfo['id'];?>&apiq=webhooks' enctype='multipart/form-data'>
						<div class="form-group">
							<label for="whichbot" class="col-sm-4 control-label">Which Bot:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="whichbot" placeholder="<?php echo $botinfo['displayName']; ?>" disabled>
								<input type='hidden' value='<?php echo $botinfo['id']; ?>' name='sender'>
							</div>
						</div>
						<div class="form-group">
							<label for="webhookname" class="col-sm-4 control-label">Webhook name:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="webhookname" placeholder="Name" name="name">
							</div>
						</div>
						<div class="form-group">
							<label for="targeturl" class="col-sm-4 control-label">Target URL:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="targeturl" placeholder="Target URL" name="targeturl">
							</div>
						</div>
						<div class="form-group">
							<label for="resource" class="col-sm-4 control-label">Resource:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="resource" placeholder="Messages" name="resource">
							</div>
						</div>
						<div class="form-group">
							<label for="event" class="col-sm-4 control-label">Event:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="event" placeholder="Created" name="event">
							</div>
						</div>
						<div class="form-group">
							<label for="filter" class="col-sm-4 control-label">Filter:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="filter" placeholder="roomId=xx&roomType=xx" name="filter">
							</div>
						</div>
						<div class="form-group">
							<label for="secret" class="col-sm-4 control-label">Secret:</label>
							<div class="col-sm-12">
								<input type="text" class="form-control" id="secret" placeholder="Random / Optional" name="secret">
							</div>
						</div>
			    </div>
			    <div class="modal-footer justify-content-between">
			      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      <input type='submit' name='webhook_create' class="btn btn-primary" value='Create webhook for this bot'>
			      </form>
			    </div>
			  </div>
			  <!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

				<?php 
					if(isset($_POST['webhook_create'])) {								
						$result = $spark->webhookCreate($_POST);
						$db_local->webhookDbLink($result['id'], $botid);
						redirect("#{$result['id']}");
					}
					if (isset($_POST['delete_webhook'])) {
						$webhookid = issetor($_POST['webhookid']);
						if ($webhookid) {
							$spark->webhookDelete($webhookid, $botid);
							$db_local->webhookDbUnlink($webhookid);
						}
					}
					if (isset($_POST['resync'])) {
						$result = $spark->webhookGet($botid);
						$webhookid = issetor($_POST['webhookid']);
						if ($webhookid and count($result['items'])) {
							foreach ($result['items'] as $key => $value) {
								if ($webhookid == $value['id']) {
									$db_local->webhookDbLink($value['id'], $botid);
								}
								
							}
						}
					}		
					
				echo "</div>
				</div>";
					
				break;
				case "response":
					if (isset($_GET['setfeature'])) {
						$db_local->adminSetFeature($_GET['setfeature'],$botid);
					}
					if (isset($_GET['edit'])) {
						$action_sub = "&update={$_GET['edit']}";
						$values = $db_local->responseFetchResponses("",$_GET['edit'])[0];
						$selected = ($values['is_task'] == 1) ? "selected" : "";
						$f_selected = ($values['is_feature'] == 1) ? "selected" : ""; 
					}
					$group_dropdown = "<select name='accessgroup' class='form-control'><option value='0'>none</option>";
					$group_data = $db_local->groupFetchGroups();		
					foreach ($group_data as $key) {			
						$group_selected = "";
						if (isset($_GET['edit']) and isset($values['accessgroup'])) {
							if ($key['id'] == $values['accessgroup'])
								$group_selected = "selected";
							}
							$group_dropdown .= ($key['botid'] == "0" or $key['botid'] == $botid) ? "<option value='{$key['id']}' $group_selected>{$key['groupname']}</option>":"";
						}
						$group_dropdown .= "</select>";
					?>

					<div class='row'>
						<div class='col-lg-3'>
							<div class='card card-primary card-outline'>
								<div class='card-header'>
									<h3 class='card-title'>Bot built-in features (on/off)</h3>
								</div>
								<div class='card-body'>
									<table class="table">
										<tr>
											<th>Feature</th>
											<th>On/Off</th>
										</tr>
										<?php 
										$features = $db_local->adminGetFeatures();
										foreach ($features as $key=>$feature) {
											$current = $db_local->responseFetchResponse($botid, $feature['keyword']);
											if(empty($current))
											{
												$current = array();
											}
											$en = (count($current)>0 and $current['is_feature'] == '1') ? true:false;
											$featuredesc = tooltip($feature['title'], $feature['description']);
											echo "
												<tr>
													<td>$featuredesc {$feature['title']}</td>
													<td><a href='index.php?id=bots&sub=profile&botid=$botid&apiq=response&setfeature={$feature['id']}'>".onoff($en,'Disable','Enable')."</td>
												</tr>";
										}
										?>
									</table>
								</div>
							</div>
						</div>
						
						<?php
							if(!empty($_GET['edit'])){ $responsetitle = 'Edit Response'; } else { $responsetitle = 'Create Response'; }
						?>
						<div class='col-lg-9'>
							<div class='card card-primary card-outline'>
								<div class='card-header'>
									<h3 class='card-title'><?php echo $responsetitle; ?></h3>
								</div>
								<div class='card-body' style='overflow: auto'>
									<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo issetor($botinfo['id']) . issetor($action_sub);?>&apiq=response' enctype='multipart/form-data'>
										<div class="form-group">
											<label for="Headline"><?php echo $tooltips['taskTrigger']; ?> Trigger external task (API):</label>
											<select name='is_task' class="form-control">
												<option value='0'>No</option>
												<option value='1' <?php echo issetor($selected) ?>>Yes</option>
											</select>	
										</div>
										<div class="form-group">
											<label for="Headline"><?php echo $tooltips['responseKeyword']; ?> Keyword:</label>
											<input type='text' required='' class="form-control" placeholder='Keyword' name='keyword' value='<?php echo issetor($values['keyword']);?>'>
											<input type='hidden' value='<?php echo issetor($botinfo['id']);?>' name='botid'>
										</div>
										<?php 
											if (isset($_GET['edit'])) {
												$editid = issetor($_GET['edit']);
												echo "<input type='hidden' value='$editid' name='id'>";
											}
										?>
										<div class="form-group">
											<?php echo $tooltips['response']; ?> <label for="Message">Response:</label>
											<link rel="stylesheet" href="editormd/css/editormd.css" />

											<div id="test-editor" id="Message" class="form-control">
												<textarea required='' placeholder='Response to keyword' name='response' rows='20' cols='40'><?php echo issetor($values['response']);?></textarea>
											</div>
										</div>
										<div class="form-group">
											<?php echo $tooltips['fileUrl']; ?> <label for="Headline">File URL:</label>
											<input type='text' placeholder='URL' name='file_url' class="form-control" value='<?php echo issetor($values['file_url']);?>'>
										</div>
										<div class="form-group">
											<?php echo $tooltips['accessGroup']; ?> <label for="Headline">Access group:</label>
											<?php echo $group_dropdown;?>
										</div>
										<div class="form-group">
											<?php echo $tooltips['createResponse']; ?> <label for="Headline">Save response:</label>
											<input type='submit' class="btn btn-md btn-primary btn-block" value='Save response'>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class='col-lg-12'>
							<div class='card card-primary card-outline'>
								<div class='card-header'>
									<h3 class='card-title'>My Responses</h3>
								</div>
								<div class='card-body' style='overflow: auto'>
									<table width='100%' id='tasks' class='table table-bordered table-striped'>
										<thead>
											<tr>
												<th>Keywords</th>
												<th>Responses</th>
												<th>File URL</th>
												<th>Task</th>
												<th>Feature</th>
												<th>Protected</th>
												<th>Modify</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if(isset($_POST['keyword']) and isset($_POST['response']) and !isset($_GET['update'])) {									
												echo $db_local->errorHandler($db_local->responseCreate($_POST));																			
												}
												if (isset($_GET['delete'])) {
													$delete_id = $_GET['delete'];
													$db_local->responseDelete($delete_id);
												}
												if (isset($_GET['update'])) {
													$update_id = $_GET['update'];
													if(isset($_POST['keyword']) and isset($_POST['response']) and isset($_GET['update'])) {
														echo $db_local->errorHandler($db_local->responseUpdate($_POST));
													}
												}

												$responses = $db_local->responseFetchResponses($botid);
												foreach ($responses as $key => $value) {
												if ($value['accessgroup'] != 0) {
													$group = $db_local->groupFetchGroups($value['accessgroup']);
													$group_title = (issetor($group[0]['groupname'])) ? $group[0]['groupname']:"404";
													$location = "index.php?id=groups&viewgroup={$value['accessgroup']}";
												}
												else {
													$group_title = "Everyone";
													$location = "#";
												}
												$modaljson = json_encode($value, JSON_UNESCAPED_UNICODE);
												$is_feature = onoff($value['is_feature'],"Feature","Not a Feature");
												$is_task = onoff($value['is_task'],"Task","Not a Task");
												$response = substr($value['response'],0,20) . '...';
												$response = str_replace("<", "", str_replace(">", "", $response));
												$accessgroup = ($group_title == "404") ? warning("This response is protected but the accessgroup does NOT EXIST! Please update the access group for this response") : "<a title='{$group_title}' href='{$location}'>".lockunlock($value['accessgroup'],"Restricted to {$group_title}","Unrestricted")."</a>";
												echo "<tr>
												<td> <b>{$value['keyword']}</b></td>
												<td>$response</td>
												<td>{$value['file_url']}</td>
												<td>$is_task</td>
												<td>$is_feature</td>
												<td>{$accessgroup}</td>
												<td><a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=response&edit={$value['id']}'>".actionButton("edit", "Edit")."</a>&nbsp;&nbsp;<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=response&delete={$value['id']}' {$link_confirm}>".actionButton("delete", "Delete")."</a></td>";
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

				<?php
				break;
				//SETTINGS
				case "settings":
					 //$me = $spark->peopleGetMe($botid);
					 // echo "<pre>";
					 // var_dump($me);
					 // echo "</pre>";
					 $myid = issetor($profile['id']);		
					$validity = onoff(($myid == $botid), "Valid", "Not Valid", $textTrue='Token is valid for this bot!', $textFalse='Token is not valid for this bot!');
					// $spark->download_image($me['avatar'], 'images/bots/'.$me['id'].'.jfif');
					?>
					<div class="row">
						<div class="col-12">
							<div class="card card-primary card-outline">
								<div class='card-header'>
									<h3 class='card-title'>Settings</h3>
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-12">
											<div class='row mb-2 mb-md-1'>
												<div class="col-md-3 mb-1">
													<b>BotId:</b>
												</div>
												<div class="col-md-6 mb-2">
													<?php echo $botinfo['id']; ?>
												</div>
												<div class="col-md-3">
									
												</div>
											</div>
											<div class='row mb-2 mb-md-1'>
												<div class="col-md-3 mb-1">
													<b>Current access token:</b>
												</div>
												<div class="col-md-6 mb-2">
													<?php echo $botinfo['access']; ?>
												</div>
												<div class="col-md-3">
													<?php echo $validity; ?>
												</div>
											</div>
											<form method='post' action='#' enctype='multipart/form-data'>
												<div class='row mb-2 mb-md-1'>
													<div class="col-md-3 mb-1">
														<b>Update access token:</b>
													</div>
													<div class="col-md-6 mb-2">
														<input type='text' name='addtoken' class="form-control" placeholder='New access token'>
													</div>
													<div class="col-md-3">
														<input type='submit' value='Update token' class="btn btn-md btn-primary btn-block"></form>
													</div>
												</div>
											</form>
											<form method='post' action='#' enctype='multipart/form-data'> 
												<div class='row mb-2 mb-md-1'>
													<div class="col-md-3 mb-1">
														<b>Delete bot and all relations:</b>
													</div>
													<div class="col-md-6 mb-2">
														<a tabindex="0" role="button" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="Deleting a bot" data-content="Deleting a bot will remove all the responses and configurations related to the bot. All the groups owned by this bot will be made public (not removed)." ><i class='icon fa fa-exclamation-triangle'></i> This will purge the bot and all its configurations / responses and webhooks</a>
													</div>
													<div class="col-md-3">
														<?php echo $botid_form_hidden; ?>
														<input type='submit' value='Purge bot' name='deletebot' <?php echo $link_confirm ?> class="btn btn-md btn-danger btn-block">
													</div>
												</div>
											</form>
											<div class='row mb-2 mb-md-1'>
												<div class="col-md-3 mb-1">
													<b>Restricted response</b>
												</div>
												<div class="col-md-6 mb-2">
													<?php echo onoff($db_local->adminCheckBotRestriction($botid),'Restricted, bot will reply to specific mail domains!','Unrestricted, bot will reply to everyone!');
													echo ($db_local->adminCheckBotRestriction($botid)) ? "<font color=$pos_color> Bot has an active restriction policy!</font>":warning("Please consider restricting the bot by adding a domain!") . "<font color=$neg_color> Bot has no restriction policy and can reply to any domains!</font>"; ?>
												</div>
												<div class="col-md-3">
												
												</div>
											</div>
											<form method='post' action='#' enctype='multipart/form-data'>
												<div class='row mb-2 mb-md-1'>
													<div class="col-md-3 mb-1">
														<b>Bot responds to these mail domains (Enables restriction)</b>
													</div>
													<div class="col-md-6 mb-2">
														<input type='hidden' name='domaintrigger'>	
														<select class='select2' multiple='multiple' name='botdomains[]' style='width: 100%;'>";
															<?php 
															$domains = $db_local->acceptedDomainFetch();
															foreach ($domains as $key => $value) {
																echo ($db_local->botCheckDomain($value['id'], $botid)) ? "<option value='{$value['id']}' selected>{$value['domain']}</option>" : "<option value='{$value['id']}'>{$value['domain']}</option>";
															}
															?>
														</select>
													</div>
													<div class="col-md-3">
														<input type='submit' value='Apply selection' class="btn btn-md btn-primary btn-block">
													</div>
												</div>
											</form>
											<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo $botid;?>&apiq=settings' enctype='multipart/form-data'>
												<div class='row mb-2 mb-md-1'>
													<div class="col-md-3 mb-1">
														<b>Default response:</b>
													</div>
													<div class="col-md-6 mb-2">
														<input type='text' class='form-control' value='<?php echo $botinfo['defres']; ?>' name='defres' placeholder='Unknown command, please type ... to see a list of commands'>
													</div>
													<div class="col-md-3">
														<input type='submit' value='Apply default response' class="btn btn-md btn-primary btn-block">
													</div>
												</div>
											</form>
											<form method='post' action='index.php?id=bots&sub=profile&botid=<?php echo $botid;?>&apiq=settings&updatebot' enctype='multipart/form-data'>
												<div class='row mb-2 mb-md-1'>
													<div class="col-md-3 mb-1">
														<b>Update bot details:</b>
													</div>
													<div class="col-md-6 mb-2">
													
													</div>
													<div class="col-md-3">
														<input type='submit' value='Fetch bot details from Webex Teams' class="btn btn-md btn-primary btn-block">
													</div>
												</div>
											</form>
											<div class='row mb-2 mb-md-1'>
												<div class="col-md-3 mb-1">
													<b>Primary bot:</b>
												</div>
												<div class="col-md-6 mb-2">
												
												</div>
												<div class="col-md-3">
													<?php echo ($db_local->adminCheckIsBotMain($botid)) ? onoff(true,20,20):"<form method='post' action='index.php?id=bots&sub=profile&botid=$botid&apiq=settings&setprimary' enctype='multipart/form-data'><input type='submit' class='btn btn-primary btn-block' value='Set as primary bot!'></form>";?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 			
						if (isset($_GET['removespace'])) {
							$db_local->adminRemoveJoinableSpace($botid,$_GET['removespace']);
						}
						if (count($spaces = $db_local->adminGetJoinableSpace($botid))>0) {
							echo "<h3>My joinable spaces</h3><hr class='gradient'>
									<table class='rounded'><tr><td bgcolor='#737CA1'><b>Joinable spaces (Spaces joinable via this bot)</b><td bgcolor='#737CA1'><b>Manage</b><tr><td>Options<td><a href='index.php?id=bots&apiq=settings&sub=profile&botid=$botid&joinableupdate'>Update all</a> - Purge all";
							foreach ($spaces as $key => $value) {
								if (isset($_GET['joinableupdate'])) {
									$test = $spark->roomGetDetails($botid,$value['spaceid']);
									if (!isset($test['id'])) {
										$db_local->adminRemoveJoinableSpace($botid,$value['spaceid']);
									}
									else {
										$db_local->adminUpdateJoinableSpace($test['id'], $test['title']);
										$value['spacetitle'] = $test['title'];
									}
								}
								echo "<tr><td>{$value['spacetitle']}<td><a href='index.php?id=bots&sub=profile&apiq=settings&botid=$botid&removespace={$value['spaceid']}'>Remove</a>";
							}
							echo "</table>";
						}
						//Checks if this bot has some excluded spaces where the bot will reply to all people in that partiucular space
						//Can update the name on all spaces and check if the space still exists, if not it will delete the space from the database
						if (count($excluded_spaces = $db_local->adminGetGroupResponseAcl($botid))) {
							echo "<br><table class='rounded'><tr><td bgcolor='#737CA1'><b>Excluded spaces (Group response enabled for this bot)</b><td bgcolor='#737CA1'><b>Manage</b><tr><td>Options<td><a href='index.php?id=bots&sub=profile&apiq=settings&botid=$botid&excludedupdate'>Update all</a> - Purge all";
							if (isset($_GET['removeexcluded'])) {
								$db_local->adminRemoveGroupResponseAcl($botid,$_GET['removeexcluded']);
								$excluded_spaces = $db_local->adminGetGroupResponseAcl($botid);
							}
							foreach ($excluded_spaces as $key => $value) {
								if (isset($_GET['excludedupdate'])) {
									$test = $spark->roomGetDetails($botid,$value['id']);
									if (!isset($test['id'])) {
										$db_local->adminRemoveGroupResponseAcl($botid,$value['id']);
										continue;
									}
									else {
										$db_local->adminUpdateGroupResponseAcl($test['id'], $test['title']);
										$value['spacetitle'] = $test['title'];
									}
								}
								if ($value['spacetitle'] == "") $value['spacetitle'] = "Unknown, please update!";
								echo "<tr><td>{$value['spacetitle']}<td><a href='index.php?id=bots&sub=profile&apiq=settings&botid=$botid&removeexcluded={$value['id']}'>Remove</a>";
							}
							echo "</table>";
						}
				echo "</div>
				</div>";		
				default:
				break;
			}
		}
			else {
				echo '
				<div class="row">
					<div class="col-lg-8">
						<div class="card card-primary card-outline">
							<div class="card-header">
								<h3 class="card-title">My Bots</h3>
							</div>
							<div class="card-body" style="overflow: auto">
								<table id="bots" class="table table-bordered table-striped" width="100%">
									<thead>
										<tr>
											<th>Bot Icon</th>
											<th>Bot Name</th>
											<th>Responses</th>
											<th>Webhooks</th>
											<th>Excluded spaces</th>
											<th>Has access token?</th>
											<th>Restricted?</th>
											<th>Primary?</th>
										</tr>
									</thead>
									<tbody>';
									foreach ($bots as $key => $value) 
									{
										$name = $value['displayName'];
										
										$filename = "images/bots/{$value['id']}.jfif";

										if (!file_exists($filename)) {
											$spark->download_image($value['avatar'], 'images/bots/'.$value['id'].'.jfif');
										}
					
										$image = "<img class='img-circle' src='images/bots/{$value['id']}.jfif' height=20 width=20 border=0>";
										$responses = count($db_local->responseFetchResponses($value['id']));
										$webhooks = count($db_local->select("SELECT * FROM bot_webhook WHERE botid = '{$value['id']}'"));
										$excluded_spaces = count($db_local->adminGetGroupResponseAcl($value['id']));
										$restriction = $db_local->adminCheckBotRestriction($value['id']);
										$warning = ($webhooks > 0 and !$restriction) ? warning("This bot is unrestricted and have webhooks! This means that the bot will respond to everyone talking to it, also outside your organization. Please consider setting up a domain restriction.") : "";
										$n_responses = ($responses > 0) ? colorize_value($pos_color, $responses): colorize_value($inactivecolor, $responses);
										$n_webhooks = ($webhooks > 0) ? colorize_value($pos_color, $webhooks): colorize_value($inactivecolor, $webhooks);
										$n_excluded = ($excluded_spaces > 0) ? colorize_value($pos_color, $excluded_spaces): colorize_value($inactivecolor, $excluded_spaces);
										$primary = ($maininfo['id'] == $value['id']) ? onoff(true,"All requests to Webex Teams API is primarily made with the primary bot"):"";
										$n_restriction = lockunlock($restriction,'Domain restricted!', 'Not domain restricted!');
										$hastoken = onoff((!empty($value['access'])),'Yes', 'No') ;	
								echo "	<tr>
								<td align='center'> <a href='index.php?id=bots&sub=profile&botid={$value['id']}&apiq=settings'>$image</a>
											<td> <a class='linkblock' href='index.php?id=bots&sub=profile&botid={$value['id']}&apiq=settings'>$name</a>
											<td><b/>$n_responses</td>
											<td><b/>$n_webhooks</td>
											<td><b/>$n_excluded</td>
											<td><b/>$hastoken</td>
											<td>$n_restriction $warning</td>
											<td>$primary</td></tr>";
									}
				
								echo'
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card card-primary card-outline">
							<div class="card-header">
								<h3 class="card-title">'.$tooltips["addBot"].' Add more bots</h3>
							</div>
							<div class="card-body" style="overflow: auto">
								<form method="post" action="index.php?id=bots" enctype="multipart/form-data"> 
									<input type="text" class="form-control" name="addbottoken" required placeholder="Bot accesstoken">
									<br>
									<input type="submit" class="btn btn-lg btn-primary btn-block" value="Add bot"> 
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>';
			}		
		?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript">
	$(function() {
		var editor = editormd("test-editor", {
			width  : "100%",
			height : "290px",
			path   : "plugins/editormd/lib/",
			toolbarIcons : function() {
            // Or return editormd.toolbarModes[name]; // full, simple, mini
            // Using "||" set icons align right.
            return ["undo", "redo", "|", "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|", "h1", "h2", "h3", "h3", "h4", "h5", "h6", "|", "list-ul", "list-ol", "hr", "|", "link", "code", "preformatted-text", "code-block", "table", "html-entities", "pagebreak", "|", "goto-line", "unwatch", "preview", "clear", "search"]
        },
		});
	});
</script>