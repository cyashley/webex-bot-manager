<?php 
if ($windowid != "login" and !verify()) redirect("index.php?id=login"); 
$s_n = $d_n = "checked";
$s_y = $d_y = "";
$page_group = issetor($_GET['viewgroup']);
$botid = issetor($_GET['botid']);
$bot = ($botid) ? "&botid=$botid":"";

//Page tooltips
$tooltips = array(
      'groupName'=>tooltip('Group name','Generic group display name'),
      'groupSubscription'=>tooltip('Subscription groups','Allow users to subscribe to the group using the bot subscription feature (must be enabled).'),
      'groupDefault'=>tooltip('Default groups','Users get automatically added to default groups when they are imported or added via a bot. Note that if the user is added using a bot, the user will only be added to the public groups and that specific bots owned groups marked as default.'),
      'groupAlias'=>tooltip('Group alias','Alternate group string identifier i.e. group-1, if nothing is specified the group identifier is a number'),
      'groupBotOwner'=>tooltip('Bot owner','A bot is marked as owner of the group. Allows adding Webex Teams spaces to the group for easy announcments. An owned group may also be subscribable and default but will only have an effect via the bot owner.'),
      'groupDescription'=>tooltip('Group description','Generic group description. The subscription feature will display this description in the subscription overview.'),
       'groupLinking'=>tooltip('Group linking','WBM supports one layer of nested groups. If group-2 and group-3 has real members and are public groups (not owned by any bot), you can link them to another public group or an owned group (group-1) so that the members of group-2 and group-3 also becomes virtual members of group-1. Only real user members can be linked to another group.'),
      'groupCopy'=>tooltip('Copy members','Copy all user members of another groups into this group. Will also copy virtual members as real members into the group.')
);

//Page operations
if (isset($_POST['group_add'])) {
   unset($_POST['group_add']);
   $db_local->groupAdd($_POST);
}

if (isset($_POST['group_update'])) {
   unset($_POST['group_update']);
   //Sub ID = alternate ID of the group (more sane)
   if (!is_numeric($_POST['sub_id'])) {
      $_POST['sub_id'] = str_replace(" ", "", $_POST['sub_id']);
      $run = $db_local->groupUpdate($_POST);
      if (!$run) {
         redirect("index.php?id=groups&viewgroup={$page_group}$bot&feedback=warning-groupNestedBot"); 
      }
   } else {
      echo feedbackMsg("Could not save form, subscription ID cannot be numeric!", "", "alert");
   }
}

if (isset($_POST['delete_group']) and $_POST['id'] != '15') {
   $db_local->groupRemove($db_local->quote($_POST['id']));
   if (!count($db_local->groupFetchGroups($_POST['id']))) {
       redirect("index.php?id=groups&feedback=success-DeleteGroup"); 
   }
   else {
       redirect("index.php?id=groups&feedback=alert-Delete"); 
   }
}
elseif (isset($_POST['delete_group']) and $_POST['id'] == '15') {
   echo $db_local->errorHandler(false);
}

if (isset($_POST['copy'])) {
	$current_group = $_POST['to_group'];
	$from_group = $_POST['from_group'];
	$members_to_copy = $db_local->groupGetMembers($from_group);
	foreach ($members_to_copy as $key => $value) {
		$db_local->groupAddContact(array('groupid'=>$current_group, 'contactid'=>$value['contactid']));
	}
}

if (isset($_GET['execute'])) {
   $botid_e = $db_local->quote(issetor($_GET['botid']));
  
   if (isset($_POST['non_member'])) {
   	$db_local->query("DELETE FROM group_contacts WHERE groupid={$page_group}");
   	foreach ($_POST['non_member'] as $value) {
   		$db_local->groupAddContact(array('groupid' => $page_group, 'contactid' => $db_local->quote($value)));
   	}
   }else{
   	$db_local->query("DELETE FROM group_contacts WHERE groupid={$page_group}");
   }
   
   //Add Spaces to group
   if (isset($_POST['non_space_member'])) {
   	$db_local->query("DELETE FROM group_spaces WHERE groupid = '{$page_group}' and botid = '{$botid}'");
      foreach ($_POST['non_space_member'] as $value) {
         $db_local->groupAddSpace(array('groupid' => $page_group,
               'spaceid' => $db_local->quote($value), 'botid' => $botid_e));
      }
   }else{
   	$db_local->query("DELETE FROM group_spaces WHERE groupid = '{$page_group}' and botid = '{$botid}'");
   }
   //Add Groups to group
   if (isset($_POST['nest'])) {
      $db_local->groupRemoveGroup($_POST['to_group']);
      if (isset($_POST['groups']) and count($_POST['groups'])) {
         foreach ($_POST['groups'] as $value) {
            $db_local->groupAddGroup(array('groupid' => $_POST['to_group'],
                                    'nestedid' => $db_local->quote($value)));
         }
      }
   }
}

if ($page_group) {
      $groupinfo = $db_local->groupFetchGroups($db_local->quote($page_group));
      $form = "&viewgroup={$groupinfo[0]['id']}";
      $actionname = "group_update";
      $submitname = "Update group ";
      $groupname = "{$groupinfo[0]['groupname']}";
      $edit = "Modifying group <b>(" . $groupname . ")</b>";
      list($s_y, $s_n) = ($groupinfo[0]['subscribable']) ? array("checked", ""):array("","checked");
      list($d_y, $d_n) = ($groupinfo[0]['default_group']) ? array("checked", ""):array("","checked");
      $bot_dropdown = $generate->botGenDropdown('botid', $groupinfo[0]['botid']);
}
else {
      $actionname = "group_add";
      $submitname = "Create group";
      $groupname = "";
      $edit = "Create a group";
      $groupinfo = array("0"=>array("id"=>"", "groupname"=>""));
      $form = "";
      $bot_dropdown = $generate->botGenDropdown('botid');
}

?>
<!-- Content Header (Page header) -->
<div class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1 class="m-0 text-dark">Groups</h1>
         </div><!-- /.col -->
      </div><!-- /.row -->
   </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
   <div class="container-fluid">
      <div class='row'>
         <div class='col-lg-6'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'><?php echo $edit; ?></h3>
               </div>
               <div class='card-body'>
                  <?php
                     echo "<form name='groups' method='post' action='index.php?id=groups{$form}$bot' enctype='multipart/form-data'>
                              <div class='form-group'>
                                 <label for='groupname'>{$tooltips['groupName']} Group Name:</label>
                                 <div id='groupname'> 
                                    <input type='text' placeholder='Group Name' required name='groupname' id='groupname' class='form-control' value='".issetor($groupinfo[0]['groupname'])."'>
                                    <input type='hidden' name='id' value='{$groupinfo[0]['id']}'>
                                 </div>
                              </div>
                              <div class='form-group'>
                                 <label for='subscribable'>{$tooltips['groupSubscription']} Subscription group:</label>
                                    <div class='radio'>
                                       <label><input type='radio' name='subscribable' $s_y value='1'>&nbsp;Yes</label>&nbsp;&nbsp;
                                       <label><input type='radio' name='subscribable' $s_n value='0'>&nbsp;No</label>
                                    </div>
                              </div>
                              <div class='form-group'>
                                 <label for='default_group'>{$tooltips['groupDefault']} Default group:</label>
                                    <div class='radio'>
                                       <label><input type='radio' name='default_group' $d_y value='1'>&nbsp;Yes</label>&nbsp;&nbsp;
                                       <label><input type='radio' name='default_group' $d_n value='0'>&nbsp;No</label>
                                    </div>
                              </div>
                              <div class='form-group'>
                                 <label for='sub_id'>{$tooltips['groupAlias']} Group alias:</label>
                                 <div id='sub_id'>
                                    <input type='text' name='sub_id' id='sub_id' placeholder='Alternate group id' class='form-control' value='".issetor($groupinfo[0]['sub_id'])."'>
                                 </div>
                              </div>
                              <div class='form-group'>
                                 <label for='bot_drop'>{$tooltips['groupBotOwner']} Bot owner:</label>
                                 <div id='bot_drop'>
                                    $bot_dropdown
                                 </div>
                              </div>
                              <div class='form-group'>
                                 <label for='description'>{$tooltips['groupDescription']} Description:</label>
                                 <div id='description'>
                                    <textarea placeholder='Group description' name='description' class='form-control'>".issetor($groupinfo[0]['description'])."</textarea>
                                 </div>
                              </div>
                              <div class='box-footer'>
                                 <input type='submit' name='{$actionname}' value='{$submitname}' class='btn btn-md btn-primary' />";
                                 if ($groupinfo[0]['id'] != 15 and $page_group) {
                                 echo "<input type='submit' name='delete_group' title='Delete group and remove all members' style='margin-left: 2px' class='btn btn-danger pull-right' {$link_confirm} value='Delete group'>";
                                 }
                                 if ($page_group) {
                                    echo "<a href='index.php?id=groups' class='btn btn-danger float-right'>Cancel</a>";
                                 }
                              echo "  
                              </div>
                           </form>";
                  ?>
               </div>
            </div>
         </div>
         <div class='col-lg-6'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'>Created Groups</h3>
               </div>
               <div class='card-body'>
                  <?php
                     echo $generate->groupLinks('linksnumbers');
                  ?>
               </div>
            </div>
               <?php

         if ($page_group) {
            $groupslist = $generate->groupLinks('options');
            $groupid = $page_group;

         echo "
         <form name='copy_groups' method='post' action='index.php?id=groups&viewgroup={$groupid}$bot' enctype='multipart/form-data'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'>{$tooltips['groupCopy']} Copy members from another group to this group</h3>
                  <div class='card-tools'>
                     <input type='submit' name='copy' value='Copy' class='btn btn-sm btn-primary'>
                  </div>
               </div>
               <div class='card-body'>
                  <div class='col-md-3'>
                        <select name='from_group' class='form-control'>
                           $groupslist
                        </select>
                     </div>
                     <input type='hidden' name='to_group' value='{$groupid}'>
               </div>
            </div>
         </form>
         <form name='nest_groups' method='post' action='index.php?id=groups&viewgroup={$groupid}$bot&execute' enctype='multipart/form-data'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'>{$tooltips['groupLinking']} Logical group linking</h3>
                  <div class='card-tools'>
                     <input type='submit' name='nest' value='Apply selection' class='btn btn-sm btn-primary'>
                  </div>
               </div>
               <div class='card-body'>";
                  $groupslist = $generate->groupLinks('nestedgroups', $groupid);
                  echo "$groupslist
                  <input type='hidden' name='to_group' value='{$groupid}'>
               </div>
            </div>
         </form>
         </div>
      </div>";

      $contacts = $db_local->contactFetchContacts();
      $current_group = $db_local->quote($_GET['viewgroup']);
      $botidlink = "";
      $num_of_total_members = count($contacts);
      $num_of_members = $db_local->groupMembershipNumber($current_group);
      
   
   if ($botid = issetor($_GET['botid'])) {
      $botvalues = $db_local->botFetchBots($botid);
      $botidlink = "&botid=" . $botid;
   }
   
   echo "<div class='row'>
   <div class='col-lg-6'>
   <form name='members' id='members' method='post' action='index.php?id=groups&viewgroup={$groupinfo[0]['id']}&groupmembers&{$botidlink}&execute#members' enctype='multipart/form-data'>
   <div class='card card-primary card-outline'>
   <div class='card-header'>
   <h3 class='card-title'>User memberships for <b>({$groupname})</b> $num_of_members / $num_of_total_members </h3>
   <div class='card-tools'>
   <input type='submit' name='members' value='Apply changes' class='btn btn-sm btn-primary'>
   </div>
   </div>
   <div class='card-body'>
   <div class='form-group'>
   <select class='duallistboxuser' name='non_member[]' multiple='multiple'>";
   foreach ($contacts as $key => $value)
   {
   	if ($db_local->groupCheckIfMember($value['id'], $current_group)) {
   		$linked_member = "";
   		$disable = "";
   		if (!$db_local->groupCheckIfActualMember($value['id'], $current_group)) {
   			$linked_group_info = $db_local->groupCheckContactGroupLink($current_group, $value['id']);
   			$disable = "disabled";
   			$linked_member = colorize_value($neg_color, " via {$linked_group_info[0]['groupname']}", "");
   			echo "<option value='{$value['id']}'>{$value['firstName']} {$value['lastName']}</option>";
   		}
   		echo "<option selected $disable value='{$value['id']}'>{$value['firstName']} {$value['lastName']} $linked_member</option>";
   	}
   	else {
   		echo "<option value='{$value['id']}'>{$value['firstName']} {$value['lastName']}</option>";
   	}
   }
   // echo "<option hidden value='' selected></option>";
   echo "</select>
                </div>
               </div>
            </div>
            
         </div>
      ";
            
   if ($botid) {
         
   		$spaces = $spark->roomGet(array("sender"=>$botid,"max"=>'500',"type"=>"group"));
   		$num_total_spaces = count($spaces['items']);
   		$removed = 0;
   		$space_members = $db_local->groupGetSpaceMembers($current_group, $botid);
   		foreach ($space_members as $key => $value) {
   			if (!in_array($value['spaceid'],array_column($spaces['items'], 'id'))) {
   				$db_local->groupRemoveSpace($current_group, $value['spaceid'], $botid);
   				$removed++;
   			}
   		}
   		if ($removed) echo feedbackMsg('Spaces removed ', "$removed spaces has been removed from the membership list because the bot is no longer in the space", 'warning');
   		
   		$num_space_members = $db_local->groupSpaceMembershipNumber($current_group, $botid);
   		$num_non_space_members = $num_total_spaces - $num_space_members;
   		$non_space_members = "";
   		
        echo "
            	<div class='col-lg-6'>
            	<div class='card card-primary card-outline'>
            	<div class='card-header'>
            	<h3 class='card-title'>Space memberships for <b>({$groupname})</b> $num_space_members / $num_total_spaces</h3>
            	<div class='card-tools'>
            	<input type='submit' name='members' value='Apply changes' class='btn btn-sm btn-primary'>
            	</div>
            	</div>
            	<div class='card-body'>
            	<div class='form-group'>
            	<select class='duallistboxspace' name='non_space_member[]' multiple='multiple'>";
        foreach ($spaces['items'] as $key => $value) {
        	if ($db_local->groupCheckIfSpaceMember($value['id'], $current_group, $botid)) {
        		echo "<option selected value='{$value['id']}'>{$value['title']}</option>";
        	}
        	else {
        		echo "<option value='{$value['id']}'>{$value['title']}</option>";
        	}
        }
            	// echo "<option hidden value='' selected></option>";
            	echo "</select>
                </div>
               </div>
            </div>
            </form>
         </div>
	    </div>
	</div>
</div>
      ";
            		         
   } else { echo "</div></div></div>"; }
 }
 
 if(!$page_group) echo "</div></div></div></div>";
 

   ?>