<?php 
//$subpage = (issetor($_GET['sub'])) ? $db_local->quote($_GET['sub']):header('Location: index.php?id=administration&sub=default');
$subpage = 'default';
$statusimage = "";
$down = "";
if (isset($_POST['domains'])) {
	foreach ($_POST['domains'] as $key => $value){
		$db_local->acceptedDomainDelete($value);
	}
}
if (isset($_POST['domain'])) {
	if (validateDomain($_POST['domain'])) {
		$db_local->acceptedDomainAdd($_POST);
	} else {
		redirect("index.php?id=administration&sub=default&feedback=warning-InvalidDomain#");
	}
}

$tooltips = array(
		'eventsender'=>tooltip('Event sender','It is recommended to add a dummy bot (with no webhooks) to use as a dedicated event notification sender.'),
		'eventemails'=>tooltip('Event receivers','When a user talks to a bot managed by WBM an event will be generated and sent via a bot to everyone in this list. This is useful for troubleshooting and providing help to users. The event will also contain details about the service at the time of the event and what the user typed.'),
		'eventemail'=>tooltip('Event receiver','Add a Webex Teams e-mail that will receive event notifications for WBM, the user does not have to be added as a user to WBM for this to work but it must exist as a user in Webex Teams.'),
		'alloweddomains'=>tooltip('Allowed domains','This is a list of domains that can be allowed communication by a bot. Note that you have to configure each individual bot (bot settings) with its own domain restriction, this just shows which domains that are available for restriction.'),
		'allowdomain'=>tooltip('Allow domain','Add selectable domains for communication restriction'),
		'blocked'=>tooltip('Blocked e-mails','WBM will completely ignore messages originating from users in this list (valid for any bot managed by this tool), but will still generate an event notification that the user has tried to communicate with a bot.'),
		'block'=>tooltip('Block e-mail','Type in an e-mail of the person you want to block'),
		'maintenanceresponse'=>tooltip('Maintenance response','If maintenance mode is enabled, the bot will reply with this message (only for "task" responses)'),
		'maintenancemode'=>tooltip('Maintenance mode','If you are doing an update on your task service or you have issues you can enable maintenance mode. The bot will reply with the bot maintenance response instead of creating a task.'),
		'systemresponse'=>tooltip('System message','If system message mode is enabled, the bot will include this message in every reply.'),
		'systemmode'=>tooltip('System message mode','A system message is applied to all bot responses. This is useful if you want to make a user aware of an ongoing issue or something else regardless of what command the user types to the bot.'),
		'taskmon'=>tooltip('Task service monitoring','If you are using the Task API, you can monitor if the communication channel is up, not needed if the Task API is not used'),
		'adserver'=>tooltip('Active Directory Server','Insert your companies Active Directory host server. Must be FQDN'),
		'adport'=>tooltip('Active Directory Port','Insert your AD servers port. Currently, only 389 (unsecure) is available'),
		'basedn'=>tooltip('Base DN','Insert the base DN you want searches to take place at. Anything outside of this will not show up. It&#39;s recommended to put the whole BaseDN'),
		'aduser'=>tooltip('AD Username','A service account is recommended'),
		'adpass'=>tooltip('AD Password','Insert the password that is used for the Service Account Username'),
		'admingroup'=>tooltip('AD Admin Group','With AD integration, setting up a group for login will allow only certain users to have the capability of logging in under the admin privileges. This is a mandatory field.'),
		'ProxyURL'=>tooltip('Proxy URL','In the case WBM is behind a proxy server. The Proxy URL information is needed. The proxy URL is required without http or https. Example: proxy.example.com.'),
		'ProxyPort'=>tooltip('Proxy Port','The Proxy is required for WBM. General Proxy Ports are 80 or 443.'),
		'ProxyUser'=>tooltip('Proxy Username','Some Proxy servers require credentials. Please enter the username in this field.'),
		'ProxyPass'=>tooltip('Proxy Password','Some Proxy servers require credentials. Please enter the password in this field. NOTE: passwords with colons are NOT recommended.'),
);

switch ($subpage) {
	case "default": 	
	//Flip for maintenance mode
	if (isset($_POST['maintenance_mode'])) {
		$db_local->adminSetMaintenance($_POST['maintenance_mode']);
		redirect('index.php?id=administration&sub=default');
	}	
	if (isset($_POST['maintenance_message'])) {
		$db_local->adminSetMaintenanceMessage($_POST['maintenance_message']);
	}
	if (isset($_POST['warning_mode'])) {
		$db_local->adminSetWarning($_POST['warning_mode']);
	}
	if (isset($_POST['warning_message'])) {
		$db_local->adminSetWarningMessage($_POST['warning_message']);
	}
	if (isset($_POST['task_monitor'])) {
		echo $db_local->adminSetTaskMonitor($_POST['task_monitor']);
		redirect('index.php?id=administration&sub=default');
	}	
	if (isset($_POST['botid'])) {
		$db_local->query($db_local->constructUpdateValues("log_bot", $_POST, "WHERE id = '0'"));
	}
	if (isset($_POST['email'])) {
		$db_local->adminBlockContact($db_local->quote($_POST['email']));
	}
	if (isset($_POST['logemail'])) {
		$db_local->adminAddLogUser($db_local->quote($_POST['logemail']));
	}
	if (isset($_POST['blocked'])) {
		foreach ($_POST['blocked'] as $key => $value){
			$db_local->query("DELETE FROM blocked_contacts WHERE id = '{$value}'");
		}
	}
	if (isset($_POST['logusers'])) {
		foreach ($_POST['logusers'] as $key => $value){
			$db_local->query("DELETE FROM log_users WHERE id = '{$value}'");
		}
	}
	//Check if maintenance mode is enabled and sets variables
	$maintenance_activator = ($db_local->adminCheckIfMaintenance()) ? "Disable":"Enable";
	$warning_activator = ($db_local->adminCheckIfWarning()) ? "Disable":"Enable";
	$task_activator = ($db_local->adminCheckIfTaskMonitor()) ? "Disable":"Enable";
	
	if ($db_local->adminCheckIfTaskMonitor()) {
		//Checks the service status, if up show a green light else a red light
		$statusimage = onoff(!$db_local->adminCheckServiceStatus(),20,20);
		$down = (!$db_local->adminCheckServiceStatus()) ? "All good!":$db_local->adminCheckServiceStatus("report");
	} 						

	//Title
	$titlepage = "Administration";
	break;	
case "logs":
	$botfilter="";
	$limit = $_GET['maxresults'];
	if (is_numeric($limit)) {
			if (isset($_GET['botfilter'])) {
				$botfilter = $_GET['botfilter'];		
			}
			$logs = $generate->logsPrint($botfilter, $limit);
			$titlepage = "LOGS";
	}
	else{
		$logs = "Issue with the maxresults";	
	}
	break;
case "spaceoptions":
	$titlepage = "Space options";
}	
if (!$statusimage) $statusimage = "<b>Disabled</b>";
?>
<!-- Content Header (Page header) -->
<div class='content-header'>
	<div class='container-fluid'>
		<div class='row mb-2'>
			<div class='col-sm-6'>
				<h1 class='m-0 text-dark'><?php echo $titlepage; ?></h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class='content'>
	<div class='container-fluid'>
			<!-- Info boxes -->
		<div class="row">
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-paper-plane"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Messages Sent</span>
						<span class="info-box-number"><?php echo count($db_local->messagesentcount());?></span>
              		</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box">
					<span class="info-box-icon bg-info elevation-1"><i class="nav-icon fab fa-android"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Bots</span>
						<span class="info-box-number"><?php echo count($db_local->botFetchBots());?></span>
              		</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-address-book"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Users</span>
						<span class="info-box-number"><?php echo count($db_local->contactFetchContacts());?></span>
              		</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="info-box">
					<span class="info-box-icon bg-info elevation-1"><i class="fas fa-crown"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Site Admins</span>
						<span class="info-box-number"><?php echo count($db_local->select("SELECT * FROM contacts WHERE type=1"));?></span>
              		</div>
				</div>
			</div>

		</div>

        <div class="row">
			<div class="col-lg-6">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Task service settings</h3>
					</div>
					<div class="card-body">
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['taskmon'];?> Task service monitoring
							</div>
							<div class="col-md-6 mb-1">
								<?php echo $statusimage; ?>&nbsp<?php echo $down; ?>
							</div>
							<div class="col-md-3 mb-1">
								<form method='post' action='#taskservice' id='taskservice' enctype='multipart/form-data'>
									<input id='button' type='submit' class="btn btn-sm btn-primary btn-block" value='<?php echo $task_activator; ?>'>
									<input type='hidden' value='<?php echo !$db_local->adminCheckIfTaskMonitor(); ?>' name='task_monitor'>
								</form>
							</div>
						</div>
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['maintenancemode'];?> Maintenance mode 
								<?php $main_message = $db_local->adminGetMaintenanceMessage(); ?>
							</div>
							<div class="col-md-6 mb-1">
								<?php echo ($db_local->adminCheckIfMaintenance()) ? "<b>Enabled</b>":"<b>Disabled</b>"; ?>
							</div>
							<div class="col-md-3 mb-1">
								<form method='post' action='#maintenance' id='maintenance' enctype='multipart/form-data'>
									<input id='button' type='submit' class="btn btn-sm btn-primary btn-block" value='<?php echo $maintenance_activator; ?>'>
									<input type='hidden' value='<?php echo !$db_local->adminCheckIfMaintenance(); ?>' name='maintenance_mode'>
								</form>
							</div>
						</div>
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['maintenanceresponse'];?> Maintenance mode bot response
							</div>
							<div class="col-md-6 mb-1">
								<form method='post' id='maintenancemsg' action='#maintenancemsg' enctype='multipart/form-data'>
								<input type='text' placeholder='Maintenance message' name='maintenance_message' class="form-control form-control-sm" value='<?php echo $main_message?>'>
							</div>
							<div class="col-md-3 mb-1">
								<input id='button' type='submit' class="btn btn-sm btn-primary btn-block" value='Set'></td></form>
							</div>
						</div>
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['systemmode'];?> System message mode 
								<?php $main_message = $db_local->adminGetWarningMessage(); ?>
							</div>
							<div class="col-md-6 mb-1">
								<?php echo ($db_local->adminCheckIfWarning()) ? "<b>Enabled</b>":"<b>Disabled</b>"; ?>
							</div>
							<div class="col-md-3 mb-1">
								<form method='post' action='#warning' id='warning' enctype='multipart/form-data'>
									<input id='button' type='submit' class="btn btn-sm btn-primary btn-block" value='<?php echo $warning_activator; ?>'>
									<input type='hidden' value='<?php echo !$db_local->adminCheckIfWarning(); ?>' name='warning_mode'>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['systemresponse'];?> System message 
							</div>
							<div class="col-md-6 mb-1">
								<form method='post' id='warningmsg' action='#warningmsg' enctype='multipart/form-data'>
								<input type='text' placeholder='Warning message' name='warning_message' class="form-control form-control-sm" value='<?php echo $main_message?>'>
							</div>
							<div class="col-md-3 mb-3">
								<input id='button' type='submit' class="btn btn-sm btn-primary btn-block" value='Set'></td></form>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Restrict bot communication</h3>
					</div>
					<div class="card-body">
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['block'];?> Block e-mail
							</div>
							<div class="col-md-6 mb-1">
								<form name='usersBlocked' method='post' id='block' action='#block' enctype='multipart/form-data'>
							<input type='text' placeholder='Email' required name='email' class="form-control form-control-sm">
							</div>
							<div class="col-md-3 mb-1">
								<input type='submit' class='btn btn-sm btn-primary btn-block' value='Add'>
								</form>
							</div>
						</div>
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['blocked'];?> Blocked e-mails
							</div>
							<div class="col-md-6 mb-1">
								<form method='post' action='#blocked' id='blocked' enctype='multipart/form-data'>
								<?php 
									$blocked_contacts = $db_local->adminGetBlockedContacts();
									$number_of_blocked_contacts = count($blocked_contacts);
									if (!$blocked_contacts) echo "<b>No blocked e-mails</b>";
									foreach ($blocked_contacts as $key => $value){
										echo "<input type='checkbox' name='blocked[]' value='{$value['id']}'> {$value['email']}<br>";
									}
									
								?>
							</div>
							<div class="col-md-3 mb-1">
								<?php
								if ($number_of_blocked_contacts>0) echo "<td><input type='submit' class='btn btn-sm btn-danger btn-block' value='Delete selected'>";		
								?>
								</form>
							</div>
						</div>
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['allowdomain'];?> Allow domain
							</div>
							<div class="col-md-6 mb-1">
								<form method='post' id='domains' action='#domains' enctype='multipart/form-data'>
									<input type='text' placeholder='example.com' required name='domain' class="form-control form-control-sm">
							</div>
							<div class="col-md-3 mb-1">
									<input class='btn btn-sm btn-primary btn-block' type='submit' value='Add'>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['alloweddomains'];?> Allowed domains
							</div>
							<div class="col-md-6 mb-1">
							<form method='post' action='#domainsadded' id='domainsadded' enctype='multipart/form-data'>
								<select multiple class="custom-select" name="domains[]">
								<?php 
									$domain_data = $db_local->acceptedDomainFetch();
									$number_of_domains = count($domain_data);
									foreach ($domain_data as $key => $value) {
										//echo "<input type='checkbox' name='domains[]' value='{$value['id']}'> {$value['domain']}<br>";
										echo "<option value='{$value['id']}'>{$value['domain']}</option>";
									}
									
								?>
								</select>
							</div>
							<div class="col-md-3 mb-1">
							<?php
								if ($number_of_domains) echo "<td><input type='submit' class='btn btn-sm btn-danger btn-block' value='Delete selected'>";
									else echo "<b>No domains added</b>";
							?>
							</form>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Proxy Settings</h3>
					</div>
					<div class="card-body">
						<?php
							
							$configfile  = readconfigfile();

							$proxyURL = explode(":", $configfile['proxyurl']);
							$proxycreds = explode(":", $configfile['proxyuserpass']);
							if(!empty($proxyURL[0])) { $proxyurl = $proxycreds[0]; } else { $proxyurl = ''; }
							if(!empty($proxyURL[1])) { $proxyport = $proxycreds[1]; } else { $proxyport = ''; }

							if(!empty($proxycreds[0])) { $proxyuser = $proxycreds[0]; } else { $proxyuser = ''; }
							if(!empty($proxycreds[1])) { $proxypass = $proxycreds[1]; } else { $proxypass = ''; }
						?>
						<form name="proxyinfo" method="post" action="" enctype="multipart/form-data">
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['ProxyURL'];?> Proxy Server URL: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='ProxyURL' placeholder="proxy.example.com" class="form-control form-control-sm" value="<?php echo $proxyurl; ?>">
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['ProxyPort'];?> Proxy Server Port: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='ProxyPort' placeholder="80" class="form-control form-control-sm" value="<?php echo $proxyport; ?>">
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['ProxyUser'];?> Proxy Username: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='ProxyUser' placeholder="Service Account Username" class="form-control form-control-sm"  value="<?php echo $proxyuser; ?>">
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['ProxyPass'];?> Proxy Password: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='password' name='ProxyPass' placeholder="Service Account Password" class="form-control form-control-sm"  value="<?php echo $proxypass; ?>">
								</div>
								<div class="col-md-3 mb-1">
									<input type='submit' name="ProxySave" class='btn btn-sm btn-primary btn-block' value='Save and Test Connection'>
								</div>
							</div>
						</form>
						<?php

							if(isset($_POST['ProxySave']))
							{

								if(!empty($_POST['ProxyURL']) && !empty($_POST['ProxyPort']))
								{
									$proxyurl = $_POST['ProxyURL'] . ":" . $_POST['ProxyPort'];
								}
								else
								{
									$proxyurl = '';
								}
								if(!empty($_POST['ProxyUser']) && !empty($_POST['ProxyPass']))
								{
									$proxycreds = $_POST['ProxyUser'] . ":" . $_POST['ProxyPass'];
								}
								else
								{
									$proxycreds = '';
								}

								$checkconnection = checkinternet($proxyurl, $proxycreds);

								if($checkconnection == 200)
								{
									$config  = readconfigfile();
									$configwrite = writeconfigfile($config['dbhost'], $config['dbname'], $config['username'], $config['password'], $config['base_dir'], $proxyurl, $proxycreds, $config['ADserver'], $config['ADport'], $config['BaseDN'], $config['AdminGroup'], $config['ADuser'], $config['ADpass']);

									if($configwrite == 1)
									{
										echo alerts('success', 'Internet Access Validated', 'Internet Access has been validated and the proxy settings are now saved in the config_ini.php file');
									}
									else
									{
										echo alerts('warning', 'ALERT!', 'WBM can reach the internet, but was not able to save the proxy settings in the config_ini.php file'); 
									}
								}
								else
								{
									echo alerts('error', 'Error!', 'WBM can not access Webex.com - Please check proxy settings');
								}
							}

						?>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Bot event notifications</h3>
					</div>
					<div class="card-body">
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['eventemail'];?> Add event-notification receivers <br> (Webex Teams e-mails)
							</div>
							<div class="col-md-6 mb-1">
								<form name='usersLogging' method='post' id='events' action='#events' enctype='multipart/form-data'>
									<input type='text' class="form-control form-control-sm" placeholder='Email' required name='logemail'>
							</div>
							<div class="col-md-3 mb-1">
								<input class='btn btn-sm btn-primary btn-block' type='submit' class='spacing-left' value='Add'>
								</form>
							</div>
						</div>
						<div class="row mb-2 mb-md-1">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['eventemails'];?> Event-notification receivers
							</div>
							<div class="col-md-6 mb-1">
								<form method='post' action='#eventreceivers' id='eventreceivers' enctype='multipart/form-data'>
									 <select multiple class="custom-select" name='logusers[]'>
							<?php 
							$log_users = $db_local->adminGetLogUsers();
							if (!$log_users) echo "<b>No event receivers</b>";
							$number_of_log_users = count($log_users);
							foreach ($log_users as $key => $value){
								//echo "<input type='checkbox' name='logusers[]' value='{$value['id']}'> {$value['email']}<br>";
								echo "<option value='{$value['id']}'>{$value['email']}</option>";
							}
							?>
									</select>
							</div>
							<div class="col-md-3 mb-1">
								<?php
								if ($number_of_log_users>0) echo "<td><input type='submit' class='btn btn-sm btn-danger btn-block' value='Delete selected'>";		
								?>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 mb-1">
								<?php echo $tooltips['eventsender'];?> Event notification sender
							</div>
							<div class="col-md-6 mb-1">
								<form method='post' action='#eventsender' id='eventsender' enctype='multipart/form-data'>
							<?php 
							$selectedbot = $db_local->adminGetLogBot();
							echo (empty($selectedbot)) ? $generate->botGenDropdown("botid"):$generate->botGenDropdown("botid",$selectedbot);
							?>
							
							</div>
							<div class="col-md-3 mb-1">
								<input type='submit' class='btn btn-sm btn-primary btn-block' value='Set'>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Active Directory Settings</h3>
					</div>
					<div class="card-body">
						<?php
							$configfile = "../config_ini.php";
							if(!empty($config['ADserver'])){ $configADserver = $config['ADserver']; } else { $configADserver = '';}
							if(!empty($config['ADport'])){ $configADport = $config['ADport']; } else { $configADport = '';}
							if(!empty($config['BaseDN'])){ $configBaseDN = $config['BaseDN']; } else { $configBaseDN = '';}
							if(!empty($config['AdminGroup'])){ $configAdminGroup = $config['AdminGroup']; } else { $configAdminGroup = '';}
							if(!empty($config['ADuser'])){ $configADuser = $config['ADuser']; } else { $configADuser = '';}
							if(!empty($config['ADpass'])){ $configADpass = $config['ADpass']; } else { $configADpass = '';}
						?>
						<form name="ADsettings" method="post" action="" enctype="multipart/form-data">
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['adserver'];?> Active Directory Server: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='ADserver' placeholder="ad.example.com" class="form-control form-control-sm" value="<?php echo $configADserver; ?>" required>
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['adport'];?> Active Directory Port: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='ADport' placeholder="389" class="form-control form-control-sm" value="<?php echo $configADport; ?>" required>
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['basedn'];?> Base DN: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='BaseDN' placeholder="DC=example,DC=com" class="form-control form-control-sm"  value="<?php echo $configBaseDN; ?>" required>
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['admingroup'];?> Admin Group: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='AdminGroup' placeholder="groupname" class="form-control form-control-sm"  value="<?php echo $configAdminGroup; ?>" required>
								</div>
							</div>
							<div class="row mb-2 mb-md-1">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['aduser'];?> Username: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='text' name='ADuser' placeholder="user@example.com" class="form-control form-control-sm" value="<?php echo $configADuser; ?>" required>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 mb-1">
									<?php echo $tooltips['adpass'];?> Password: 
								</div>
								<div class="col-md-6 mb-1">
									<input type='password' name='ADpass' placeholder="****" class="form-control form-control-sm" required>
								</div>
								<div class="col-md-3 mb-1">
									<input type='submit' name="ADsave" class='btn btn-sm btn-primary btn-block' value='Save and Test Connection'>
								</div>
							</div>
						</form>
						<?php

							if(isset($_POST['ADsave']))
							{

								$connection = $ldap->connect($_POST['ADserver'],$_POST['ADport']);
								$bind = $ldap->bind($connection,$_POST['ADuser'],$_POST['ADpass']);
								if($bind)
								{
									$config  = readconfigfile();
									$configwrite = writeconfigfile($config['dbhost'], $config['dbname'], $config['username'], $config['password'], $config['base_dir'], $config['proxyurl'], $config['proxyuserpass'], $_POST['ADserver'], $_POST['ADport'], $_POST['BaseDN'], $_POST['AdminGroup'], $_POST['ADuser'], $_POST['ADpass']);

									if($configwrite == 1)
									{
										echo alerts('success', 'Success!', 'Connected Successfully to AD Server');
									}
									else
									{
										echo alerts('warning', 'ALERT!', 'Was able to connect succcessfully to AD server but was not able to write the settings to config_ini.php file'); 
									}
								}
								else
								{
									echo alerts('error', 'Error!', 'Could not connect to AD server');
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
