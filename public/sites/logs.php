<?php if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); ?>
<!-- Content Header (Page header) -->
<div class='content-header'>
	<div class='container-fluid'>
		<div class='row mb-2'>
			<div class='col-sm-6'>
				<h1 class='m-0 text-dark'>Logs</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class='content'>
	<div class='container-fluid'>

		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Logs</h3>
					</div>
					<div class="card-body">
						<table width='100%' id='logs' class='table table-bordered table-striped'>
							<thead>
								<tr>
									<th>Date</th>
									<th>User</th>
									<th>Bot</th>
									<th>Payload</th>
								</tr>
							</thead>
							<tfoot>
					            <tr>
					            	<th>Date</th>
					                <th>User</th>
									<th>Bot</th>
									<th>Payload</th>
					            </tr>
					        </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>