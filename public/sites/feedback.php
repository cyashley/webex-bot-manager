<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$v_y = $c_y = $ec_y = $ed_y = $ev_y = "checked";
$v_n = $c_n = $ec_n = $ed_n = $ev_n = "";
$topic = issetor($_GET['topic']);
$botid = issetor($_GET['botid']);
$entryid = issetor($_GET['entry']);

$note = (!$botid) ? " - Select a bot":"";

//PAGE OPERATIONS
if (issetor($_POST['topic_add'])) {
    $db_local->feedbackTopicAdd($_POST);
}
if (issetor($_POST['topic_update'])) {
	$db_local->feedbackTopicUpdate($_POST, $_POST['topicid']);
}
if (issetor($_POST['topic_delete'])) {
	$db_local->feedbackTopicDelete($_POST['topicid']);
	redirect("index.php?id=feedback&botid={$botid}");
}
if (issetor($_POST['entry_add'])) {
	$db_local->feedbackEntryAdd($_POST);
}
if (issetor($_POST['entry_update'])) {
	$entryid = issetor($_POST['entryid']);
	$db_local->feedbackEntryUpdate($_POST, $entryid);
}
if (issetor($_POST['entry_delete'])) {
	$db_local->feedbackEntryDelete($_POST['entryid']);
	if (issetor($_GET['entry'])) unset($_GET['entry']);
	redirect(formUrl($_GET)."#entry");
}
if (issetor($_POST['entries_delete'])) {
	$entries_d = issetor($_POST['entries']);
	foreach ($entries_d as $key => $value) {
		$db_local->feedbackEntryDelete($value);
	}
	redirect(formUrl($_GET));
}
if (issetor($_POST['comment_add'])) {
	$db_local->feedbackEntryComment(issetor($_POST['entryid']), issetor($_POST['comment']), $_SESSION['email']);
}
if (issetor($_POST['vote_add'])) {
	$db_local->feedbackEntryVote(issetor($_SESSION['email']), issetor($_POST['entryid']));
}
if (issetor($_POST['vote_revoke'])) {
	$db_local->feedbackEntryVoteDelete("",issetor($_SESSION['email']), issetor($_POST['entryid']));
}
if (issetor($_POST['votes_delete'])) {
	$entries_d = issetor($_POST['votes_selected']);
	foreach ($entries_d as $key => $value) {
		$db_local->feedbackEntryVoteDelete($value);
	}
	redirect(formUrl($_GET));
}
if (issetor($_POST['comments_delete'])) {
	$entries_d = issetor($_POST['comments_selected']);
	foreach ($entries_d as $key => $value) {
		$db_local->feedbackEntryCommentDelete($value);
	}
	redirect(formUrl($_GET));
}
if (issetor($_POST['cancel_topic'])) {
	redirect("index.php?id=feedback&botid={$botid}");
}
if (issetor($_POST['cancel_entry'])) {
	redirect("index.php?id=feedback&botid={$botid}&topic=$topic#entry");
}

if ($topic) {
		$actionname = "topic_update"; 
		$submitname = "Update topic";
		$edit = "Update topic (topicId: <b>$topic</b>)";
		$topicinfo = $db_local->feedbackFetchTopics($botid, $db_local->quote($topic));
		$form = (issetor($_GET['topic'])) ? "" : "&topic={$topicinfo[0]['id']}";
		list($v_y, $v_n) = ($topicinfo[0]['votes_allowed']) ? array("checked", ""):array("","checked");
		list($c_y, $c_n) = ($topicinfo[0]['comments_allowed']) ? array("checked", ""):array("","checked");
		list($ec_y, $ec_n) = ($topicinfo[0]['entry_create_allowed']) ? array("checked", ""):array("","checked");
		list($ev_y, $ev_n) = ($topicinfo[0]['entry_view_allowed']) ? array("checked", ""):array("","checked");
		list($ed_y, $ed_n) = ($topicinfo[0]['entry_delete_allowed']) ? array("checked", ""):array("","checked");
		$entry_title = "Create new entry in <b>{$topicinfo[0]['title']}</b> (topicId: {$topicinfo[0]['id']})";
		$entry_e = "Create new entry";
		$entry_action = "entry_add";
		$created_by = "<input type='hidden' value='{$_SESSION['email']}' name='created_by'>";
		if ($entryid) {
			$entryinfo = $db_local->feedbackFetchEntry($entryid);
			$entry_e = "Update";
			$entry_title = "Modifying entry (entryId: <b>{$entryinfo[0]['id']}</b>) in (topicId: <b>{$topicinfo[0]['id']}</b>)";
			$entry_action = "entry_update";
			$created_by = "";
			
		}
}
else {
		$actionname = "topic_add";
		$submitname = "Create topic";
		$edit = "Create topic";
		$groupinfo = array("0"=>array("id"=>"", "groupname"=>""));
		$form = "";
}

//Page tooltips
$tooltips = array(
        'feedback'=>tooltip('Feedback topics','Feedback is a bot feature that provides a generic way of gathering feedback from users. Users can via the bot, add feedback entires and others can vote and comment on the entries. By modifying the allow list you can turn off voting / commenting etc, and also create private topics that only users part of a specific access group can see and interact with.'),
        'selectBot'=>tooltip('Select a bot','Created topics will belong to the bot you select.'),
		'topics'=>tooltip('Topics','Here are all the topics that has been created for this bot. Only bot-admins or site admins can create topics.'),
		'topicTitle'=>tooltip('Topic title','The name of the topic, it should be something short and relate to a specific topic, for example: "Feature requests", "Trip suggestions" etc..'),
		'topicSettings'=>tooltip('Topic settings','Allow/Disallow the below actions within a specific topic. By disallowing commenting for example, users can create entries to a topic but users cannot add comments to the entries in that topic.'),
		'topicAccess'=>tooltip('Topic access group','Setting an access group on a topic will only allow users part of the access group to view/create entries, comment, vote etc.. in that topic. Private topics. '),
		'entries'=>tooltip('Entries','Entries are sub-data to a topic, it can either be open so users can add entires or locked so that users can only vote on pre-defined entries in a topic for example. Users may only delete their own entries.'),
		'comment'=>tooltip('Commenting','Users may comment on entries in a topic, this could also work as a voting system to force users to write feedback on a certain entry instead of just a +1. Users may only delete their own comments'),
		'vote'=>tooltip('Voting','Voting is a simple +1 for each people that has voted on a entry. If the topic is "What do you think about the seminar?" with entries "A: Good", "B: OK", "C: Bad", a user can vote for the entries. A user can vote for more than one entry, but only place one vote per entry. Users may also revoke their vote on an entry.')
);

$topic_id = ($topic) ? "<input type='hidden' value='{$topic}' name='topicid'>":"";
$group_options = $generate->groupOptions(issetor($topicinfo[0]['accessgroup']));
?>
<script language="JavaScript">
function toggleMember(source, name) {
	  checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggleNonMember(source, name) {
	checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	} 
</script>
<!-- Content Header (Page header) -->
<div class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1 class="m-0 text-dark">Feedback</h1>
         </div><!-- /.col -->
      </div><!-- /.row -->
   </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
   <div class="container-fluid">
<div class='row'>
         <div class='col-lg-6'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'><?php echo $tooltips['feedback']; ?> <?php echo $edit; ?></h3>
               </div>
               <div class='card-body'>
					<form name='groups' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>		
					<div class='form-group'>
						<label for='BotSelection'><?php echo $tooltips['selectBot']; ?> Select A Bot</label>
						<br>
						<?php $generate->botGenSelector('feedback', issetor($botid)); ?>
					</div>
					<?php 
					if($botid)
					{
					echo "
					<div class='form-group'>
						<label for='BotSelection'>{$tooltips['topicTitle']} Topic Title</label>
						<br>
						<input type='text' class='form-control' placeholder='Topic title' required name='title' value='".issetor($topicinfo[0]['title'])."'>
						<input type='hidden' value='{$botid}' name='botid'>
					</div>
					<div class='form-group'>
						<label for='BotSelection'>{$tooltips['topicSettings']} Topic user settings (bot commands)</label>
						<br>
						<table class='table'>
							<tr>
								<th>Entry votes:</th>
								<td><label>On</label> <input type='radio' $v_y name='votes_allowed' value='1'>
								<label>Off</label> <input type='radio' $v_n name='votes_allowed' value='0'> <i>Allow voting</i></td>
							</tr>
							<tr>
								<th>Entry comments:</th>
								<td><label>On</label> <input type='radio' $c_y name='comments_allowed' value='1'>
								<label>Off</label> <input type='radio' $c_n name='comments_allowed' value='0'> <i>Allow commenting</i></td>
							</tr>
							<tr>
								<th>Entry create:</th>
								<td><label>On</label> <input type='radio' $ec_y name='entry_create_allowed' value='1'>
								<label>Off</label> <input type='radio' $ec_n name='entry_create_allowed' value='0'> <i>Allow creating entries</i></td>
							</tr>
							<tr>
								<th>Entry view:</th>
								<td><label>On</label> <input type='radio' $ev_y name='entry_view_allowed' value='1'>
								<label>Off</label> <input type='radio' $ev_n name='entry_view_allowed' value='0'> <i>Allow view of entry details</i></td>
							</tr>
							<tr>
								<th>Entry delete:</th>
								<td><label>On</label> <input type='radio' $ed_y name='entry_delete_allowed' value='1'>
								<label>Off</label> <input type='radio' $ed_n name='entry_delete_allowed' value='0'> <i>Allow users delete their own entries</i></td>
							</tr>
							<tr>
								<th>{$tooltips['topicAccess']} Topic accessgroup:</th>
								<td><select name='accessgroup' class='form-control'>
										<option value=''>Select access group</option>
											$group_options
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class='box-footer'>
						<input type='submit' name='$actionname' class='btn btn-primary' value='{$submitname}' />";
						if ($topic) 
						{
							echo "<input type='hidden' name='topicid' value='$topic'>";
						    echo "<input type='submit' class='btn btn-danger float-right' style='margin-left: 2px' $link_confirm name='topic_delete' value='Delete topic'>";
							echo "<input type='submit' class='btn btn-danger float-right' name='cancel_topic' value='Cancel'>";
						}
					echo "
					</div>
					";
					}?>
				</form>
			</div>
		</div>
	</div>	
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'><?php echo $tooltips['topics']; ?> Topics</h3>
			</div>
			<div class='card-body'>
				<?php
				if($botid)
					{
				?>
				<table width='100%' id='topics' class='table table-bordered table-striped'>
					<thead>
						<tr>
							<th width='10%'>ID</th>
							<th width='60%'>Topic</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<?php
if ($topic) {
	$entries = $generate->feedbackGenTopicEntries($botid, $topic);
	?>
<div class='row'>
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'><?php echo $tooltips['entries']; ?> <?php echo $entry_title; ?></h3>
			</div>
			<div class='card-body'>
				<form method='post' id='entry' action='<?php echo formUrl($_GET); ?>#entry' enctype='multipart/form-data'>	
						
					<textarea name='description' rows='10' class='form-control'></textarea>
					<input type='hidden' value='<?php echo $topic; ?>' name='topic_id'>
					<input type='hidden' value='<?php echo $entryid; ?>' name='entryid'>
					<?php echo $created_by; ?>
					<br>
					<input type='submit' name='<?php echo $entry_action; ?>' class='btn btn-primary' value='<?php echo $entry_e; ?>'/>
					<?php
					if ($entryid) {
						echo "<input type='submit' style='margin-left: 2px' class='btn btn-danger float-right' $link_confirm name='entry_delete' value='Delete entry'>";
						echo "<input type='submit' class='btn btn-danger float-right' name='cancel_entry' value='Cancel'>";
					}
					?>
				</form>
			</div>
		</div>
	</div>
	<div class='col-lg-6'>
		<form method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>	
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>Entries in <b><?php echo $topicinfo[0]['title']; ?></b> (<?php echo $topicinfo[0]['id']; ?>)</h3>
				<div class='card-tools'>
					<input type='submit' class='btn btn-sm btn-danger' <?php echo $link_confirm; ?> name='entries_delete' value='Delete selected entries'>
				</div>
			</div>
			<div class='card-body'>
				<?php
					echo $entries;
				?>
			</table>
			</div>
		</div>
		</form>
	</div>
	</div>
	<?php
		}
	?>

<?php
if ($entryid) {
	$votes = $generate->feedbackGenEntryVotes($entryid);
	$comments = $generate->feedbackGenEntryComments($entryid);
	$n_c = $db_local->feedbackFetchEntryComments($entryid);
	$n_v = $db_local->feedbackFetchEntryVotes($entryid);
	$num_votes = count($n_v);
	$num_comments = count($n_c);
	$votebutton = "Vote";
	$voteclass = "btn btn-primary btn-block";
	$votename = "vote_add";
	$email = $_SESSION['email'];
	if ($db_local->feedbackFetchEntryVoteExists($entryid, $email)) {
		$votebutton = "You have already voted (Click here to revoke your vote)";
		$voteclass = "btn btn-danger btn-block";
		$votename = "vote_revoke";
	}
	
echo "
<div class='row'>
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>{$tooltips['comment']} Comments in entryId $entryid: <b>$num_comments</b></h3>
			</div>
			<div class='card-body'>
			<form method='post' id='comment' action='".formUrl($_GET)."#comment' enctype='multipart/form-data'>
				<div class='input-group mb-3'>
					<input type='text' name='comment' width='100%' class='form-control' placeholder='New comment..'>
					<div class='input-group-append'>
						<span class='input-group-text'><i class='fa fa-comment'></i></span>
						<input type='hidden' name='search' value='Search'>
					</div>
					<input type='hidden' name='entryid' value='".issetor($_GET['entry'])."'>
					<input type='hidden' name='comment_add' value='Add comment' class='btn btn-primary hide'>
				</div></form>
					$comments
			</div>
		</div> 
	</div>
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>{$tooltips['vote']} Votes for (entryId: <b>$entryid</b>): <b>$num_votes</b></h3>
			</div>
			<div class='card-body'>
				<form method='post' id='vote' action='".formUrl($_GET)."#vote' enctype='multipart/form-data'>
				<div class='form-group'>
				<input type='hidden' name='entryid' value='".issetor($_GET['entry'])."'>
				<input type='submit' name='$votename' value='$votebutton' class='$voteclass'>
				</div>
						$votes
				</form>
			</div>
			
		</div> 
	</div>
</div>
			 	
";
}

?>

</div>
</div>