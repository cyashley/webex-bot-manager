<?php
//Step 1 -> Bot accesstoken
//Check if a primary bot exists with valid accesstoken!
function stepDie($step) {
	return die("<br>Step: $step must be completed before you can proceed...");
}
if (isset($_POST['accesstoken'])) {
	$token = trim($_POST['accesstoken']);
	$botAdd = $spark->wizardAddMainBot($token);
	$bot = $db_local->botGetMainInfo();
	if ($botid = $bot['id']) {
		if (isset($_POST['webhook_direct'])) {
			$spark->webhookCreateQuick($botid, 'direct');
		}
		if (isset($_POST['webhook_group'])) {
			$spark->webhookCreateQuick($botid, 'group');
		}
		echo msgList("success-BotAdd");
		echo feedbackMsg("Primary bot added!", $botAdd, "info");
	}
	else echo msgList("alert-BotAddAccess");
}
$primaryFound = $tokenExists = $tokenValid = false;
$primaryBotInfo = $db_local->botGetMainInfo();
if (empty($primaryBotInfo)) {
	$primaryFound = false;
}
else {
	$primaryFound = true;
	$botid = $primaryBotInfo['id'];
	if (empty($primaryBotInfo['access'])) {
		$tokenExists = false;
	}
	else {
		$tokenExists = true;
		$me = $spark->peopleGetMe($botid);
		$tokenValid = ($me['id'] == $botid) ? true:false;
	}
}
if(isset($_POST['useremail'])) {
	if (validateEmail($useremail = $db_local->quote($_POST['useremail']))) {
		$payload = array("sender"=>$botid, "recepientType"=>"email", "recepientValue"=>$useremail);
		$userDetails = $spark->peopleGet($payload);
		if (count($userDetails['items']) == 1) {
			$db_local->contactAdd($userDetails, $botid);
			if (!empty($userdata = $db_local->contactFetchContacts($userDetails['items'][0]['id']))) {
				$userid = $userdata[0]['id'];
				$password = generatePassword();
				$token = hash('md5', generatePassword());
				$hashpw = password_hash($password, PASSWORD_DEFAULT);
				$host = getHostUrl();
				$text = "Here is your new login passord to the [Webex Teams - Bot Manager site]($host), please change the password in the user settings when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
				list($foo, $domain) = explode('@', $userdata[0]['emails']);
				$domainid = $db_local->acceptedDomainAdd(array('domain'=>$domain));
				if ($domainid){
					$db_local->botAddAllowedDomain($domainid, $botid);
				}
				$features = $db_local->adminGetFeatures();
				$feature_list = blockquote("Here are my currently enabled features, type the keyword to get a help list", "success");
				if (count($features)) {
					foreach ($features as $key => $value) {
						$db_local->adminSetFeature($value['id'], $botid);
						$feature_list .= "\n- {$value['keyword']}";
					}
				}
				$db_local->userUpdateSettings('update',$hashpw,$userid,'1');
				$db_local->userUpdateSettings('token',$token,$userid,'1');
				$db_local->botSetDefaultResponse("Hi, nice to meet you! This is my default response. You can change it and create more responses in the bot settings. You can also enable features in the bot responses tab. Have fun :)" ,$botid);
				if ($spark->messageSend($spark->messageBlob($text, $userid, $botid))) {
					echo msgList("success-Query");
					$spark->messageSend($spark->messageBlob($feature_list, $userid, $botid));
					sleep(2);
					$spark->messageSend($spark->messageBlob("You can login and start to administer the responses. Remember, you might want to disable some of the commands or put them behind an accessgroup to avoid misuse.", $userid, $botid));
				}
				else {
					echo msgList("success-Generic");
				}
			}
			else {
				echo msgList("warning-AddFailed");
			}
		}
		else {
			echo msgList("warning-UserNotFound");
		}
	}
	else {
		echo msgList("warning-Email");
	}
}
$loginUser = $db_local->adminCheckSiteAdminExists();
if (!empty($feedback)) {
	echo msgList($headline,$msg,$style);
}
echo "
	  <table class='rounded'>
	<tr>
		<td><b>STATUS CHECK:</b>
	<tr>
		<td>STEP 1:<hr>
	<tr><td>
	";

echo "		<table>
		<tr>
			<td>
				PRIMARY BOT FOUND:
				<td>".onoff($primaryFound);
echo "	<tr>
			<td>
				ACCESS TOKEN EXISTS:
				<td>".onoff($tokenExists);
echo "	<tr>
			<td>
				ACCESS TOKEN VALID:
				<td>".onoff($tokenValid);
echo "<tr><td>STEP 2: <hr>";
echo "<tr><td>SITE ADMIN FOUND:<td>".onoff($loginUser);
echo "</table></table>
		
	  <table class='rounded'>
	<tr>
		<td><b>SETUP WIZARD</b>
	<tr>
		<td>STEP 1:<hr>
	<tr>
		<td>";
if (!$primaryFound or !$tokenExists or !$tokenValid) {
	echo "<div id='input'>
				<form method='post' action='index.php?id=setup' enctype='multipart/form-data'>
					<table width='100%'>
						<tr>
							<td colspan='2'>
								<input type='text' style='width: 70%' value='' placeholder='Enter a valid bot accesstoken' name='accesstoken'> <input type='submit' style='width: 20%' value='Add primary bot'>
						<tr>
							<td>
								<input type='checkbox' checked name='webhook_direct' value='1'> <td> Setup direct conversation webhook
						<tr>
							<td>
								<input type='checkbox' checked name='webhook_group' value='1'> <td> Setup group conversation webhook
			 	</form></table>
			  </div>";
	stepdie("1");
}
else {
	echo "COMPLETED!<tr><td>STEP 2: <hr>";
	if (!$loginUser) {
		echo "<tr><td><div id='input'>
					<form method='post' action='index.php?id=setup' enctype='multipart/form-data'>
						<input type='text' size='100' value='' style='width: 70%' placeholder='Enter a valid user email (must exist in Spark)' name='useremail'> <input type='submit' style='width: 20%' value='Add site admin'>
			 	  	</form>
				  </div>";
	} else {
		echo msgList("success-Wizard");
	}
}

echo "</table>";