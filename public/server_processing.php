<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
include('session.php');
$windowid = (isset($_GET['id'])) ? $_GET['id'] : "bots";
if ($windowid != "login" and !verify()) header("Location: index.php?id=login");

include_once "includes/functions.inc.php";
$db_local = new Db();
$generate = new OutputEngine();
$spark = new SparkEngine();
$ldap = new LDAP();

// SQL server connection information
$sql_details = array(
    'user' => $config['username'],
    'pass' => $config['password'],
    'db'   => $config['dbname'],
    'host' => $config['dbhost']
);

//Get Data Processing Type
$type = $_GET['type'];


 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

if($type == 'table')
{
	require( 'ssp.class.php' );

	// DB table to use
$table = $_GET['table'];
 
// Table's primary key
$primaryKey = $_GET['primarykey'];


	if($table == 'contacts')
	{
		$columns = 
		array(
			array(
					'db' 		=> 'id',
					'dt'		=> 0,
					'formatter' => function( $d, $row ) {
						return '<input type="checkbox" name="userlist[]" value="'. $d .'">';
					}
				),
			array(
	        		'db'        => 'avatar',
					'dt'        => 1,
					'formatter' => function( $d, $row ) {
	    			if(empty($d)) { $d = 'images/static/noimagefound.jpeg';} 
	    				return '<img class="img-circle" src="'.$d.'" width="30" height="30">';
	    			}
	    		),
			array(
					'db'		=> 'firstName',
					'dt'		=> 2
				),
			array(
					'db'		=> 'lastName',
					'dt'		=> 3
				),
			array(
					'db'		=> 'emails',
					'dt'		=> 4
				),
			array(
					'db'        => 'id',
					'dt'        => 5,
					'formatter' => function( $d, $row ) {
	    			return '<a href="index.php?id=contacts&contactid='.$d.'" title="View Contact"><i class="fas fa-external-link-alt"></i></a>';
	        		}
	    		)
			);
		echo json_encode(
	    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
	}elseif($table == 'feedback_topic')
	{
		$columns = 
		array(
			array(
					'db'		=> 'id',
					'dt'		=> 0
				),
			array(
					'db'		=> 'title',
					'dt'		=> 1
				),
			array(
					'db'        => 'botid',
					'dt'        => 2,
					'formatter' => function( $d, $row ) {
	    			return '<a href="index.php?id=feedback&botid='.$d.'&topic='. $row['id'] .'" title="View Topic"><i class="fas fa-external-link-alt"></i></a>';
	        		}
	    		)
		);
		echo json_encode(
	    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, "botid = '{$_GET['botid']}'" )
		);
	}elseif($table == 'activity_log')
	{
		$columns = 
		array(
			array(
					'db'		=> 'date',
					'dt'		=> 0
				),
			array(
					'db'		=> 'user',
					'dt'		=> 1
				),
			array(
					'db'		=> 'botid',
					'dt'		=> 2,
					'formatter' => function( $d, $row ) {
						global $db_local;
						$bot = $db_local->botFetchBots($d);
	    			return $bot[0]['emails'];
	        		}
				),
			array(
					'db'		=> 'command',
					'dt'		=> 3
				)
		);
		echo json_encode(
	    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
	}
}


/*
elseif($type == 'ajax')
{
	setcookie('cross-site-cookie', 'name', ['samesite' => 'None', 'secure' => true]);
	$responseid = $_POST['responseid'];
	$botid = $_POST['botid'];

	$responses = $db_local->responseFetchResponses($botid);
	$values = $db_local->responseFetchResponses("",$responseid)[0];
	$selected = ($values['is_task'] == 1) ? "selected" : "";
	$f_selected = ($values['is_feature'] == 1) ? "selected" : ""; 

	$group_dropdown = "<select name='accessgroup' class='form-control'><option value='0'>none</option>";
	$group_data = $db_local->groupFetchGroups();		
	foreach ($group_data as $key) {			
		$group_selected = "";
		if (isset($_GET['edit']) and isset($values['accessgroup'])) {
			if ($key['id'] == $values['accessgroup'])
				$group_selected = "selected";
			}
			$group_dropdown .= ($key['botid'] == "0" or $key['botid'] == $botid) ? "<option value='{$key['id']}' $group_selected>{$key['groupname']}</option>":"";
		}
		$group_dropdown .= "</select>";

	echo "
	<form method='post' action='index.php?id=bots&sub=profile&botid=". $botid . "&update=" . $responseid . "&apiq=response' enctype='multipart/form-data'>
		<div class='form-group'>
			<label for='Headline'>Trigger external task (API):</label>
			<select name='is_task' class='form-control'>
				<option value='0'>No</option>
				<option value='1' <?php echo issetor($selected) ?>>Yes</option>
			</select>	
		</div>
		<div class='form-group'>
			<label for='Headline'>Keyword:</label>
			<input type='text' required='' class='form-control' placeholder='Keyword' name='keyword' value='" . $values['keyword'] . "'>
			<input type='hidden' value='" . $botid . "' name='botid'>
			<input type='hidden' value='" . $responseid . "' name='id'>
		</div>
		<div class='form-group'>
			<label for='Message'>Response:</label>

			<br>
			<div id='test-editor1'>
			<br>
				<textarea required='' placeholder='Response to keyword' name='response'>" . $values['response'] . "</textarea>
			</div>
		</div>
		<div class='form-group'>
			<label for='Headline'>File URL:</label>
			<input type='text' placeholder='URL' name='file_url' class='form-control' value='" . $values['file_url'] . "'>
		</div>
		<div class='form-group'>
			<label for='Headline'>Access group:</label>
			" . $group_dropdown . "
		</div>
		<div class='form-group'>
			<label for='Headline'>Save response:</label>
			<input type='submit' class='btn btn-md btn-primary btn-block' value='Save response'>
		</div>
	</form>
	";

echo '
<script type="text/javascript">
	$(function() {
		var editor = editormd("test-editor1", {
			width  : "1100px",
			height : "300",
			delay : 1000,
			path   : "plugins/editormd/lib/"
		});
	});
</script>';
// 	echo '
// <script type="text/javascript">
// 	$(function() {
// 		var editor = editormd("test-editor1", {
// 			width  : "1100px",
// 			height : "300",
// 			delay : 1000,
// 			path   : "plugins/editormd/lib/",
// 			toolbarIcons : function() {
//             // Or return editormd.toolbarModes[name]; // full, simple, mini
//             // Using "||" set icons align right.
//             return ["undo", "redo", "|", "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|", "h1", "h2", "h3", "h3", "h4", "h5", "h6", "|", "list-ul", "list-ol", "hr", "|", "link", "reference-link", "code", "preformatted-text", "code-block", "table"]
//         },
// 		});
// 	});
// </script>';
}
*/