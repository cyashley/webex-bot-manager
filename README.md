# Webex Bot Manager

Webex Bot Manager (WBM) is a feature rich tool for managing your bots and your day to day workflows in Webex Teams. It was designed to make it easy to start using your Webex Teams bots! 

## Features

* Responsive web design viewable on desktop, tablet and mobile web browsers.
* Easy configuration setup
* Ability to add bots
* Send messages via bots to individual users (Thousands of Users)
* Add/Remove users from spaces
* Markdown Editor
* Active Directory Integration for Authentication and adding users via AD groups
* And much more!

## Screenshots

![bots_dashboard](https://gitlab.com/mohm/webex-bot-manager/-/wiki_pages/uploads/d6f495c3b07b22cdb5ff400ecbb5c2fb/2019-11-02_22-46-11.png)
![messages_page](https://gitlab.com/mohm/webex-bot-manager/-/wiki_pages/uploads/3d7b4fd29b13c9fb35d59f7dba7ecd51/2019-11-02_22-47-50.png)
![users_page](https://gitlab.com/mohm/webex-bot-manager/-/wiki_pages/uploads/7643f3eb4e6a5f888103ab2cccfa159c/2019-11-02_22-48-22.png)

## Installation

* Read the [Installation Guide](https://gitlab.com/mohm/webex-bot-manager/-/wiki_pages/Installation) for instructions to install webex-bot-manager.

## Migration from easybot

* Read the [Migration Guide](https://gitlab.com/mohm/webex-bot-manager/-/wiki_pages/Migrating-from-easybot-to-WBM) for instructions to migrate to webex-bot-manager.

## Support
Webex Bot Manager is NOT a Cisco Webex Supported tool. Please do not call Cisco Webex for support with WBM. For support, file an issue within gitlab. 

## Credits

* Creator and Lead Developer Magnus Ohm (mohm@cisco.com)
* Secondary Developer Yusof Yaghi (yyaghi@cisco.com)